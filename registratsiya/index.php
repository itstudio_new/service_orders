<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
?>
<?
global $USER;
if ($USER->IsAuthorized()):?>
	Вы успешно зарегистрированы!<br>
	<a href="/sozdat-poruchenie/">создать поручение</a><br>
	<a href="/porucheniya/">смотреть поручения в регионе #REGION#</a><br>
	<a href="/">на главную</a><br>

<?else:?>
<form action="#" id="registation_form">
<div class="registration_form">
	<div class="top">
		<div class="left"></div>
		<div class="description">
			<div class="forms">
				  <div class="label row">
					  <em>ФИО </em>
					  <div class="col-md-6 col-sm-6 col-xs-10">
						 <input class="textbox fi fi3" placeholder="ФИО" name="name" id="reg_name" type="text" value="" >
						 <span id="err_name_text" style="text-align:center"></span>
					  </div>
					  <div></div>
				  </div>
				  <div class="label row"><em>E-mail </em>
					  <div class="col-md-6 col-sm-6 col-xs-10">
						<input class="textbox fi fi4" placeholder="E-mail" name="mail" id="reg_email" type="email" value="" >
						<span id="err_mail_text" style="text-align:center"></span>
					  </div>
					  <div></div>
				  </div>
				  <div class="label labelr row">
						<em>Телефон </em>
						<div class="col-md-6 col-sm-6 col-xs-10">
							<input class="textbox fi fi6" type="tel" name="phone" id="reg_phone" value="">
							<span id="err_phone_text" style="text-align:center"></span>
						</div>
						<div class="submit_button" style="float: left; padding-left: 10px">
							<input id="show_code" type="button" style="text-transform: none; padding-top: 12px; padding-bottom: 12px;" value="Запросить код">
						</div>
						<div class="code_field" style="padding-left: 10px">
							<input class="textbox" style="max-width: 100px; float: left" type="text" name="phone_check" id="reg_phone_check" value="">
							<div class="submit_button">
								<input type="button" style="text-transform: none; padding-top: 12px; padding-bottom: 12px;" id="check_code" value="ОК">
							</div>
						</div>
						
						<div id="phone_err_div"></div>
					</div>
				  <div class="label row reg_pass">
					  <div class="field">
						 <em>Пароль </em>
						<div>
							<input class="textbox fi fi7" type="password" id="reg_pass" name="pass" value="" >
							
						</div>
					  </div>
					  <div class="empty_pic"></div>
					  <div class="field">
						 <em>Повторите Пароль </em>
						<div>
						  <input class="textbox fi fi7" name="check_pass" id="reg_pass_check" type="password" value="" >
						 </div>
					  </div>
					  <div></div>
					  <span id="err_pass_text" style="text-align:center"></span>
				  </div>
				  <div class="reg_chk">
					  <label class="custom_checkbox" style="display: inline-block">
						<input type="checkbox" checked="checked" id="chkRules">
						Принимаю 
					  </label>
					  <a href="/pravila-servisa/">правила портала</a>
				  </div>
				  <div class="">
					  <label class="custom_checkbox" style="display: inline-block">
						<input type="checkbox" checked="checked" id="chkInfo">
						Хочу получать уведомления по почте
					  </label>
				  </div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="bot">
			<div id="final_error_div"></div>
			<div class="submit_button_alt"><input type="submit" value="Зарегистрироваться"></div>
	</div>
</div>
</form>
<?endif;?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>