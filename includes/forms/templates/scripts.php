<script src="https://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU&mode=debug" type="text/javascript"></script>
 <script type="text/javascript">

	ymaps.ready(init);

	function init() {
		 myMap = new ymaps.Map("map", {
			center: [57.002620490491296, 40.94630303081358],
			zoom: 11,
			controls: ['typeSelector', 'zoomControl']
		});		
		
		<?if($arElement['point']){?>
			var result = ymaps.geoQuery(<?=$arElement['point'];?>);
			myMap.geoObjects.add(result.search('geometry.type == "Point"').clusterize()); 

			//myMap.setBounds(myMap.geoObjects.getBounds());	

			myMap.setBounds(myMap.geoObjects.getBounds(), {checkZoomRange:true}).then(function(){ if(myMap.getZoom() > 17) myMap.setZoom(17);});
		<?}?>
					
	}

	function getPlaceMark(addr = 0){
		var addr_full = '?';
		var i = 1;
		$('input[name=address]').each(function(){
			var val = $(this).val();
			if(val){
				addr_full =  addr_full + 'addr'+i+'='+val+'&';
				i = i + 1;
			}
		});
		myMap.geoObjects.removeAll();
		$.getJSON('/includes/get_json.php'+addr_full, function (json) {
			var result = ymaps.geoQuery(json);
			myMap.geoObjects.add(result.search('geometry.type == "Point"').clusterize()); 

			//myMap.setBounds(myMap.geoObjects.getBounds());	

			myMap.setBounds(myMap.geoObjects.getBounds(), {checkZoomRange:true}).then(function(){ if(myMap.getZoom() > 17) myMap.setZoom(17);});

		

		});
		

	}
	
	$(document).ready(function(){
		

		
		$('#perenos').click(function(){
			$('.custom_checkbox.avto').remove();
			$('.label.ves').remove();
			$('.custom_checkbox.gruz, .custom_checkbox.meg').css('display', 'inline-block');
			var link = "?kat=6&sub=42";
			history.pushState('', '', link);
			$('#sel1').val(6);
			$('#sel1').closest('.custom_select').find('div').text('Перевозка и грузоперевозка');
			$('#sel2').val(42);
			$('#sel2').closest('.custom_select').find('div').text('Перевозка грузов');
			$('input[name=kat]').val(6);
			$('input[name=sub]').val(42);
			$('.popup').hide();
			return false;
			
		});
		
		 $('body').on('click', '.sort_line li .del', function () {
			$(this).closest('li').remove();
			$('#sortable li').each(function (index) {
				$(this).find('.num').text(index + 1);
			});
			getPlaceMark();
			return false;
		});
		
		$("#sortable").sortable();
		//$("#sortable").disableSelection();
		$("#sortable").sortable({
			handle: '.num',
			stop: function () {
				$('#sortable li').each(function (index) {
					$(this).find('.num').text(index + 1);
				});
				getPlaceMark();
			},
			
		});
	
		$('.result').click(function(){
			var addr = $(this).text();
			/*$.post('/includes/get_json.php?adr='+addr,{}, function (json) {
				console.log(json);
			});*/
			$.getJSON('/includes/get_json.php?adr='+addr, function (json) {
				var result = ymaps.geoQuery(json);
				myMap.geoObjects.add(result.search('geometry.type == "Point"').clusterize()); 

				myMap.setBounds(myMap.geoObjects.getBounds());	

			

			});
		});
		
		$('input[name=address]').focusout(function(){
			var addr = $(this).val();
			getPlaceMark();
		});
		
		$('#form').submit(function(){

			if($('tr.new').length == false){
				sendForm('N');
				console.log('N');
			} else{
				$('.btn-primary').click();
				console.log('Y');
			}

			return false;
		});
		
		//Телефон
		$("#reg_phone_made").focusout(function(){
			checkPhone(this);
			//var check = $(this).attr('check');
			$.post('/includes/ajax.php', {phone: $(this).val(), check_phone: 'y'}, function(data){
				//console.log(data);
				var check = parseInt(data);
				console.log(check);
				if(check == 1){
					$("#show_code_made").parent().hide();
					$('.code_field').hide();
					$('.submit_button input').hide();
					$('#reg_phone_made').attr('check', 'Y');
					//console.log(22);
				} else {
					$("#show_code_made").parent().show();
					$('.code_field').hide();
					$('.submit_button input').show();
					$('#reg_phone_made').attr('check', 'N');
					//console.log(33);
				}
			});
			$('.code_field .textbox').val('');
		});
		$("#reg_phone_check_made").focusout(function(){
			var name = $(this).val();
			if (name == false)
			{
				$(this).parent().addClass("error");
			}
			else
			{
				$(this).parent().removeClass("error");
			}
		});
		
		//Проверка телефона
		$("#show_code_made").click(function(){
			var regphone = /^[0-9\s\-\(\)\+]+$/;
			var phone = $("#reg_phone_made").val();
			if (phone != false && regphone.test(phone))
			{
				//отправка аякса
				var info = 'send_sms=y&phone='+phone;
				console.log(info);
				$.post('/includes/ajax.php', info, function(data){
					var check = parseInt(data);
					if(check == 1){
						$("#show_code_made").parent().hide();
						$('.code_field').show();
					}
					else{
						$("#phone_err_div").removeAttr("class").addClass("error_pic");
						$("#err_phone_text").addClass("error");
						$("#err_phone_text").text("Подтверждение данного номера телефона было менее суток назад");
						return false;
					}	
					console.log(data);
				});
			}
			else
			{
				$(this).parent().prev().addClass("error");
				$("#phone_err_div").removeAttr("class").addClass("error_pic");
				$("#err_phone_text").addClass("error");
				$("#err_phone_text").text("Некорректный телефон");
				return false;
			}
		});
		
		$("#check_code_made").click(function(){
			var code = $("#reg_phone_check_made").val();
			var phone = $("#reg_phone_made").val();
			if (code != false)
			{
				//отправка аякса
				var info = 'check_phone=y&check_code='+code+'&phone='+phone;
				$.post('/includes/ajax.php', info, function(data){
					var check = parseInt(data);
					if(check == 1){
						//проверка прошла успешно
						
						$("#phone_err_div").removeAttr("class").addClass("success_pic");
						var buttonOKstyle = $("#check_code").attr('style');
						$("#err_phone_text").removeClass("error");
						$("#err_phone_text").text("");
						$("#check_code").attr('style', buttonOKstyle+"display:none");
						$('#reg_phone_made').attr('check', 'Y');
					}
					else{
						//ошибка проверки
						$("#phone_err_div").removeAttr("class").addClass("error_pic");
						$("#reg_phone_check").parent().addClass("error");
						$("#err_phone_text").addClass("error");
						$("#err_phone_text").text("Неверный проверочный код телефон");
					}	
				});
			}
			else
			{
				$("#reg_phone_check").parent().addClass("error");
				$("#phone_err_div").removeAttr("class").addClass("error_pic");
			}
		});
		
		$('input[name=price]').keyup(function(){
			var number = Number($(this).val().replace(/\D+/g,""));
			$(this).val(number);
			console.log(number);
		});
		
		$('input[name=price]').focusout(function(){
			var number = Number($(this).val().replace(/\D+/g,""));
			if(number < 300){
				$(this).val(300);
			}

		});

	});
	

	
	function del_file(file){
		$.post('/includes/ajax.php/', {FILE_DEL:file}, function(data){
			console.log(data);
		});
	}
		
 </script>