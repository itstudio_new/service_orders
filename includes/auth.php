<div id="call_enter" style="display: none">
	<form action="/" id="authorize">
        <h2>Вход на сайт</h2>
        <div class="inner">
            <label>
                <em>Ваш e-mail</em>
                <input class="textbox fi1" type="text" name="login" placeholder="Логин" value="">
            </label>
            <div class="label row">
                <div style="padding-right: 40px;" class="col-md-8 col-sm-8 col-xs-8">
                    <input class="textbox fi2" type="password" name="password" placeholder="Пароль" value="">
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="submit_button">
                        <input style="width: 100%; text-transform: none" type="submit" class="submit" value="Войти">
                    </div>
                </div>
            </div>
            <div class="error_message"></div>
        </div>
        <div class="pass_link">
            <a href="/vosstanovlenie-parolya/?forgot_password=yes&clear_cache=Y">Забыли пароль?</a>
        </div>
        <div class="or"><div class="text">или</div></div>
        <div class="social">
		<?php
		 $APPLICATION->IncludeComponent("ulogin:auth", "", Array(
			"PROVIDERS" => "facebook,vkontakte,odnoklassniki,mail",
				"HIDDEN" => "other",
				"TYPE" => "small",
				"SEND_MAIL" => "N",
				"SOCIAL_LINK" => "Y",	// Сохранять ссылку на страницу пользователя в соцсети
				"GROUP_ID" => "",	// Группа клиентов по умолчанию:
				"ULOGINID1" => "3e56a809",	// uLogin ID общая форма №1
				"ULOGINID2" => "",	// uLogin ID общая форма №2
				"COMPONENT_TEMPLATE" => ".default",
				"SEND_EMAIL" => "Y",	// Отправлять email администратору при регистрации пользователя
				"LOGIN_AS_EMAIL" => "Y",	// Использовать email в качестве логина
			),
			false
		); 
		?>
        </div>
        <div class="bottom">
            <div class="h3">Еще не зарегистрированы?</div>
            <div class="submit_button_alt">
                <a href="/registratsiya/">Зарегистрироваться</a>
            </div>
        </div>
	</form>
</div>