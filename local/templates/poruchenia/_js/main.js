/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';
	var allImages=[];
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
		url: '/includes/php/',
		stop: function(e){
			console.log('fileupload');
			sendForm('Y');
		}

    });
	
	$('body').on('click', '.btn_del', function(){
		var attr = $(this).attr('data-del');
		if(attr){
			alert(attr);
		}
	});
	

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
		{
			previewMaxWidth: 130,
			previewMaxHeight: 130,
			previewCrop: true,

		},
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        ),
        'dropZone', $('#drop')
		
		
		
    );

    if (window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $('#fileupload').fileupload('option', {
            url: '//jquery-file-upload.appspot.com/',
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 999000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,

        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '//jquery-file-upload.appspot.com/',
                type: 'HEAD'
            }).fail(function () {
                $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });
    }
	
	$('body').on('click', '.table-striped .btn_del', function(){
		$(this).closest('tr').remove();
		
	})
	$('body').on('click', '.fileupload_buttonbar .btn_delete, .fileupload_buttonbar .btn_cancel', function(){
		$('.table-striped tr').remove();
		
	})
	
	/*$('body').on('submit', '#form', function(){
		console.log($('#fileupload')[0]);
		$.when(uploadAllImages())
			.then(function () {

		 });
		 var form = $('#fileupload').serialize();
		 console.log(form);
		 $.post('/includes/uploadFiles.php', form, function(data){
			 console.log(data);
		 });
		return false;
	});*/
});

function uploadAllImages() {
        for (var i = 0; i < allImages.length; i++) {
			$('.fileupload_container').append('<input type="text" name="files[]"  value="'+allImages[i]['files'][0]['name']+'" >')
            allImages[i].submit();            
        }
        allImages = [];
        return true;
        return dfd.promise();
    }
