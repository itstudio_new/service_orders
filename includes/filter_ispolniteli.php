<?CModule::IncludeModule('iblock');

if($_GET['kat']){
	$arFilter = array('IBLOCK_ID' => 2, 'ID' => $_GET['kat']); // выберет потомков без учета активности
	$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter, false, array('NAME'));
	if ($arSect = $rsSect->GetNext()){
	   $katName = $arSect['NAME'];
	   $APPLICATION->SetTitle("Исполнители: ".$katName);
	}
	
	
}

if($_GET['sub']){
	$arFilter = array('IBLOCK_ID' => 2, 'ID' => $_GET['sub']); // выберет потомков без учета активности
	$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter, false, array('NAME'));
	if ($arSect = $rsSect->GetNext()){
	   $katSubName = $arSect['NAME'];
	   $APPLICATION->SetTitle("Исполнители: ".$katName." - ".$katSubName);
	}
}

$arFilter = Array("IBLOCK_ID"=>4, "ACTIVE"=>"Y");

if($_GET['sub']){
	$arFilter['PROPERTY_SUB_KATEGORIYA'] = $_GET['sub'];
} elseif($_GET['kat']) {
	$arFilter['PROPERTY_KATEGORIYA'] = $_GET['kat'];
}
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('NAME'));
if($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();    
	$reviewCheck = 1;
}

?>

<div class="map_top_filter top_filter_noline">
	<div class="row">
		<form id="user_list">
		
			<?if($_GET['kat']){?>
				<input type="hidden" name="kat" value="<?=$_GET['kat'];?>" />
			<?}?>
			<?if($_GET['sub']){?>
				<input type="hidden" name="sub" value="<?=$_GET['sub'];?>" />
			<?}?>
			
			<label class="custom_checkbox" style="display: inline-block">
				<input type="checkbox" name="online" <?if($_GET['online']){?> checked<?}?>>
				<span class="light">Сейчас на сайте</span>
			</label>
			<div class="rating_sign_cnt">
				<div class="rating_container readonly">
					<?if($_GET['rating'] == false) $rating = 5;
					else $rating = $_GET['rating'];?>
					<div class="name" style="font-weight: normal;">Рейтинг от : </div>
					<div class="rating" data-rate-value="<?=$rating;?>"></div>
					<div class="count"></div>
					<input type="hidden" name="rating" value="" />
				</div>
			</div>
			<label class="custom_checkbox" style="display: inline-block">
				<input type="checkbox" name="tried" <?if($_GET['tried']){?> checked<?}?>>
				<span class="light">Только проверенные исполнители</span>
			</label>
			<?if($reviewCheck){?>
				<label class="custom_checkbox" style="display: inline-block">
					<input type="checkbox" name="reviews" <?if($_GET['reviews']){?> checked<?}?>>
					<span class="light">Только с отзывами в этой категории</span>
				</label>
			<?}?>
		</form>
	</div>
</div>

<?





//пользователи, которые сейчас на сайте
$by = "s_last_date"; 
$order = "desc"; 
$db = CUser::GetList($by, $order, array("LAST_ACTIVITY"=>120)); 
while($dba = $db->Fetch()){ 
	$arActiveUser[$dba['ID']] = $dba['ID'];
}

//список пользователей по фильтру
$filter = array();

$filter["UF_TYPE"] = 1; //исполнители

if($_GET['sub']){
	$filter['PERSONAL_NOTES'] = '%/'.$_GET['sub'].'/%';
} elseif($_GET['kat']) {
	$filter['PERSONAL_NOTES'] = '%/'.$_GET['kat'].'/%';
}

if($_GET['online'] == 'on'){
	$filter["LAST_ACTIVITY"] = 120;
}

if($_GET['rating']){
	$filter[">=UF_RATING"] = $_GET['rating'];
}

if($_GET['rating']){
	$filter[">=UF_RATING"] = $_GET['rating'];
}

if($_GET['tried']){
	$filter["!UF_TRIED"] = false;
}



$arUserID = array('1');
if($_GET['reviews']){
	$arFilter = Array("IBLOCK_ID"=>4, "ACTIVE"=>"Y");

	if($_GET['sub']){
		$arFilter['PROPERTY_SUB_KATEGORIYA'] = $_GET['sub'];
	} elseif($_GET['kat']) {
		$arFilter['PROPERTY_KATEGORIYA'] = $_GET['kat'];
	}
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('NAME', 'PROPERTY_ISPOLNITEL', 'PROPERTY_MARK',  'PROPERTY_KATEGORIYA',  'PROPERTY_SUB_KATEGORIYA'));
	while($ob = $res->GetNextElement()){ 
		$arFields = $ob->GetFields();    
		$arReviews[] = $arFields;
		$arUserID[] = $arFields['PROPERTY_ISPOLNITEL_VALUE'];
	}
	

	$filter["ID"] = $arUserID;
	$filter["ID"] = implode('|', $filter["ID"]);
}

$rsUser = CUser::GetList(($by="ID"), ($order="desc"), $filter,array("SELECT"=>array("UF_*"), 'FIELD' => array('NAME', 'ID')));
while($arUser = $rsUser->Fetch()){
	if($arUser['PERSONAL_PHOTO']){
		$img = CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array("width"=>106, "height"=>106), BX_RESIZE_IMAGE_EXACT, true);
		$arUser['PHOTO'] = $img['src'];
	} else {
		$arUser['PHOTO'] = SITE_TEMPLATE_PATH.'/images/photo.jpg';
	}	
	
	$last_date = $arUser['LAST_ACTIVITY_DATE'];
	if($arUser['LAST_ACTIVITY_DATE'] == false ) $last_date = $arUser['LAST_LOGIN'];
	$arDateReg = dateDifference($last_date ,date());
	$arDateReg = explode(' ', $arDateReg);
	$arUser['LAST_Y'] = $arDateReg[0];
	$arUser['LAST_M'] = $arDateReg[1];
	$arUser['LAST_D'] = $arDateReg[2];
	$arUser['LAST_H'] = $arDateReg[3];
	$arUser['LAST_MIN'] = $arDateReg[4];
	//echo '<pre>';print_r ($arUser);echo '</pre>';
	$arUsers[] = $arUser;
}





global $USER;
if ($USER->IsAuthorized()){
	$temp = array();
	$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$USER->GetID()),array("SELECT"=>array("UF_*"), 'FIELD' => array('UF_WORK_WITH_USER')));
	$arUser = $rsUser->Fetch();
	$arWorkWithUser = $arUser['UF_WORK_WITH_USER'];
	$arWorkWithUser = explode('/',$arWorkWithUser);
	foreach($arWorkWithUser as $userID){
		if($userID)
			$temp[$userID] = $userID;
	}
	
	$arWorkWithUser = $temp;
}

?>


<pre><?//print_r ($arDateReg);?></pre>


<script type="text/javascript"> 
	$(document).ready(function(){
		$('form#user_list input[type=checkbox]').change(function(){
			$('form#user_list').submit();
		});
		
		$('form#user_list .rating').click(function(){
			var rating = $(this).attr('data-rate-value');
			$('form#user_list input[name=rating]').val(rating);
			$('form#user_list').submit();
		});
	});	
</script>
