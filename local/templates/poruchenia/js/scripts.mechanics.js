$(function () {
	/*$('body').on('click', '.map_win', function () {
        var id = $(this).attr('data-id');
		$.post('/includes/map_element.php', {ID:id}, function(data){
			$('#res_element').html(data);
		});
        return false;
    });*/
	
	$('body').on('submit', '#otclick', function(){
		$.post('/includes/ajax.php', $(this).serialize(), function(data){
			if(data){
				$('#call_ready_dialog .close').click();
				$('#call_make .inner').html(data);
				var call_make = $('#call_make').customDialog();
				call_make.show();
				
			}
		});
		return false;
	});
	
	$('body').on('change', '#otclick_update input', function(){
		$.post('/includes/ajax.php', $(this).closest('form').serialize(), function(data){
			if(data){
				console.log(data);
				
			}
		});
		return false;
	});
	
	
	 $('body').on('click', '.map_bottom .close', function () {	
        $(this).closest('.map_bottom').remove();
        return false;
    });
	
	$('input[name=time]').keyup(function(){
		var error = 0;
		var val = $(this).val();
		var arVal = val.split(':'); 
		var chas = parseInt(arVal[0].replace(/\D+/g,""));
		var min = parseInt(arVal[1].replace(/\D+/g,""));
		if(isNaN(chas)) chas = '00';
		if(isNaN(min)) min = '00';
		if(chas > 23){
			chas = '00';
			error = 1;
		} 
		if(min > 59){
			min = '00';
			error = 1;
		}
		if(error){
			$(this).val(chas+':'+min);
		}
		
		
	});
	
	
	$('body').on('click', '.user_cat_scroll ul>li>ul>li a', function () {
        var _t = $(this).closest('ul').parents('li').children('a').text() + " / " + $(this).text()
		var id_parent = $(this).closest('ul').parents('li').children('a').attr('data-id');
		var id = $(this).attr('data-id');
		if($('.item input[data-id='+id+']').length == 0)
			$('.user_cat_select').append('<div class="item">' + _t + '<input type="hidden" data-id="'+id+'" name="section[]" value="'+id+'/'+id_parent+'"><a class="del" href="#"></a></div>');
        return false;
    });
});	
	
	
//Авторизация
$(function () {
	$('#authorize').submit(function(){
		error = 0;
		var regmail = /@/;
		var regphone = /^[0-9\s\-\(\)\+]+$/;
		$("#authorize .error").removeClass('error');
		$(this).find("div.error_message").text('');
		var login = $(this).find('input[name="login"]');
		var password = $(this).find('input[name="password"]');
		if(login.val() == false){
			   login.parent().addClass('error');
			   error = 1;
			   $(this).find("div.error_message").text("Проверьте, правильно ли вы заполнили поля");
		}
		if(password.val() == false){
			   password.parent().addClass('error');
			   error = 1;
			   $(this).find("div.error_message").text("Проверьте, правильно ли вы заполнили поля");
			   
		}
		if(error == 0){
			var form = $(this).serialize();
			form = form + '&authorize=y';
			$.post('/includes/ajax.php', form, function(data){
				if(data){
					console.log(data);
					if (data.indexOf('success') !== -1)
					{
						window.location.reload();
					}
					else
					{
						$(password).val('');
						$("#authorize").find("div.error_message").text("Проверьте, правильно ли вы заполнили поля");
					}
				}
					
			});
			console.log(form);
		}
		return false;
	});
});
//Авторизация - end

//Восстановление пароля
function recovery() {
	var regmail = /@/;
	$("#recoverymyform div.error_message").text('');
	$("#recoverymyform .error").removeClass('error');
	$(this).find("div.error_message").text('');
	var email = $("#recovery_name");
	if (email.val() == false)
	{
		email.parent().addClass('error');
	}
	else
	{
		if (!regmail.test(email.val()))
		{
			email.parent().addClass('error');
		}
	}
	if ($('#recoverymyform .error').length == 0 ) {
		var form = $("#recoverymyform").serialize();
		form = form + '&fogot=y';
		$.post( "/includes/ajax.php", form,
		function( data ) {
			$('.informtext').html(data);
		});
	}
	else
	{
		$("#recoverymyform div.error_message").text("Проверьте, правильно ли вы заполнили поля");
	}
}
	
$(function () {	
	$('#change_pass').submit(function(){
		error = 0;
		var regmail = /@/;
		var regphone = /^[0-9\s\-\(\)\+]+$/;
		$("#change_pass .error").removeClass('error');
		$(this).find("div.error_message").text('');
		var login = $(this).find('input[name="login"]');
		var check = $(this).find('input[name="check"]');
		var pass = $(this).find('input[name="pass"]');
		var check_pass = $(this).find('input[name="check_pass"]');
		if (check_pass.val().length < 6)
		{
			check_pass.parent().addClass('error');
			error = 3;
		}
		if (pass.val().length < 6)
		{
			pass.parent().addClass('error');
			   error = 3;
		}
		if(login.val() == false){
			   login.parent().addClass('error');
			   error = 1;
		}
		if(check.val() == false){
			   ckeck.parent().addClass('error');
			   error = 1;
		}
		if(pass.val() == false){
			   pass.parent().addClass('error');
			   error = 1;
		}
		if(check_pass.val() == false){
			   check_pass.parent().addClass('error');
			   error = 1;
		}
		if(pass.val() != check_pass.val()){
			pass.parent().addClass('error');
			check_pass.parent().addClass('error');
			error = 2;
		}
		if(error == 0){
			var form = $(this).serialize();
			form = form + '&change=y';
			$.post('/includes/ajax.php', form, function(data){
				if(data == false){
					document.location.href = '/personal/';
				}
				if(data != false){
					$('#change_pass .error_message').html(data);
				}
					
			});
		}
		else if (error == 1)
		{
			$(this).find("div.error_message").text("Проверьте, правильно ли вы заполнили поля");
		}
		else if (error == 2)
		{
			$(this).find("div.error_message").text("Введенные пароли не совпадают");
		}
		else if (error == 3)
		{
			$(this).find("div.error_message").text("Слишком короткий пароль");
		}
		return false;
	});
});
//Восстановление пароля - end

//Регистрация
$(function () {	
	//ФИО
	$("#reg_name").focusout(function(){
		checkName(this);
	});
	//Email
	$("#reg_email").focusout(function(){
		checkEmail(this);
	});
		
	//Телефон
	$("#reg_phone").focusout(function(){
		checkPhone(this);
		$("#show_code").parent().show();
		$('.code_field').hide();
		$('.submit_button input').show();
		$('.code_field .textbox').val('');
	});
	$("#reg_phone_check").focusout(function(){
		var name = $(this).val();
		if (name == false)
		{
			$(this).parent().addClass("error");
		}
		else
		{
			$(this).parent().removeClass("error");
		}
	});
	//Проверка телефона
	$("#show_code").click(function(){
		var regphone = /^[0-9\s\-\(\)\+]+$/;
		var phone = $("#reg_phone").val();
		if (phone != false && regphone.test(phone))
		{
			//отправка аякса
			var info = 'send_sms=y&phone='+phone;
			console.log(info);
			$.post('/includes/ajax.php', info, function(data){
				var check = parseInt(data);
				if(check == 1){
					$("#show_code").parent().hide();
					$('.code_field').show();
				}
				else{
					$("#phone_err_div").removeAttr("class").addClass("error_pic");
					$("#err_phone_text").addClass("error");
					$("#err_phone_text").text("Подтверждение данного номера телефона было менее суток назад");
					return false;
				}	
				console.log(data);
			});
		}
		else
		{
			$(this).parent().prev().addClass("error");
			$("#phone_err_div").removeAttr("class").addClass("error_pic");
			$("#err_phone_text").addClass("error");
			$("#err_phone_text").text("Некорректный телефон");
			return false;
		}
	});
	
	$("#check_code").click(function(){
		var code = $("#reg_phone_check").val();
		var phone = $("#reg_phone").val();
		if (code != false)
		{
			//отправка аякса
			var info = 'check_phone=y&check_code='+code+'&phone='+phone;
			$.post('/includes/ajax.php', info, function(data){
				var check = parseInt(data);
				if(check == 1){
					//проверка прошла успешно
					
					$("#phone_err_div").removeAttr("class").addClass("success_pic");
					var buttonOKstyle = $("#check_code").attr('style');
					$("#err_phone_text").removeClass("error");
					$("#err_phone_text").text("");
					$("#check_code").attr('style', buttonOKstyle+"display:none");
				}
				else{
					//ошибка проверки
					$("#phone_err_div").removeAttr("class").addClass("error_pic");
					$("#reg_phone_check").parent().addClass("error");
					$("#err_phone_text").addClass("error");
					$("#err_phone_text").text("Неверный проверочный код телефон");
				}	
			});
		}
		else
		{
			$("#reg_phone_check").parent().addClass("error");
			$("#phone_err_div").removeAttr("class").addClass("error_pic");
		}
	});
	
	//Пароль - не менее 6 символов, и хватит
	$("#reg_pass").focusout(function(){
		checkPass(this);
	});
	$("#reg_pass_check").focusout(function(){
		checkPassSecond(this);
	});
	//Отравка данных на регистрацию
	$("#registation_form").submit(function(){
		checkName($("#reg_name"));
		checkEmail($("#reg_email"));
		if($('#phone_err_div').hasClass('success_pic')){
			var success_pic = true;
		}
		checkPhone($("#reg_phone"));
		if(success_pic)
			$('#phone_err_div').addClass('success_pic');
		checkPass($("#reg_pass"));
		checkPassSecond($("#reg_pass_check"));
		$("#final_error_div").removeClass("error");
		$("#final_error_div").text("");
		var checkRules = $("#chkRules")[0].checked;
		var checkInfo = $("#chkInfo")[0].checked;
		var errDiv = $("#registation_form .error");

		
		if(checkRules){
			if (errDiv.length == 0)
			{
				var form = $(this).serialize();
				form = form + '&register=y&spam='+(checkInfo ? 1 : 0);
				if(success_pic)
					form = form + '&phone_ch=Y';
				console.log(form);
				$.post('/includes/ajax.php', form, function(data){
					if(data){
						console.log(data);
						window.location.reload();
					}
						
				});
			}
		}
		else
		{
			$("#final_error_div").addClass("error");
			$("#final_error_div").text("Необходимо соглашение с правилами портала");
		}
		return false;
	});
	
	
	//Отравка данных на изменение исполнителя
	var files;
	var check_file = false;
	$('input#file').change(function(){
		files = this.files;
		check_file = true;
	});
	
	$("#user_form").submit(function(){
		checkName($("#reg_name"));
		var checkPhone = $('#reg_phone_made').attr('check');
		$("#final_error_div").removeClass("error");
		$("#final_error_div").text("");
		var errDiv = $("#user_form .error");

		if (errDiv.length == 0)
		{
			var form = $(this).serialize();
			if(checkPhone == 'Y')
				form = form + '&phone_ch=Y';
			form = form + '&user_change=Y';
			$.post('/includes/ajax.php', form, function(data){
				if(data){
					//console.log(data);
					$('#user_form .rem').html(data);
					//window.location.reload();
				}
					
			});
		}
		
			if(check_file){
				var data = new FormData();
				$.each( files, function( key, value ){
					data.append( key, value );
				});

			
				$.ajax({
					url: '/includes/ajax.php?avatar=y',
					type: 'POST',
					data: data,
					cache: false,
					dataType: 'json',
					processData: false, // Не обрабатываем файлы (Don't process the files)
					contentType: false, // Так jQuery скажет серверу что это строковой запрос
					success: function( respond, textStatus, jqXHR ){
			 
						// Если все ОК
			 
						if( typeof respond.error === 'undefined' ){


						}
						else{
							//console.log('ОШИБКИ ОТВЕТА сервера: ' + respond.error );
						}
					},
					error: function( jqXHR, textStatus, errorThrown ){
						//console.log('ОШИБКИ AJAX запроса: ' + textStatus );
					}
				});
			}

		return false;
	});
	
	
	
});

function checkName(e)
{
	var name = $(e).val();
	if (name == false)
	{
		$(e).parent().addClass("error");
		$(e).parent().next("div").removeAttr("class").addClass("error_pic");
		$("#err_name_text").addClass("error");
		$("#err_name_text").text("Поле должно быть заполнено");
	}
	else
	{
		$(e).parent().removeClass("error");
		$(e).parent().next("div").removeAttr("class").addClass("success_pic");
		$("#err_name_text").removeClass("error");
		$("#err_name_text").text("");
	}

}

function checkEmail(e)
{
	var regmail = /@/;
	var email = $(e).val();
	if (email == false)
	{
		$(e).parent().addClass("error");
		$(e).parent().next("div").removeAttr("class").addClass("error_pic");
		$("#err_mail_text").addClass("error");
		$("#err_mail_text").text("Поле должно быть заполнено");
	}
	else
	{
		if (!regmail.test(email))
		{
			$(e).parent().addClass("error");
			$(e).parent().next("div").removeAttr("class").addClass("error_pic");
			$("#err_mail_text").addClass("error");
			$("#err_mail_text").text("Некорректный E-Mail");
		}
		else
		{
			//проверка на повторение
			var info = 'check_email=y&email='+email;
			$.post('/includes/ajax.php', info, function(data){
				var check = parseInt(data);
				if(check == 1){
					//проверка прошла успешно
					$("#reg_email").parent().removeClass("error");
					$("#reg_email").parent().next("div").removeAttr("class").addClass("success_pic");
					$("#err_mail_text").removeClass("error");
					$("#err_mail_text").text("");
				}
				else{
					//ошибка проверки
					$("#reg_email").parent().addClass("error");
					$("#reg_email").parent().next("div").removeAttr("class").addClass("error_pic");
					$("#err_mail_text").addClass("error");
					$("#err_mail_text").text("Такой логин уже существует");
				}	
			});
		}
	}
}

function checkPhone(e)
{
	var regphone = /^[0-9\s\-\(\)\+]+$/;
	var phone = $(e).val();
	if (phone == false)
	{
		$(e).parent().addClass("error");
		$("#phone_err_div").removeAttr("class").addClass("error_pic");
		$("#err_phone_text").addClass("error");
		$("#err_phone_text").text("Поле должно быть заполнено");
	}
	else
	{
		if(!regphone.test(phone))
		{	
			$(e).parent().addClass("error");
			$("#phone_err_div").removeAttr("class").addClass("error_pic");
			$("#err_phone_text").addClass("error");
			$("#err_phone_text").text("Некорректный телефон");
		}
		else
		{
			$(e).parent().removeClass("error");
			$("#phone_err_div").removeAttr("class");
			$("#err_phone_text").removeClass("error");
			$("#err_phone_text").text("");
		}
	}
}

function checkPass(e)
{
	var pass_check = $("#reg_pass_check").val();
	var pass = $(e).val();
	if (pass == false)
	{
		$(e).parent().addClass("error");
		$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
		$("#err_pass_text").addClass("error");
		$("#err_pass_text").text("Введите пароль");
	}
	else
	{
		if (pass.length < 6)
		{
			$(e).parent().addClass("error");
			$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
			$("#err_pass_text").addClass("error");
			$("#err_pass_text").text("Пароль должен быть не менее 6 символов");
		}
		else
		{
			if (pass_check != false && pass != pass_check)
			{
				$(e).parent().addClass("error");
				$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
				$("#err_pass_text").addClass("error");
				$("#err_pass_text").text("Пароли не совпадают");
			}
			else
			{
				$(e).parent().removeClass("error");
				$(e).parent().parent().next("div").removeAttr("class").addClass("success_pic");
				$("#err_pass_text").removeClass("error");
				$("#err_pass_text").text("");
			}
		}
	}
}

function checkPassSecond(e)
{
	var pass = $("#reg_pass").val();
	var pass_check = $(e).val();
	if (pass_check == false)
	{
		$(e).parent().addClass("error");
		$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
		$("#err_pass_text").addClass("error");
		$("#err_pass_text").text("Введите подтверждение пароля");
	}
	else
	{
		if (pass_check.length < 6)
		{
			$(e).parent().addClass("error");
			$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
			$("#err_pass_text").addClass("error");
			$("#err_pass_text").text("Пароль должен быть не менее 6 символов");
		}
		else
		{
			if (pass != false && pass != pass_check)
			{
			
				$(e).parent().addClass("error");
				$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
				$("#err_pass_text").addClass("error");
				$("#err_pass_text").text("Пароли не совпадают");
			}
			else
			{
				$(e).parent().removeClass("error");
				$(e).parent().parent().next("div").removeAttr("class").addClass("success_pic");
				$("#err_pass_text").removeClass("error");
				$("#err_pass_text").text("");
			}
		}
	}
}
//Регистрация - end


function otclick(id){
	$.post('/includes/otclick.php', {ID:id}, function(data){
		if(data){
			$('#call_ready_dialog .inner').html(data);
			var ready_dialog = $('#call_ready_dialog').customDialog();
			ready_dialog.show();
		}
	});
	return false;
};

function select_ispolnitel(id){
	$.post('/includes/select_ispolnitel.php', {ID:id}, function(data){
		if(data){
			$('#call_dialog_order_select').html(data);
			var call_dialog_order_select = $('#call_dialog_order_select').customDialog();
			call_dialog_order_select.show();
		}
	});

	return false;
};

function select_ispolnitel_y(user, poruchenie){
	$.post('/includes/ajax.php', {USER:user, PORUCHENIE: poruchenie, SELECT_ISPOLNITEL: 'Y'}, function(data){
		window.location.reload();
	});

	return false;
};

function del_poruchenie(id){
	$.post('/includes/del_poruchenie.php', {ID:id}, function(data){
		if(data){
			$('#call_dialog_del').html(data);
			var call_dialog_del = $('#call_dialog_del').customDialog({ width: 460, addClass: "blue_dialog" });
			call_dialog_del.show();
		}
	});

	return false;
};

function del_poruchenie_y(id){
	$.post('/includes/ajax.php', {ID:id, DEL: 'Y'}, function(data){
		window.location.reload();
	});

	return false;
};


function cancel_poruchenie(id){
	$.post('/includes/cancel_poruchenie.php', {ID:id}, function(data){
		if(data){
			$('#call_dialog_cancel').html(data);
			var call_dialog_cancel = $('#call_dialog_cancel').customDialog({ width: 430, addClass: "white_dialog"  });
			call_dialog_cancel.show();
		}
	});

	return false;
};

function cancel_poruchenie_y(id){
	$.post('/includes/ajax.php', {ID:id, CANCEL: 'Y'}, function(data){
		window.location.reload();
	});

	return false;
};


function accept(id){
	$.post('/includes/accept_poruchenie.php', {ID:id}, function(data){
		if(data){
			$('#call_user_sign').html(data);
			var call_user_sign = $('#call_user_sign').customDialog({ width: 430, addClass: "white_dialog"  });
			call_user_sign.show();
			 $(".rating_accept").rate({
				selected_symbol_type: 'image',
				max_value: 5,
				step_size: 0.5,
				readonly: false,
				symbols: {
					image: {
						base: '<div class="star">&nbsp;</div>',
						hover: '<div class="star_hover">&nbsp;</div>',
						selected: '<div class="star_selected">&nbsp;</div>',
					},
				}
			});
			$(".rating_accept").on("change", function (ev, data) {
				$(this).closest('form').find('input[name=rating]').val(data.to);
			});
		}
	});

	return false;
};

$(function () {
	$('body').on('submit', '#accept_poruchenie', function(){
		error = 0;
		var regmail = /@/;
		var regphone = /^[0-9\s\-\(\)\+]+$/;
		$("#accept_poruchenie .error").removeClass('error');
		$(this).find("div.error_message").text('');
		var rating = $(this).find('input[name="rating"]');
		var text = $(this).find('textarea[name="text"]');
		if(rating.val() == false && text.val() == false){
			   rating.parent('label').addClass('error');
			   error = 1;
			  // $(this).find("div.error_message").text("Проверьте, правильно ли вы заполнили поля");
		}
		if(error == 0){
			var form = $(this).serialize();
			form = form + '&ACCEPT=y';
			$.post('/includes/ajax.php', form, function(data){
				window.location.reload();	
			});
		}
		return false;
	});
});