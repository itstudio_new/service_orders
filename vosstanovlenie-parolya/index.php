<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Восстановление пароля");
?>
<? 
if($_GET['forgot_password']){
?>
<div class="forms color_form" style="width: 100%;max-width: 100%;" >
	<div class="inner reg_inner">
		<div class="reg_form">
			<p style="width: 100%;" >Введите Ваш E-mail </p>
			<form method="post" id="recoverymyform" action="" >
				<div class="row form_row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						  <label>
							  <input class="textbox" placeholder="E-mail" type="text" name="recovery_name" id="recovery_name" value="" required="required">
							  <span></span>
						  </label>
					</div>
				</div>			
				<div class="error_message" style="text-align:left"></div>
				<div class="informtext" style="font-size: 14px; color: green;" ></div>
				<br><br>
				<div class="submit_button" style="text-align: left;" ><input type="submit" onclick="recovery(); return false;" value="Напомнить пароль"></div>
			</form>
		</div>
	</div>
</div>

<? } elseif($_GET['change_password']){?>
<div class="forms">
	<form id="change_pass">
		<label>
			<input type="text" class="textbox" name="login" value="<?=$_GET['USER_LOGIN']?>"  placeholder="Логин"/>
		</label>
		<label>
			<input type="text" class="textbox" name="check" value="<?=$_GET['USER_CHECKWORD']?>"  placeholder="Контрольная строка"/>
		</label>
		<label>
			<input type="password" class="textbox" name="pass" value=""  placeholder="Пароль"/>
		</label>
		<label>
			<input type="password" class="textbox" name="check_pass" value=""  placeholder="Подтверждение пароля"/>
		</label>
		<div class="error_message" style="text-align:left"></div><br>
		<div class="submit_button"><input type="submit" name="send_account_info" value="Изменить" /></div>
	</form>

</div>
<? } else { ?>
<? LocalRedirect('/'); ?>
<? } ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>