$(function () {
	/*$('body').on('click', '.map_win', function () {
        var id = $(this).attr('data-id');
		$.post('/includes/map_element.php', {ID:id}, function(data){
			$('#res_element').html(data);
		});
        return false;
    });*/
	
	 $('body').on('click', '.map_bottom .close', function () {	
        $(this).closest('.map_bottom').remove();
        return false;
    });
	
	$('input[name=time]').keyup(function(){
		var error = 0;
		var val = $(this).val();
		var arVal = val.split(':'); 
		var chas = parseInt(arVal[0].replace(/\D+/g,""));
		var min = parseInt(arVal[1].replace(/\D+/g,""));
		if(isNaN(chas)) chas = '00';
		if(isNaN(min)) min = '00';
		if(chas > 23){
			chas = '00';
			error = 1;
		} 
		if(min > 59){
			min = '00';
			error = 1;
		}
		if(error){
			$(this).val(chas+':'+min);
		}
		
		
	});
});	
	
	
//Авторизация
$(function () {
	$('#authorize').submit(function(){
		error = 0;
		var regmail = /@/;
		var regphone = /^[0-9\s\-\(\)\+]+$/;
		$("#authorize .error").removeClass('error');
		$(this).find("div.error_message").text('');
		var login = $(this).find('input[name="login"]');
		var password = $(this).find('input[name="password"]');
		if(login.val() == false){
			   login.parent().addClass('error');
			   error = 1;
			   $(this).find("div.error_message").text("Проверьте, правильно ли вы заполнили поля");
		}
		if(password.val() == false){
			   password.parent().addClass('error');
			   error = 1;
			   $(this).find("div.error_message").text("Проверьте, правильно ли вы заполнили поля");
			   
		}
		if(error == 0){
			var form = $(this).serialize();
			form = form + '&authorize=y';
			$.post('/includes/ajax.php', form, function(data){
				if(data){
					console.log(data);
					if (data.indexOf('success') !== -1)
					{
						window.location.reload();
					}
					else
					{
						$(password).val('');
						$("#authorize").find("div.error_message").text("Проверьте, правильно ли вы заполнили поля");
					}
				}
					
			});
			console.log(form);
		}
		return false;
	});
});
//Авторизация - end

//Восстановление пароля
function recovery() {
	var regmail = /@/;
	$("#recoverymyform div.error_message").text('');
	$("#recoverymyform .error").removeClass('error');
	$(this).find("div.error_message").text('');
	var email = $("#recovery_name");
	if (email.val() == false)
	{
		email.parent().addClass('error');
	}
	else
	{
		if (!regmail.test(email.val()))
		{
			email.parent().addClass('error');
		}
	}
	if ($('#recoverymyform .error').length == 0 ) {
		var form = $("#recoverymyform").serialize();
		form = form + '&fogot=y';
		$.post( "/includes/ajax.php", form,
		function( data ) {
			$('.informtext').html(data);
		});
	}
	else
	{
		$("#recoverymyform div.error_message").text("Проверьте, правильно ли вы заполнили поля");
	}
}
	
$(function () {	
	$('#change_pass').submit(function(){
		error = 0;
		var regmail = /@/;
		var regphone = /^[0-9\s\-\(\)\+]+$/;
		$("#change_pass .error").removeClass('error');
		$(this).find("div.error_message").text('');
		var login = $(this).find('input[name="login"]');
		var check = $(this).find('input[name="check"]');
		var pass = $(this).find('input[name="pass"]');
		var check_pass = $(this).find('input[name="check_pass"]');
		if (check_pass.val().length < 6)
		{
			check_pass.parent().addClass('error');
			error = 3;
		}
		if (pass.val().length < 6)
		{
			pass.parent().addClass('error');
			   error = 3;
		}
		if(login.val() == false){
			   login.parent().addClass('error');
			   error = 1;
		}
		if(check.val() == false){
			   ckeck.parent().addClass('error');
			   error = 1;
		}
		if(pass.val() == false){
			   pass.parent().addClass('error');
			   error = 1;
		}
		if(check_pass.val() == false){
			   check_pass.parent().addClass('error');
			   error = 1;
		}
		if(pass.val() != check_pass.val()){
			pass.parent().addClass('error');
			check_pass.parent().addClass('error');
			error = 2;
		}
		if(error == 0){
			var form = $(this).serialize();
			form = form + '&change=y';
			$.post('/includes/ajax.php', form, function(data){
				if(data == false){
					document.location.href = '/personal/';
				}
				if(data != false){
					$('#change_pass .error_message').html(data);
				}
					
			});
		}
		else if (error == 1)
		{
			$(this).find("div.error_message").text("Проверьте, правильно ли вы заполнили поля");
		}
		else if (error == 2)
		{
			$(this).find("div.error_message").text("Введенные пароли не совпадают");
		}
		else if (error == 3)
		{
			$(this).find("div.error_message").text("Слишком короткий пароль");
		}
		return false;
	});
});
//Восстановление пароля - end

//Регистрация
$(function () {	
	//ФИО
	$("#reg_name").focusout(function(){
		checkName(this);
	});
	//Email
	$("#reg_email").focusout(function(){
		checkEmail(this);
	});
		
	//Телефон
	$("#reg_phone").focusout(function(){
		checkPhone(this);
		$("#show_code").parent().show();
		$('.code_field').hide();
		$('.submit_button input').show();
		$('.code_field .textbox').val('');
	});
	$("#reg_phone_check").focusout(function(){
		var name = $(this).val();
		if (name == false)
		{
			$(this).parent().addClass("error");
		}
		else
		{
			$(this).parent().removeClass("error");
		}
	});
	//Проверка телефона
	$("#show_code").click(function(){
		var regphone = /^[0-9\s\-\(\)\+]+$/;
		var phone = $("#reg_phone").val();
		if (phone != false && regphone.test(phone))
		{
			//отправка аякса
			var info = 'send_sms=y&phone='+phone;
			console.log(info);
			$.post('/includes/ajax.php', info, function(data){
				var check = parseInt(data);
				if(check == 1){
					$("#show_code").parent().hide();
					$('.code_field').show();
				}
				else{
					$("#phone_err_div").removeAttr("class").addClass("error_pic");
					$("#err_phone_text").addClass("error");
					$("#err_phone_text").text("Подтверждение данного номера телефона было менее суток назад");
					return false;
				}	
				console.log(data);
			});
		}
		else
		{
			$(this).parent().prev().addClass("error");
			$("#phone_err_div").removeAttr("class").addClass("error_pic");
			$("#err_phone_text").addClass("error");
			$("#err_phone_text").text("Некорректный телефон");
			return false;
		}
	});
	
	$("#check_code").click(function(){
		var code = $("#reg_phone_check").val();
		var phone = $("#reg_phone").val();
		if (code != false)
		{
			//отправка аякса
			var info = 'check_phone=y&check_code='+code+'&phone='+phone;
			$.post('/includes/ajax.php', info, function(data){
				var check = parseInt(data);
				if(check == 1){
					//проверка прошла успешно
					
					$("#phone_err_div").removeAttr("class").addClass("success_pic");
					var buttonOKstyle = $("#check_code").attr('style');
					$("#err_phone_text").removeClass("error");
					$("#err_phone_text").text("");
					$("#check_code").attr('style', buttonOKstyle+"display:none");
				}
				else{
					//ошибка проверки
					$("#phone_err_div").removeAttr("class").addClass("error_pic");
					$("#reg_phone_check").parent().addClass("error");
					$("#err_phone_text").addClass("error");
					$("#err_phone_text").text("Неверный проверочный код телефон");
				}	
			});
		}
		else
		{
			$("#reg_phone_check").parent().addClass("error");
			$("#phone_err_div").removeAttr("class").addClass("error_pic");
		}
	});
	
	//Пароль - не менее 6 символов, и хватит
	$("#reg_pass").focusout(function(){
		checkPass(this);
	});
	$("#reg_pass_check").focusout(function(){
		checkPassSecond(this);
	});
	//Отравка данных на регистрацию
	$("#registation_form").submit(function(){
		checkName($("#reg_name"));
		checkEmail($("#reg_email"));
		if($('#phone_err_div').hasClass('success_pic')){
			var success_pic = true;
		}
		checkPhone($("#reg_phone"));
		if(success_pic)
			$('#phone_err_div').addClass('success_pic');
		checkPass($("#reg_pass"));
		checkPassSecond($("#reg_pass_check"));
		$("#final_error_div").removeClass("error");
		$("#final_error_div").text("");
		var checkRules = $("#chkRules")[0].checked;
		var checkInfo = $("#chkInfo")[0].checked;
		var errDiv = $("#registation_form .error");

		
		if(checkRules){
			if (errDiv.length == 0)
			{
				var form = $(this).serialize();
				form = form + '&register=y&spam='+(checkInfo ? 1 : 0);
				if(success_pic)
					form = form + '&phone_ch=Y';
				console.log(form);
				$.post('/includes/ajax.php', form, function(data){
					if(data){
						console.log(data);
						window.location.reload();
					}
						
				});
			}
		}
		else
		{
			$("#final_error_div").addClass("error");
			$("#final_error_div").text("Необходимо соглашение с правилами портала");
		}
		return false;
	});
	
});

function checkName(e)
{
	var name = $(e).val();
	if (name == false)
	{
		$(e).parent().addClass("error");
		$(e).parent().next("div").removeAttr("class").addClass("error_pic");
		$("#err_name_text").addClass("error");
		$("#err_name_text").text("Поле должно быть заполнено");
	}
	else
	{
		$(e).parent().removeClass("error");
		$(e).parent().next("div").removeAttr("class").addClass("success_pic");
		$("#err_name_text").removeClass("error");
		$("#err_name_text").text("");
	}

}

function checkEmail(e)
{
	var regmail = /@/;
	var email = $(e).val();
	if (email == false)
	{
		$(e).parent().addClass("error");
		$(e).parent().next("div").removeAttr("class").addClass("error_pic");
		$("#err_mail_text").addClass("error");
		$("#err_mail_text").text("Поле должно быть заполнено");
	}
	else
	{
		if (!regmail.test(email))
		{
			$(e).parent().addClass("error");
			$(e).parent().next("div").removeAttr("class").addClass("error_pic");
			$("#err_mail_text").addClass("error");
			$("#err_mail_text").text("Некорректный E-Mail");
		}
		else
		{
			//проверка на повторение
			var info = 'check_email=y&email='+email;
			$.post('/includes/ajax.php', info, function(data){
				var check = parseInt(data);
				if(check == 1){
					//проверка прошла успешно
					$("#reg_email").parent().removeClass("error");
					$("#reg_email").parent().next("div").removeAttr("class").addClass("success_pic");
					$("#err_mail_text").removeClass("error");
					$("#err_mail_text").text("");
				}
				else{
					//ошибка проверки
					$("#reg_email").parent().addClass("error");
					$("#reg_email").parent().next("div").removeAttr("class").addClass("error_pic");
					$("#err_mail_text").addClass("error");
					$("#err_mail_text").text("Такой логин уже существует");
				}	
			});
		}
	}
}

function checkPhone(e)
{
	var regphone = /^[0-9\s\-\(\)\+]+$/;
	var phone = $(e).val();
	if (phone == false)
	{
		$(e).parent().addClass("error");
		$("#phone_err_div").removeAttr("class").addClass("error_pic");
		$("#err_phone_text").addClass("error");
		$("#err_phone_text").text("Поле должно быть заполнено");
	}
	else
	{
		if(!regphone.test(phone))
		{	
			$(e).parent().addClass("error");
			$("#phone_err_div").removeAttr("class").addClass("error_pic");
			$("#err_phone_text").addClass("error");
			$("#err_phone_text").text("Некорректный телефон");
		}
		else
		{
			$(e).parent().removeClass("error");
			$("#phone_err_div").removeAttr("class");
			$("#err_phone_text").removeClass("error");
			$("#err_phone_text").text("");
		}
	}
}

function checkPass(e)
{
	var pass_check = $("#reg_pass_check").val();
	var pass = $(e).val();
	if (pass == false)
	{
		$(e).parent().addClass("error");
		$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
		$("#err_pass_text").addClass("error");
		$("#err_pass_text").text("Введите пароль");
	}
	else
	{
		if (pass.length < 6)
		{
			$(e).parent().addClass("error");
			$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
			$("#err_pass_text").addClass("error");
			$("#err_pass_text").text("Пароль должен быть не менее 6 символов");
		}
		else
		{
			if (pass_check != false && pass != pass_check)
			{
				$(e).parent().addClass("error");
				$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
				$("#err_pass_text").addClass("error");
				$("#err_pass_text").text("Пароли не совпадают");
			}
			else
			{
				$(e).parent().removeClass("error");
				$(e).parent().parent().next("div").removeAttr("class").addClass("success_pic");
				$("#err_pass_text").removeClass("error");
				$("#err_pass_text").text("");
			}
		}
	}
}

function checkPassSecond(e)
{
	var pass = $("#reg_pass").val();
	var pass_check = $(e).val();
	if (pass_check == false)
	{
		$(e).parent().addClass("error");
		$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
		$("#err_pass_text").addClass("error");
		$("#err_pass_text").text("Введите подтверждение пароль");
	}
	else
	{
		if (pass_check.length < 6)
		{
			$(e).parent().addClass("error");
			$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
			$("#err_pass_text").addClass("error");
			$("#err_pass_text").text("Пароль должен быть не менее 6 символов");
		}
		else
		{
			if (pass != false && pass != pass_check)
			{
			
				$(e).parent().addClass("error");
				$(e).parent().parent().next("div").removeAttr("class").addClass("error_pic");
				$("#err_pass_text").addClass("error");
				$("#err_pass_text").text("Пароли не совпадают");
			}
			else
			{
				$(e).parent().removeClass("error");
				$(e).parent().parent().next("div").removeAttr("class").addClass("success_pic");
				$("#err_pass_text").removeClass("error");
				$("#err_pass_text").text("");
			}
		}
	}
}
//Регистрация - end