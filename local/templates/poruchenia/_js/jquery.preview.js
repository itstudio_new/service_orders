﻿/**
* ...
* @author ser
*/
(function($) {
    $.fn.preview = function(settings) {
        settings = jQuery.extend({

    }, settings);

        var obj = $(this);

        obj.find('.big_img a').hide();
        obj.find('.big_img a:eq(0)').show();

        obj.find('ul li a').click(function () {
            var n = $(this).parent('li').index();
            $(this).closest('.pic').find('.big_img a').hide();
            $(this).closest('.pic').find('.big_img a:eq(' + n + ')').show();
            return false;
        });
};
})(jQuery);    