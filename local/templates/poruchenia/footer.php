			 </div>
		   </div>
		<!--Эти 2 закрывают дивы content и fc -->
		</div>
    </div>
    <footer>
        <div class="f">
            <div class="fixed">
                <div class="flex flj flt">
                    <div>
                        <p class="copy">&copy; Сервис поручений,
                            <? if ( date('Y') == 2016 ){ ?>
								2016
							<? } else { ?>
								2016-<?=date('Y')?>
							<? } ?>
                        </p>
                        <div class="social">
                            <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/fs1.png"></a>
                            <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/fs2.png"></a>
                            <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/fs3.png"></a>
                        </div>
                    </div>
                    <div>
                        <?$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"bottom_menu",
							Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => array(0=>"",),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"ROOT_MENU_TYPE" => "footer_1",
								"USE_EXT" => "N"
							)
						);?>
                    </div>
                    <div>
                        <?$APPLICATION->IncludeComponent(
							"bitrix:menu", 
							"bottom_menu", 
							array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => array(
								),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"ROOT_MENU_TYPE" => "footer_2",
								"USE_EXT" => "N",
								"COMPONENT_TEMPLATE" => "bottom_menu"
							),
							false
						);?>
                    </div>
                    <div>
                        <div class="soft flex flj flt">
                            <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/appstore.png"></a>
                            <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/gplay.png"></a>
                        </div>
                        <a href="http://www.it-studio.ru" class="dev_link" target="_blank">Разработано в </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <a id="scrollUp" href="#">Наверх</a>
<div id="call_dialog" style="display: none">
        <h2>Заказать звонок</h2>
        <label>
            <input class="textbox" type="text" placeholder="Логин" value="">
        </label>
        <label>
            <input class="textbox" type="password" placeholder="Пароль" value="">
        </label>
        <div class="submit_button">
            <input type="submit" class="submit" id="product_button" value="Заказать">
            <input type="submit" class="close" value="Закрыть">
        </div>
</div>
<?include($_SERVER['DOCUMENT_ROOT'].'/includes/auth.php');?>
<div id="search_dialog" style="display: none">
        <h2>Добро пожаловать!</h2>
        <div class="city_confirm">
            <p style="font-size: 12px;"><strong>Вы находитесь в г. Иваново?</strong></p>
            <div class="submit_button">
                <a href="#">Да</a>
            </div>
        </div>
        <div class="or"><div class="text">или</div></div>
        <div class="inner">
            <div class="h3">Выберите другой регион</div>
            <div class="dadata_address">
			    <input id="address" name="address" type="text" size="100">
			    <link href="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/css/suggestions.css" type="text/css" rel="stylesheet">
			    <!--[if lt IE 10]>
			    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
			    <![endif]-->
			    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/js/jquery.suggestions.min.js"></script>
			    <script type="text/javascript">
			        $("#address").suggestions({
			            serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
			            token: "09d08b8bed5266658a946ab6eb5e7bd20791aa72",
			            type: "ADDRESS",
			            count: 5,
			            /* Вызывается, когда пользователь выбирает одну из подсказок */
			            onSelect: function (suggestion) {
			                console.log(suggestion);
			            }
			        });
			    </script>
		    </div>
        </div>
        <div class="submit_button_alt">
            <input type="submit" name="" id="" value="Сохранить">
        </div>
</div>


<div id="call_message" style="display: none">
	<h2 style="padding: 16px;"></h2>
	<div style="text-align:center;"></div>
        
</div>

<div id="call_ready_dialog" style="display: none">
	<h2>Вы хотите исполнить поручение?</h2>
	<div class="inner">
	</div>
</div>

<div id="call_make" style="display: none">
	<h2>Ваше предложение отправленно заказчику!</h2>
	<div class="inner">
		
	</div>
</div>

<div id="call_dialog_order_select" style="display: none"></div>
<div id="call_dialog_del" style="display: none"></div>
<div id="call_dialog_cancel" style="display: none"></div>
<div id="call_user_sign" style="display: none"></div>
	
</body>
</html>