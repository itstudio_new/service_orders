<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');
$arSelect = Array("ID", "IBLOCK_ID",  "DATE_CREATE", "NAME");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array("IBLOCK_ID"=>2, "ID" => $_POST["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
if($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields(); ?> 
	<h2>Отменить поручение?</h2>
	<div class="inner">
		<div class="name"><?=$arFields['NAME'];?></div>
		<div class="state">Поручение №<?=$arFields['ID'];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="date">от <?=$arFields['DATE_CREATE'];?></span></div>
	</div>
	<div class="submit_button" style="text-align: center; margin: 20px 0 0 0; ">
		<input type="submit" class="submit" onclick="cancel_poruchenie_y(<?=$arFields['ID'];?>); return false;" value="Да, отменить поручение">
		<input type="submit" class="close" value="Отмена">
	</div>
<?}?>
