<p style="line-height: 30px">
	<strong>Адреса доставки:</strong>
</p>
<div class="address_line">
	<div class="template" style="display: none;">
		  <span class="num" style="color: #41a3cb">_rnd</span> 
		  <div class="line">
			  <div class="dadata_address_make">
				<input id="address_rnd" name="address" type="text" size="100">
				<link href="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/css/suggestions.css" type="text/css" rel="stylesheet">
				<!--[if lt IE 10]>
				<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
				<![endif]-->
				<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/js/jquery.suggestions.min.js"></script>
				<script type="text/javascript">
					$("#address_rnd").suggestions({
						serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
						token: "09d08b8bed5266658a946ab6eb5e7bd20791aa72",
						type: "ADDRESS",
						count: 5,
						/* Вызывается, когда пользователь выбирает одну из подсказок */
						onSelect: function (suggestion) {
							getPlaceMark(suggestion.value);
						}
					});
				</script>
			</div>
			<a class="del" href="#"  ></a>
		  </div>
	</div>
	<ul class="sort_line" id="sortable">
	  <li>
		  <span class="num" style="color: #41a3cb">1.</span> 
		  <div class="line">
			  <div class="dadata_address_make">
				<input id="address1" name="address" type="text" size="100" value="<?=$arElement['PROPERTIES']['ADDRESS']['VALUE'][0];?>">
				<link href="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/css/suggestions.css" type="text/css" rel="stylesheet">
				<!--[if lt IE 10]>
				<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
				<![endif]-->
				<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/js/jquery.suggestions.min.js"></script>
				<script type="text/javascript">
					$("#address1").suggestions({
						serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
						token: "09d08b8bed5266658a946ab6eb5e7bd20791aa72",
						type: "ADDRESS",
						count: 5,
						/* Вызывается, когда пользователь выбирает одну из подсказок */
						onSelect: function (suggestion) {
							getPlaceMark(suggestion.value);
						}
					});
				</script>
			</div>
			<a class="del" href="#"  ></a>
		  </div>
	  </li>
	  <li>
		  <span class="num" style="color: #41a3cb">2.</span> 
		  <div class="line">
			  <div class="dadata_address_make">
				<input id="address2" name="address" type="text" size="100" value="<?=$arElement['PROPERTIES']['ADDRESS']['VALUE'][1];?>">
				<link href="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/css/suggestions.css" type="text/css" rel="stylesheet">
				<!--[if lt IE 10]>
				<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
				<![endif]-->
				<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/js/jquery.suggestions.min.js"></script>
				<script type="text/javascript">
					$("#address2").suggestions({
						serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
						token: "09d08b8bed5266658a946ab6eb5e7bd20791aa72",
						type: "ADDRESS",
						count: 5,
						/* Вызывается, когда пользователь выбирает одну из подсказок */
						onSelect: function (suggestion) {
							getPlaceMark(suggestion.value);
						}
					});
				</script>
			</div>
			<a class="del" href="#"  ></a>
		  </div>
	  </li>
	  <li>
		  <span class="num" style="color: #41a3cb">3.</span> 
		  <div class="line">
			  <div class="dadata_address_make">
				<input id="address3" name="address" type="text" size="100" value="<?=$arElement['PROPERTIES']['ADDRESS']['VALUE'][2];?>">
				<link href="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/css/suggestions.css" type="text/css" rel="stylesheet">
				<!--[if lt IE 10]>
				<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
				<![endif]-->
				<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/js/jquery.suggestions.min.js"></script>
				<script type="text/javascript">
					$("#address3").suggestions({
						serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
						token: "09d08b8bed5266658a946ab6eb5e7bd20791aa72",
						type: "ADDRESS",
						count: 5,
						/* Вызывается, когда пользователь выбирает одну из подсказок */
						onSelect: function (suggestion) {
							getPlaceMark(suggestion.value);
						}
					});
				</script>
			</div>
			<a class="del" href="#"  ></a>
		  </div>
	  </li>
	  <li>
		  <span class="num" style="color: #41a3cb">4.</span> 
		  <div class="line">
			  <div class="dadata_address_make">
				<input id="address4" name="address" type="text" size="100" value="<?=$arElement['PROPERTIES']['ADDRESS']['VALUE'][3];?>">
				<link href="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/css/suggestions.css" type="text/css" rel="stylesheet">
				<!--[if lt IE 10]>
				<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
				<![endif]-->
				<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/js/jquery.suggestions.min.js"></script>
				<script type="text/javascript">
					$("#address4").suggestions({
						serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
						token: "09d08b8bed5266658a946ab6eb5e7bd20791aa72",
						type: "ADDRESS",
						count: 5,
						/* Вызывается, когда пользователь выбирает одну из подсказок */
						onSelect: function (suggestion) {
							getPlaceMark(suggestion.value);	
						}
					});
				</script>
			</div>
			<a class="del" href="#"  ></a>
		  </div>
	  </li>
	  <?if(count($arElement['PROPERTIES']['ADDRESS']['VALUE']) > 4){
		foreach($arElement['PROPERTIES']['ADDRESS']['VALUE'] as $key => $adrr){
			if($key > 3){?>
				<li>
				  <span class="num" style="color: #41a3cb"><?=$key+1;?>.</span> 
				  <div class="line">
					  <div class="dadata_address_make">
						<input id="address<?=$key+1;?>" name="address" type="text" size="100" value="<?=$arElement['PROPERTIES']['ADDRESS']['VALUE'][$key];?>">
						<link href="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/css/suggestions.css" type="text/css" rel="stylesheet">
						<!--[if lt IE 10]>
						<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
						<![endif]-->
						<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/js/jquery.suggestions.min.js"></script>
						<script type="text/javascript">
							$("#address<?=$key+1;?>").suggestions({
								serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
								token: "09d08b8bed5266658a946ab6eb5e7bd20791aa72",
								type: "ADDRESS",
								count: 5,
								/* Вызывается, когда пользователь выбирает одну из подсказок */
								onSelect: function (suggestion) {
									getPlaceMark(suggestion.value);	
								}
							});
						</script>
					</div>
					<a class="del" href="#"  ></a>
				  </div>
			  </li>
			<?}
		}
	  }?>
	</ul>
	<div class="submit_button" style="text-align: left; margin-bottom: 20px;">
		<a href="#add_address" style="text-transform: none;">Добавить еще</a>
	</div>
</div>
