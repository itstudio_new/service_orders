<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade new">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td  >
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled style="display:none;">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}

				<button class="btn_del cancel"></button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>


<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">

{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"><img src="{%=file.url%}" style="width:130px; height:130px;"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td  >
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled style="display:none;">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            

				<button class="btn_del cancel" onclick="del_file('{%=file.url%}');"></button>
            
        </td>
    </tr>
{% } %}


</script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery.fileupload.js?<?=time();?>"></script>
<!-- The File Upload processing plugin -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/main.js?<?=time();?>"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="<?=SITE_TEMPLATE_PATH;?>/js/cors/jquery.xdr-transport.js"></script>
<![endif]-->
