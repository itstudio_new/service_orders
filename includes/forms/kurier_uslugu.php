 <?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
global $USER;
if ($USER->IsAuthorized()){
	$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$USER->GetID()),array("SELECT"=>array("UF_*"), 'FIELD' => array('NAME', 'ID')));
	$arUser = $rsUser->Fetch();
}?>

<form id="made_1">
		<label class="custom_checkbox avto" style="display: inline-block">
			<input type="checkbox" name="avto" <?if($arElement['PROPERTIES']['AVTO']['VALUE']){?>checked<?}?>>
			<span class="mtop_icon icon1">Нужен автомобиль</span>
		</label>
		<label class="custom_checkbox" style="display: inline-block">
			<input type="checkbox" name="srochnyi" <?if($arElement['PROPERTIES']['HOT']['VALUE']){?>checked<?}?>>
			<span class="mtop_icon icon2">Срочный заказ</span>
		</label>
	<div class="h5">В чем состоит поручение</div>
	<div class="label">
		<input class="textbox" type="text" name="name" value="<?=$arElement['NAME'];?>">
		<span class="error"></span>
	</div>
	<div class="h5">Опишите важные детали поручения</div>
	<div class="label">
		<textarea class="textbox" name="text"><?=$arElement['DETAIL_TEXT'];?></textarea>
		<span class="error"></span>
	</div>
</form>

<?include('templates/file_upload.php');?>

 <form id="made_2">	
	<label class="custom_checkbox gabarit_check">
		<input type="checkbox" <?if($arElement['PROPERTIES']['DLINA']['VALUE'] || $arElement['PROPERTIES']['SHIRINA']['VALUE'] || $arElement['PROPERTIES']['VYSOTA']['VALUE']){?>checked<?}?>>
		Габариты груза
	</label>
	<div id="gabarit_block" style="display: none;">
		<label class="label abs" style="display: inline-block; padding-left: 88px; width: 190px; margin-right: 20px;">
			<em><strong>Длина (см)</strong></em>
			<input id="size_dlina" class="textbox" type="text" name="dlina" value="<?=$arElement['PROPERTIES']['DLINA']['VALUE'];?>">
		</label>
		<label class="label abs" style="display: inline-block; padding-left: 88px; width: 190px; margin-right: 20px;">
			<em><strong>Ширина (см)</strong></em>
			<input id="size_width" class="textbox" type="text" name="shirina" value="<?=$arElement['PROPERTIES']['SHIRINA']['VALUE'];?>">
		</label>
		<label class="label abs" style="display: inline-block; padding-left: 88px; width: 190px; margin-right: 20px;">
			<em><strong>Высота (см)</strong></em>
			<input id="size_height" class="textbox" name="vysota" type="text" value="<?=$arElement['PROPERTIES']['VYSOTA']['VALUE'];?>">
		</label>
		<div class="popup">
			Возможно вам подойдет рубрика «Грузоперевозки»
			<div class="submit_button">
				<a href="#" id="perenos">Да, перенести мое объявление туда</a>
			</div>
			<a class="close" href="#"></a>
		</div>
	</div>
	<div class="label abs ves" style="padding-left: 88px;">
		<em style="top: 5px;"><strong>Вес груза:</strong></em>
		<label class="custom_radio"><input type="radio" name="ves" value="9" <?if($arElement['PROPERTIES']['VES']['VALUE_ENUM_ID'] == 9){?>checked<?}?>> до 3 кг </label>
		<label class="custom_radio"><input type="radio" name="ves" value="10" <?if($arElement['PROPERTIES']['VES']['VALUE_ENUM_ID'] == 10){?>checked<?}?>> от 3 до 10 кг </label>
		<label class="custom_radio"><input type="radio" name="ves" value="11" <?if($arElement['PROPERTIES']['VES']['VALUE_ENUM_ID'] == 11){?>checked<?}?>> свыше 10 кг </label>
	</div>

	 <label class="custom_checkbox meg" style="display: none;">
		<input type="checkbox" name="mejGorod" value="7" <?if($arElement['PROPERTIES']['MEJGOROD']['VALUE']){?>checked<?}?>>
		<span class="mtop_icon icon5">Междугородняя перевозка</span>
	</label>

	 <label class="custom_checkbox gruz" style="display: none;">
		<input type="checkbox" name="gruz" value="8" <?if($arElement['PROPERTIES']['GRUZCHIKI']['VALUE']){?>checked<?}?>>
		<span class="mtop_icon icon7">С грузчиками</span>
	</label>

	<div class="label abs resz" style="padding-left: 110px;">
		<em><strong>Срок доставки:</strong></em>
		<div class="date_input">
			<input class="textbox" name="date" type="text" value="<?=$arElement['PROPERTIES']['SROK']['VALUE']?>">
		</div>
		<div class="time_input">
			<input class="textbox mtime" name="time" type="text" value="<?=$arElement['PROPERTIES']['SROK_TIME']['VALUE'];?>">
		</div>
	</div>

	<?include('templates/address_block.php');?>


	<?include('templates/scripts.php');?>

<div class="product_map" id="map" style="width: 100%; height: 500px;"></div>


<div class="label abs resz" style="padding-left: 170px;">
	<em><strong>Сумма вознаграждения:</strong></em>
	<input class="textbox" style="max-width: 100px" name="price" type="text" value="<?=$arElement['PROPERTIES']['SUMMA']['VALUE'];?>"> руб.
</div>
<div class="row label abs resz" style="padding-left: 170px;">
	<em><strong>Контактный телефон:</strong></em>
	<div class="code_tel">
		<input check="<?=($arUser['UF_CHECK_PHONE'] ? 'Y' : 'N');?>" class="textbox fi fi6" type="tel" name="phone" id="reg_phone_made" value="<?=$arUser['PERSONAL_PHONE'];?>">
		<span id="err_phone_text" style="text-align:center"></span>
	</div>
	<?if($arUser['UF_CHECK_PHONE']){
		$display_none = 'display:none;';
		$_SESSION['check_phone_made'] = $arUser['PERSONAL_PHONE'];
	}?>
	<div class="submit_button" style="float: left; padding-left: 10px; <?=$display_none;?>">
		<input  id="show_code_made" type="button" style="text-transform: none; padding-top: 12px; padding-bottom: 12px;" value="Запросить код">
	</div>
	<div class="code_field" style="padding-left: 10px">
		<input class="textbox" style="max-width: 100px; float: left" type="text" name="phone_check" id="reg_phone_check_made" value="">
		<div class="submit_button">
			<input type="button" style="text-transform: none; padding-top: 12px; padding-bottom: 12px;" id="check_code_made" value="ОК">
		</div>
	</div>

	<div id="phone_err_div"></div>
</div>

	<input type="hidden" name="kat" value="<?=$_GET['kat'];?>" /> 
	<input type="hidden" name="sub" value="<?=$_GET['sub'];?>" /> 
	<?if($_GET['id'] && $arElement){?>
		<input type="hidden" name="id" value="<?=$_GET['id'];?>" /> 
	<?}?>
	<?if($_GET['red'] && $arElement){?>
		<input type="hidden" name="red" value="Y" /> 
	<?}?>
<div class="product_bottom">
	<div class="submit_button_alt">
		<input type="submit"  style="text-transform: none;" value="Опубликовать поручение" >
	</div>
</div>

</form>

<?include('templates/file_upload_scripts.php');?>
 <script type="text/javascript">
	$(document).ready(function(){
		
		$('body').on('focusout', '#size_height, #size_width, #size_dlina', function () {
			if($('input[name=kat]').val() == 1){
				if($(this).val() >= 50){
					$('.popup').show();
				}
			}
		});

		$('body').on('click', '.popup .close', function () {
			$('.popup').hide();
			return false
		});
	});
	
	function sendForm(file){
		
		error = 0;
		errorMess = '';
		var regmail = /@/;
		var regphone = /^[0-9\s\-\(\)\+]+$/;
		
		$("#made_1 .error").removeClass('error');
		var name = $("#made_1").find('input[name="name"]');
		var text = $("#made_1").find('textarea[name="text"]');
		
		if(name.val() == false){
			   name.closest('.label').addClass('error');
			   error = 1;
		}
		if(text.val() == false){
			   text.closest('.label').addClass('error');
			   error = 2;
		}
		
		$("#made_2 .error").removeClass('error');
		var date = $("#made_2").find('input[name="date"]');
		var time = $("#made_2").find('input[name="time"]');
		var price = $("#made_2").find('input[name="price"]');
		var phone = $("#made_2").find('input[name="phone"]');
	
		if(date.val() == false){
			   date.closest('.label').addClass('error');
			   error = 1;
		}
		if(time.val() == false){
			   time.closest('.label').addClass('error');
			   error = 1;
		}
		if(price.val() == false){
			   price.closest('.label').addClass('error');
			   error = 1;
		}
		if(phone.val() == false){
			   phone.closest('.label').addClass('error');
			   error = 1;
		} else {
			console.log(phone.attr('check'));
			if(phone.attr('check') != 'Y'){
				errorMess = 'Не подтвержден телефон<br>';
				error = 1;
			}
		}
		
		var form_1 = $('#made_1').serialize();
		var form_2 = $('#made_2').serialize();
		var form = form_1 + '&' + form_2;
		if(file == 'Y'){
			form = form + '&file=Y';
		}
		var i = 0;
		$('input[name=address]').each(function(){
			if($(this).val()){
				form = form + '&address['+i+']='+$(this).val();
				i = i + 1;
			}
		});
				
		if(i == 0){
			errorMess = errorMess + 'Укажите адрес доставки<br>';
			error = 1;
		}
		
		if(errorMess){
			$('#call_message h2').text('Ошибка');
			$('#call_message div').html(errorMess);
			var call_message = $('#call_message').customDialog();
			call_message.show();
		}
		
		if(error == 0){
			$.post('/includes/made.php', form, function(data){
				if(data){
					//window.location.href=data;
					console.log(data);
				}
			});	
		}
		
	};
		
		
</script>