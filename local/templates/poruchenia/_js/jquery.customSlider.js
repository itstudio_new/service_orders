﻿/**
* ...
* @author ser
* effect -planking
*        -boxsize
*/
(function($) {
    $.fn.customSlider = function (settings) {
        settings = jQuery.extend({
            pause: 6000,
            pager: true,
            arrows: false,
            resize: false,
            width: 1920,
            effect: { "name": "default", "parts": 0, "speed": 1000 }
    }, settings);

        var obj = $(this);
        var slides = obj.find('li');
        var speed = settings.effect.speed;
        var pause = settings.pause;
        var auto = true;
        var pager = settings.pager;
        var arrow = settings.arrows;
        var current = 0;
        var minWidth = settings.width;

        var po;
        var timeoutId;
        var _an;

        getEffect("create");
        
        if (auto) show(current);
        if (pager && slides.length > 1) createPager();
        if (arrow) createArrow();


        function getEffect(type, args) {
            var _s = settings.effect.name;
            var name = type + _s.charAt(0).toUpperCase() + _s.substr(1);
            if (args == undefined) {
                eval(name)();
            } else {
                eval(name)(args);
            }
        }


        function createPager() {
            obj.parent().append('<ul class="slider_pager"></ul>');
            po = obj.parent().find('.slider_pager');
            obj.children('li').each(function (i) {
                po.append('<li><a href="#">'+ i +'</a></li>');
            });
            setPager(0);
            po.find('a').click(function () {
                clearInterval(_an);
                clearTimeout(timeoutId);
                getEffect("end", current);
                current = $(this).parent().index();
                setPager(current)
                show(current);
                return false;
            });
            
        }

        function createArrow() {
            obj.parent().append('<a href="#" class="prev"></a><a href="#" class="next"></a>');
            obj.parent().find('a.next').click(function () {
                arrowClick(true);
                return false;
            });
            obj.parent().find('a.prev').click(function () {
                arrowClick(false);
                return false;
            });
        }

        function arrowClick(forward) {
            clearInterval(_an);
            clearTimeout(timeoutId);
            getEffect("end", current);
            var next = (forward) ? getNext(current) : next = getPrev(current);
            setPager(next);
            show(next);
            current = next;
        }

        obj.swipe({
            swipe: function (event, direction) {
                if (direction === 'left' || direction === 'up') {
                    var next = getNext(current);
                    setPager(next);
                    show(next);
                    current = next;
                } else {
                    var next = getPrev(current);
                    setPager(next);
                    show(next);
                    current = next;
                }
            }
        });

        function show(index) {
            getEffect("state", index);
            slides.css({ 'z-index': 1 });
            slides.eq(index).css({ 'z-index': 2 })
            if (slides.eq(index).find('img').attr('src') != undefined) {
                if (slides.eq(index).find('img').attr('src').indexOf(".gif") >= 0) {
                    var temp = slides.eq(index).find('img').attr('src');
                    d = new Date();
                    slides.eq(index).find('img').remove();
                    var img = new Image();
                    img.src = temp + "?" + d.getTime();
                    slides.eq(index).append(img);
                }
            }

            getEffect("animate", index);
            setPager(index);
        }

        function delAttr_rcl(prmName, val) {
            var res = '';
            var d = val.split("?");
            var base = d[0];
            return base;
        }

        function getRandomArbitary(min, max) {
            return Math.floor( Math.random() * (max - min) + min );
        }

        function setPager(index) {
            if (po != undefined && index != undefined) {
                po.find('li').removeClass('current');
                po.find('li:eq(' + index + ')').addClass('current');
            }
        }

        function getNext(index) {
            return (index < slides.length - 1) ? index+1 : 0;
        }

        function getPrev(index) {
            return (index > 0) ? index - 1 : slides.length - 1;
        }
      
        function setPause() {
            var index = current;
            clearTimeout(timeoutId);
            current = getNext(index);
            if (auto && slides.length > 1) show(current);
        }

        getEffect("fixed");


        //default
        function createDefault(index) {
        }
        function stateDefault(index) {
        }
        function endDefault(index) {
        }
        function animateDefault(index) {
            slides.eq(index).fadeIn(speed, function (e) {
                timeoutId = setTimeout(setPause, pause);
                if (slides.length > 1) slides.eq(getPrev(index)).hide();
            });
        }
        function fixedDefault() {
            if (settings.resize) {
                obj.find('img').css({ 'width': '100%', 'height': 'auto' });
                var _h = Math.round($(window).width() * (parseInt(obj.find('img').attr('data-height')) / parseInt(obj.find('img').attr('data-width'))));
                $('.custom_slider').css('height', _h);
            } else {
                if ($('body').width() < minWidth) {
                    obj.find('img').css('margin-left', Math.round(($('body').width() - minWidth) / 2));
                } else {
                    obj.find('img').removeAttr('style');
                }
                var _l = ($('body').width() - 1100) / 2;
                _l = _l > 0 ? _l : 0;
                slides.find('.description').css('left', _l);

                var _r = (($('body').width() - 1100) / 2) - 10;
                _r = _r > 0 ? _r : -10;
                if (po != undefined) po.css('right', _r);
            }
        }
        

        //zoom
        function createZoom(index) {
        }
        function stateZoom(index) {
            var next = getNext(index);

            slides.eq(next).find('img').css({ 'scale': '1.2', 'opacity': '0' });
            slides.find('.description').hide();
            slides.eq(index).find('img').css({ 'scale': '1.2', 'opacity': '0' });

            var _d = Math.floor(parseInt(slides.eq(index).find('img').attr('data-height'))/10);
            if ((index % 4 + 1) == 1) {
                slides.eq(index).find('img').css({ 'left': (0 - _d) + 'px', 'top': (0 - _d) +'px' });
            }
            if ((index % 4 + 1) == 2) {
                slides.eq(index).find('img').css({ 'left': _d + 'px', 'top': (0 - _d) + 'px' });
            }
            if ((index % 4 + 1) == 3) {
                slides.eq(index).find('img').css({ 'left': (0 - _d) + 'px', 'top': _d + 'px' });
            }
            if ((index % 4 + 1) == 4) {
                slides.eq(index).find('img').css({ 'left': _d + 'px', 'top': _d + 'px' });
            }
        }
        function endZoom(index) {
            slides.eq(index).find('img').css({ 'scale': '1', left: '0', top: '0', 'opacity': '1' });
        }
        function animateZoom(index) {

            slides.eq(index).find('img').stop().transition({ scale: 1, top: 0, left: 0, opacity: 1 }, settings.effect.speed, 'ease', function () {
                timeoutId = setTimeout(setPause, pause);
                //if (slides.length > 1) slides.eq(getPrev(index)).hide();
            });

        }
        function fixedZoom() {
            fixedDefault();
        }

        //planking
        function createPlanking() {
            slides.each(function (j) {
                var _fon = slides.eq(j).find('img').attr('src');
                var _h = Math.round( slides.height() / settings.effect.parts );
                for (var i = 0; i < settings.effect.parts; i++) {
                    slides.eq(j).append('<div class="part" style="top: ' + (i * _h) + 'px; height: ' + _h + 'px; background: url(' + _fon + ') no-repeat 50% ' + (0 - (i * _h)) + 'px;"></div>');
                }
            });
        }
        function statePlanking(index) {
            var next = getNext(index);
            slides.eq(next).find('.part').css({ 'scaleX': '0' });
            slides.find('.description').hide();
            slides.eq(index).find('.part').css({ 'scaleX': '0' });
            
        }
        function endPlanking(index) {
            slides.eq(index).find('.part').css({ 'scaleX': '1' });
        }
        function animatePlanking(index) {
            var el = slides.eq(index).find('.part');
            clearInterval(_an);
            var _i = 0;
            part = el.eq(_i);
            function anim() {
                part.stop().transition({ scaleX: 1 }, settings.effect.speed, 'ease');
                _i++;
                part = el.eq(_i);
                if (_i == settings.effect.parts) {
                    slides.eq(current).find('.description').fadeIn(settings.effect.part_speed);
                    clearInterval(_an);
                    timeoutId = setTimeout(setPause, pause);
                }
            }
            _an = setInterval(anim, settings.effect.part_speed)
        }
        function fixedPlanking() {
            fixedDefault();
        }

        //boxsize
        function createBoxsize () {
            settings.effect.parts = (parseInt(settings.effect.parts) < 4) ? 4 : settings.effect.parts;
            var _w = parseInt(slides.find('img').attr("data-width")) / settings.effect.parts;
            var _sh = parseInt(slides.find('img').attr("data-height"));
            var _rCount = ((Math.floor(_sh / _w)) + 1);
            var _h = _sh / _rCount;
            
            slides.each(function (j) {
                var _fon = slides.eq(j).find('img').attr('src');
                for (var k = 0; k < _rCount; k++) {
                    var _t = k * _h;
                    for (var i = 0; i < settings.effect.parts; i++) {
                        var _l = i * _w;
                        slides.eq(j).append('<div class="part" style="top: ' + _t + 'px; left: '+ _l +'px; width: ' + _w + 'px; height: ' + _h + 'px; background: url(' + _fon + ') no-repeat ' + (0 - (i * _w)) + 'px ' + (0 - (k * _h)) + 'px;"></div>');
                    }
                }
            });
        }
        function stateBoxsize(index) {
            var next = getNext(index);
            slides.eq(next).find('.part').css({ 'scale': '0' });
            slides.eq(next).find('.part').removeClass('rnd');
            slides.find('.description').hide();
            slides.eq(index).find('.part').css({ 'scale': '0' });
            slides.eq(index).find('.part').removeClass('rnd');
        }
        function endBoxsize(index) {
            slides.eq(index).find('.part').css({ 'scale0': '1' });
        }
        function animateBoxsize(index) {
            var el = slides.eq(index).find('.part');
            var _part_count = slides.eq(index).find('.part').length;
            clearInterval(_an);
            var _i = 0;
            part = el.random();
            part.addClass('rnd');
            function anim() {
                part.stop().transition({ 'scale': '1' }, settings.effect.speed, 'ease');
                _i++;
                part = el.not('.rnd').random();
                part.addClass('rnd');
                if (_i == _part_count) {
                    slides.eq(current).find('.description').fadeIn(settings.effect.part_speed);
                    clearInterval(_an);
                    timeoutId = setTimeout(setPause, pause);
                }
            }
            _an = setInterval(anim, settings.effect.part_speed / 4);
        }
        function fixedBoxsize() {
            fixedDefault();
        }
       
        $(window).resize(function () {
            getEffect("fixed");
        });

        
};
})(jQuery);

$.fn.random = function () {
    return this.eq(Math.floor(Math.random() * this.length));
}