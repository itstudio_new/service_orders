<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$arPoint["type"] = "FeatureCollection";
$i = 1;
foreach($_GET as $addr){
	$file = file_get_contents('https://geocode-maps.yandex.ru/1.x/?format=json&geocode='.$addr);
	$file = json_decode($file);
	//echo $file;

	$file = object_to_array($file);
	$coord = $file['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
	$arCoord = explode(' ', $coord);

	
	$arPoint["features"][] = array(
		"type" => "Feature",
		"geometry" => array(
			"type" => "Point",
			"coordinates" => array(
				0 => $arCoord[1],
				1 => $arCoord[0]
			)
		),
		"properties" => array(
			"balloonContent" => $addr,
			"hintContent" => $addr,
			"iconContent" => $i
		),
		"options"=> array(
			
			 "preset" => "twirl#blueStretchyIcon"
			
		)
		
	);
	$i++;

}


echo json_encode($arPoint);
//print_r ($arPoint);


?>

<?/*

"iconLayout" => 'default#image',
			"iconImageHref"=> '/local/templates/poruchenia/images/err.png',
			 "iconImageSize" => array(
				0 => 8,
				1 => 18
			 )
			 
*/?>