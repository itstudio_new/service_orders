﻿$(function () {

    var obj = $(this);

    $(".openImg").fancybox({
        padding: 0,
        helpers: {
            overlay: { locked: false },
            title: { type: 'inside' },
            buttons: {}
        },
        afterShow: function () {
            $('.fancybox-wrap').swipe({
                swipe: function (event, direction) {
                    if (direction === 'left' || direction === 'up') {
                        $.fancybox.prev(direction);
                    } else {
                        $.fancybox.next(direction);
                    }
                }
            });
        }
    });

    window.forms = $('body').forms();

    
    

    /*forms.select_change = function(select) {
        if (select.attr('id') == "sel1") {
            $('#test').html("");
            $('#test').append('<div class="custom_select"><select id="sel2" data-placeholder="Выберите значения 2"><option>Раз</option><option>Два</option><option>Три</option><option>Четыре</option><option>Пять</option></select></div>');
            var newSel = $('#test').find('.custom_select');
            forms.select_init(newSel);
            alert(select.find('option:selected').attr('id'))
        }
    }*/

    $('.product .pic').preview();
    $('.slider').customSlider({
        effect: { "name": "zoom", "parts": 6, "speed": 2000, "part_speed": 200 }
        
    });
    $('input[type="tel"]').mask("(999) 999-9999");
    $('.mtime').mask("99:99");

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) { $("#scrollUp").fadeIn() } else { $("#scrollUp").hide() }
    });
    $("#scrollUp").click(function (e) {
        $("body,html").animate({ scrollTop: 0 }, 400);
        return false;
    });

    $('.navbar-toggle').on('click', function () {
        var _o = $(this);
        if (_o.hasClass('open')) {
            $(_o.attr('data-target')).hide();
            _o.removeClass('open')
        } else {
            $(_o.attr('data-target')).slideDown(500, function () { _o.addClass('open') });
        }
        return false;
    });

    
    function caruselInit() {
        $('.preview_carousel').jcarousel();
        if ($('.preview_carousel li').length > 3) {
            $('.prod_preview .jcarousel-control-prev').jcarouselControl({ target: '-=1' });
            $('.prod_preview .jcarousel-control-next').jcarouselControl({ target: '+=1' });
        } else {
            $('.prod_preview .jcarousel-control-prev').hide();
            $('.prod_preview .jcarousel-control-next').hide();
        }


        $('.prod_preview').swipe({
            swipe: function (event, direction) {
                if (direction === 'left' || direction === 'up') {
                    $('.preview_carousel').jcarousel('scroll', '+=1');
                } else {
                    $('.preview_carousel').jcarousel('scroll', '-=1');
                }
            }
        });

    }
    caruselInit();


    function zoomInit() {
        $('.zoom_container').imagezoomsl();

        $('.zoom_foto').click(function () {
            $('.zoom_foto').parent('li').removeClass('active');
            $(this).parent('li').addClass('active');
            var that = this;
            $('.zoom_container').fadeOut(600, function () {
                $(this).attr("src", $(that).attr("data-med"))
                       .attr("data-large", $(that).attr("data-large"))
                       .fadeIn(1000);
            });
        });
    }


    $(".rating.readonly").rate({
        selected_symbol_type: 'image',
        max_value: 5,
        step_size: 0.5,
        readonly: true,
        symbols: {
            image: {
                base: '<div class="star">&nbsp;</div>',
                hover: '<div class="star_hover">&nbsp;</div>',
                selected: '<div class="star_selected">&nbsp;</div>',
            },
        }
    });
    $(".rating:not('.readonly')").rate({
        selected_symbol_type: 'image',
        max_value: 5,
        step_size: 0.5,
        readonly: false,
        symbols: {
            image: {
                base: '<div class="star">&nbsp;</div>',
                hover: '<div class="star_hover">&nbsp;</div>',
                selected: '<div class="star_selected">&nbsp;</div>',
            },
        }
    });
    $(".rating").on("change", function (ev, data) {
        console.log(data.from, data.to);
    });

    $(".rating_big.readonly").rate({
        selected_symbol_type: 'image',
        max_value: 5,
        step_size: 0.5,
        readonly: true,
        symbols: {
            image: {
                base: '<div class="starb">&nbsp;</div>',
                hover: '<div class="starb_hover">&nbsp;</div>',
                selected: '<div class="starb_selected">&nbsp;</div>',
            },
        }
    });
    $(".rating_big:not('.readonly')").rate({
        selected_symbol_type: 'image',
        max_value: 5,
        step_size: 0.5,
        readonly: false,
        symbols: {
            image: {
                base: '<div class="starb">&nbsp;</div>',
                hover: '<div class="starb_hover">&nbsp;</div>',
                selected: '<div class="starb_selected">&nbsp;</div>',
            },
        }
    });
    $(".rating_big").on("change", function (ev, data) {
        console.log(data.from, data.to);
    });



    $('body').on('click', '.rating_sign em.plus', function () {
        var _i = $(this).parent('.rating_sign').find('input');
        var _v = parseInt(_i.val());
        if (_v < 5) _i.val( _v+1 + ",0" );
    })
    $('body').on('click', '.rating_sign em.minus', function () {
        var _i = $(this).parent('.rating_sign').find('input');
        var _v = parseInt(_i.val());
        if (_v > 0) _i.val(_v - 1 + ",0");
    })
    $('body').on('click', '.rating_sign_cnt a', function () {
        var _i = $(this).parent('.rating_sign_cnt').find('input');
        _i.val("0,0");
        return false;
    })

    function fixed() {
        if ($(window).width() > 768) {
            $('.navbar-toggle').removeClass('open');
            $('.collapse').show();
        }
    }
    fixed()

    $(window).resize(function () {
        fixed();
    })


    

    //$('.timer').timer({ date: new Date(2017, 1, 10, 14, 0, 0) });
    //new Date(year, month, date, hours, minutes, seconds, ms)

	/*
    $('body').on('click', '.user_cat_scroll ul>li>ul>li a', function () {
        var _t = $(this).closest('ul').parents('li').children('a').text() + " / " + $(this).text()
		var id = $(this).attr('data-id');
        $('.user_cat_select').append('<div class="item" data-section="'+id+'">' + _t + '<a class="del" href="#"></a></div>');
        return false;
    });
	*/

    $('body').on('click', '.user_cat_select a', function () {
        $(this).parents('.item').remove();
        return false;
    })

    $(".user_cat_scroll").mCustomScrollbar({
        axis: "y",
        theme: "inset",
        scrollButtons: {
            enable: true
        },
    });


    $('[id^="call_"]').each(function () {
        var dialog;
        if ($(this).attr('id') == "call_enter") {
            dialog = $(this).customDialog({ width: 380 });
        } else {
            if ($(this).attr('id') == "call_user_sign" || $(this).attr('id') == "call_user_review" || $(this).attr('id') == "call_dialog_cancel") {
                dialog = $(this).customDialog({ width: 430, addClass: "white_dialog" });
            } else {

                if ($(this).attr('id') == "call_user_end" || $(this).attr('id') == "call_dialog_del") {
                    dialog = $(this).customDialog({ width: 460, addClass: "blue_dialog" });
                } else {
                    if ($(this).attr('id') == "call_dialog_order_select") {
                        dialog = $(this).customDialog({ width: 630, addClass: "white_dialog" });
                    } else {
                        dialog = $(this).customDialog();
                    }
                }
                    
                
            }
        }
        $('[href="#'+$(this).attr('id')+'"]').click(function () {
            dialog.show();
            return false;
        })
    })


    var ready_dialog = $('#call_ready_dialog').customDialog();
    $('#call_ready_link').click(function () {

        
        ready_dialog.show();
        return false;
    })
    
    $('#button_call_make').click(function () {
        ready_dialog.close();
        var call_dialog = $('#call_make').customDialog();
        call_dialog.show();
        return false;
    })



    /*$('body').on('click', '.city', function () {
        var dialog = $('#search_dialog').customDialog({width: 380});
        dialog.show();
        return false;
    })*/


    $('.map_win').click(function () {
        $($(this).attr('data-href')).show();
        return false;
    })

    $('.map_bottom .close').click(function () {
        $(this).closest('.map_bottom').hide();
        return false;
    })



    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '',
        nextText: '',
        currentText: 'Сегодня',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
        'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
        'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);


    $('.tooltips').tooltip({
        close: function (event, ui) { $('.ui-helper-hidden-accessible').remove(); }
    });

    //$(".ui-tooltip-content").parents('div').remove();

    if ($(window).width() > 768) {
        $('.date_input input').datepicker({
            minDate: -0, maxDate: "+1M +10D"
        });
        $('.date_input').click(function(){ $('.date_input input').datepicker("show"); return false }); 
    } else {
        $('.date_input input').attr('type', 'date');
    }
    
    if ($('.gabarit_check').find('input:checked').length == 1) {
        $('#gabarit_block').show();
    }
    $('body').on('focusout', '#size_height', function () {
        $('.popup').show();
    });

    $('body').on('click', '.popup .close', function () {
        $('.popup').hide();
        return false
    })
    /*
    $("#sortable").sortable();
    $("#sortable").sortable({
        handle: '.num',
        stop: function () {
            //var order = $("#sortable").sortable("serialize", { key: 'order[]' });
            //console.log(order);

            $('#sortable li').each(function (index) {
                $(this).find('.num').text(index + 1);
            })
        },
        
    });*/

    $('body').on('mouseenter', 'header .user',
        function () {
            $('.user_submenu').show();
        });
    $('body').on('mouseleave', 'header .user',
        function () {
            $('.user_submenu').hide();
        });

    $('body').on('click', '[href="#add_address"]', function () {
        _temp = $('.address_line .template').html();
        tmp_click = $('#sortable li').length + 1;
        _temp = _temp.replace(/_rnd/g, tmp_click);
        $('.address_line ul.sort_line').append('<li>' + _temp + '</li>');
        tmp_click++;;
    })
    /*
    $('body').on('click', '.sort_line li .del', function () {
        $(this).closest('li').remove();
        $('#sortable li').each(function (index) {
            $(this).find('.num').text(index + 1);
        })
        return false;
    })
	*/
    $('body').on('click', '#show_code', function () {
        $(this).parent().hide();
        $('.code_field').show();
    })

    
    $('body').on('click', '.show_detail a', function () {
        if ($(this).hasClass('active')) {
            $('.detail_info').hide();
            $(this).text($(this).attr('data-text'));
            $(this).removeClass('active');
        } else {
            $('.detail_info').show();
            $(this).text($(this).attr('data-alt'));
            $(this).addClass('active');
			ymaps.ready(init);
        }
        return false;
    })

	$('.left_filter>ul>li>ul>li').click(function () {
		if ( $(this).closest('ul').find('input:checked').length <= 1 )
		{
			$(this).closest('ul').hide();
			$(this).closest('ul').parent('li').children('.custom_small_checkbox').children('input').prop('checked', false);
			$(this).closest('ul').parent('li').children('.custom_small_checkbox').removeClass('active');
		}
    });
	$('.left_filter ul li>input:checked').each(function(){
		$(this).children('ul').show();
	})
	$('.left_filter ul li:has(ul input:checked)').each(function(){
		$(this).children('ul').show();
	})
	$('.left_filter>ul>li>.custom_small_checkbox').click(function(){
		if ($(this).hasClass('active')) {
			$(this).parent('li').find('ul').hide();
			$(this).parent('li').find('input').prop('checked', false);
			$(this).parent('li').find('label').removeClass('active');
		} else {
			$(this).parent('li').find('ul').show();
			$(this).parent('li').find('input').prop('checked', true);
			$(this).parent('li').find('label').addClass('active');
		}
		return false;
	})
	
    /*$('[href="#add_basket"]').click(function () {
        var d = $('body').customDialog({
            width: 1000, onLoad: function () {
                $('body').forms();
                $('.product .pic').preview();
                caruselInit();
            }
        });
        d.load("window.html");

        return false;
    })ж*/

    //zoomInit();
    //$('.product_list li .item').css('height', Math.max.apply(null, $('.product_list li').map(function () { return $(this).find('.item').innerHeight() }).get()));

});