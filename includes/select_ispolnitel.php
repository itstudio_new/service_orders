<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');
$arSelect = Array("ID", "IBLOCK_ID", 'DETAIL_TEXT', "IBLOCK_SECTION_ID", "DATE_CREATE", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*", "TIMESTAMP_X");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array("IBLOCK_ID"=>5, "ID" => $_POST["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
if($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	$arFields['PROPERTIES'] = $ob->GetProperties();
	$arResult = $arFields;
	
	$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array('ID' => $arResult['PROPERTIES']['ISPOLNITEL']['VALUE']), array("SELECT"=>array("UF_*"), 'FIELD' => array('NAME', 'ID')));
	if($arUser = $rsUser->Fetch()){
		$arResult['USER'] = $arUser;
	}	?>
	
	<h2>Вы хотите назначить исполнителя?</h2>
	<div class="inner">
		<h3 class="user_name"><?=$arResult['USER']['NAME'];?></h3>
		<p style="text-align: center"><strong>Поручение №<?=$arResult['PROPERTIES']['PORUCHENIE']['VALUE'];?></strong></p>
		<br>
		<div class="row">
			<p>Обратите внимание!</p>
			<p>
				1. Назначенный исполнитель получит уведомление о том, что Вы выбрали его<br>
				2. Назначенный исполнитель получит Вашу контактную информацию.
				3. Исполнитель назначается один раз, Вы не сможете сменить исполнителя
			</p>
		</div>
		<div class="submit_button" style="text-align: center; margin: 20px 0">
			<input type="submit" class="submit" style="text-transform: none;" onclick="select_ispolnitel_y(<?=$arResult['USER']['ID'];?>, <?=$arResult['PROPERTIES']['PORUCHENIE']['VALUE'];?>); return false;" value="ДА, назначить исполнителя">
			<input type="submit" class="close" style="text-transform: none;" value="Отмена">
		</div>
	</div>
<?}?>
