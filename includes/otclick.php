<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock'); 
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>2, "ID" => 140, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
if($ob = $res->GetNextElement()){ 
	$arResult = $ob->GetFields();  
	$arResult['PROPERTIES'] = $ob->GetProperties();
}
?>


		
		<h3 class="user_title"><?=$arResult['NAME'];?></h3>
		<div class="flex flj" style="margin-bottom: 0px; max-width: 586px">
			<div class="make_num">Поручение №<?=$arResult['ID'];?></div>
			<?$date1 = $arResult['PROPERTIES']['SROK']['VALUE'].' '.$arResult['PROPERTIES']['SROK_TIME']['VALUE'];
						
			$date1 = strtotime($date1);
			$date2 = time();
			$date3 = $date1 - $date2;
			$dni = $date3 / 86400;
			$sec = $date3 % 60;
			$chas = floor($date3 / 60);
			$min = $chas % 60;
			$chas = floor($chas / 60);
			$dni = round($dni);
			$chas = str_pad($chas, 2, '0', STR_PAD_LEFT);
			$min = str_pad($min, 2, '0', STR_PAD_LEFT);
			$sec = str_pad($sec, 2, '0', STR_PAD_LEFT);
			?>
			<?if($chas >= 72){?>
				<div class="time"><?echo $dni;?><?echo endingsForm($dni, 'день', 'дня', 'дней');?></div>
			<?} else {?>
				<div class="time">
					<?if($chas <= 3){?><strong><?}?>
					<?echo $chas . ":" . $min . ":" . $sec;?>
					<?if($chas <= 3){?></strong><?}?>
				</div>
			<?}?>
			<div class="price_big" style="top: 2px"><em>P<em>&mdash;</em></em><?=number_format($arResult['PROPERTIES']['SUMMA']['VALUE'], 0, ' ', ' ');?> </div>
		</div>
		<div>
			<ul class="ibl mb_menu" style="display: inline-block; margin-bottom: 20px;">
				<?if($arResult['PROPERTIES']['AVTO']['VALUE']){?>
					<li class="tooltip" title="Нужен автомобиль"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon1.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
				<?if($arResult['PROPERTIES']['HOT']['VALUE']){?>
					<li class="tooltip" title="Срочный заказ"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon2.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
				<?if($arResult['PROPERTIES']['GRUZCHIKI']['VALUE']){?>
					<li class="tooltip" title="Нужны грузчики"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon7.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
				<?if($arResult['PROPERTIES']['MEJGOROD']['VALUE']){?>
					<li class="tooltip" title="Междугородняя перевозка"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon5.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
				<?if($arResult['PROPERTIES']['FOTO']['VALUE']){?>
					<li class="tooltip" title="Нужно фото"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon6.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
				<?if($arResult['PROPERTIES']['KUPIT']['VALUE']){?>
					<li class="tooltip" title="Совершить покупку"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon4.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
			</ul>
		</div>
		
		<?if($arResult['PROPERTIES']['ADDRESS']['VALUE']){?>
			<strong>Адреса доставки:</strong>
			<ul class="adr_list">
			<?foreach($arResult['PROPERTIES']['ADDRESS']['VALUE'] as $key => $address){?>
				<li><span><?=$key+1;?>.</span> <?=$address;?></li>
			<?}?>
			</ul>
		<?}?>
		
		<form id="otclick">
			<input type="hidden" name="otclick" value="on">
			<input type="hidden" name="ID" value="<?=$arResult['ID'];?>">
			<input type="hidden" name="NAME" value="<?=$arResult['NAME'];?>">
			<input type="hidden" name="ZAKAZCHIK" value="<?=$arResult['PROPERTIES']['USER']['VALUE'];?>">
			<input type="hidden" name="DETAIL_PAGE_URL" value="<?=$arResult['DETAIL_PAGE_URL'];?>">
			<label>
				<textarea class="textbox textbox2" style="height: 230px" name="text" placeholder="Вы можете оставить свой комментарий для заказчика"></textarea>
			</label>
			<div class="submit_button" style="margin-bottom: 20px">
				<input id="button_call_make" style="text-transform: none; margin-bottom: 10px" type="submit" value="Да, я Беру поручение">
				<input style="text-transform: none; margin-bottom: 10px" class="close" type="submit" value="Отмена">
			</div>
		</form>
		</div>
</div>
