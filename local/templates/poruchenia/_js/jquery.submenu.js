﻿/**
* ...
* @author ser
*/
(function($) {
    $.fn.submenu = function(settings) {
        settings = jQuery.extend({

    }, settings);

    var obj = $(this);
    obj.find('ul').hide();

    obj.hover(
        function () {
            $(this).children('ul').slideDown();
        },
        function () {
            $(this).children('ul').hide();
   });

};
})(jQuery);    