﻿/**
* ...
* @author ser
* init $('.timer').timer({ date: "2016-09-25 18:42:33" })
* html <div class="timer">
                    <span class="days">aa</span>
                    <span class="hours">aa</span>
                    <span class="minutes">aa</span>
                    <span class="seconds">aa</span>
                </div>
*/
(function($) {
    $.fn.timer = function(settings) {
        settings = jQuery.extend({
            date: Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000,
    }, settings);

        var obj = $(this);

        alert(obj.html())

        function getTimeRemaining(endtime) {
            var t = Date.parse(endtime) - Date.parse(new Date());
            var seconds = Math.floor((t / 1000) % 60);
            var minutes = Math.floor((t / 1000 / 60) % 60);
            var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            var days = Math.floor(t / (1000 * 60 * 60 * 24));
            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }

        function initializeClock(endtime) {
            //var clock = document.getElementById(id);
            var daysSpan = obj.find('.days');
            var hoursSpan = obj.find('.hours');
            var minutesSpan = obj.find('.minutes');
            var secondsSpan = obj.find('.seconds');

            function updateClock() {
                var t = getTimeRemaining(endtime);

                daysSpan.html(t.days);
                hoursSpan.html(('0' + t.hours).slice(-2));
                minutesSpan.html(('0' + t.minutes).slice(-2));
                secondsSpan.html(('0' + t.seconds).slice(-2));

                if (t.total <= 0) {
                    clearInterval(timeinterval);
                }
            }

            updateClock();
            var timeinterval = setInterval(updateClock, 1000);
        }

        var deadline = new Date(settings.date);
        initializeClock(deadline);



};
})(jQuery);    