<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach ($arResult['ITEMS'] as $key => $arItem){?>
	<div class="catalog_item">
		<a href="<?=$arItem['DETAIL_PAGE_URL'];?>">
			<div class="row">
				<div class="left">
					<div class="h2">
						<?=$arItem['NAME'];?>
					</div>
					<div class="flex flj" style="margin-bottom: 30px">
						<div class="price" style="top: 2px"><em>P<em>&mdash;</em></em><?=number_format($arItem['PROPERTIES']['SUMMA']['VALUE'], 0, ' ', ' ');?> </div>
						<?$date1 = $arItem['PROPERTIES']['SROK']['VALUE'].' '.$arItem['PROPERTIES']['SROK_TIME']['VALUE'];
						
						$date1 = strtotime($date1);
						$date2 = time();
						$date3 = $date1 - $date2;
						$dni = $date3 / 86400;
						$sec = $date3 % 60;
						$chas = floor($date3 / 60);
						$min = $chas % 60;
						$chas = floor($chas / 60);
						$dni = round($dni);
						$chas = str_pad($chas, 2, '0', STR_PAD_LEFT);
						$min = str_pad($min, 2, '0', STR_PAD_LEFT);
						$sec = str_pad($sec, 2, '0', STR_PAD_LEFT);
						?>
						<?if($chas >= 72){?>
							<div class="time"><?echo $dni;?><?echo endingsForm($dni, 'день', 'дня', 'дней');?></div>
						<?} else {?>
							<div class="time">
								<?if($chas <= 3){?><strong><?}?>
								<?echo $chas . ":" . $min . ":" . $sec;?>
								<?if($chas <= 3){?></strong><?}?>
							</div>
						<?}?>
						<ul class="ibl mb_menu">
							<?if($arItem['PROPERTIES']['AVTO']['VALUE']){?>
								<li class="tooltip" title="Нужен автомобиль"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon1.png" alt=""></li>
								<li class="separator"></li>
							<?}?>
							<?if($arItem['PROPERTIES']['HOT']['VALUE']){?>
								<li class="tooltip" title="Срочный заказ"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon2.png" alt=""></li>
								<li class="separator"></li>
							<?}?>
							<?if($arItem['PROPERTIES']['FOTO']['VALUE']){?>
								<li class="tooltip" title="Предоставить фото"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon6.png" alt=""></li>
								<li class="separator"></li>
							<?}?>
							<?if($arItem['PROPERTIES']['KUPIT']['VALUE']){?>
								<li class="tooltip" title="По окончании осмотра нужно совершить покупку"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon4.png" alt=""></li>
								<li class="separator"></li>
							<?}?>
							<?if($arItem['PROPERTIES']['MEJGOROD']['VALUE']){?>
								<li class="tooltip" title="Междугородняя перевозка"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon5.png" alt=""></li>
								<li class="separator"></li>
							<?}?>
							<?if($arItem['PROPERTIES']['GRUZCHIKI']['VALUE']){?>
								<li class="tooltip" title="С грузчиками"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon7.png" alt=""></li>
								<li class="separator"></li>
							<?}?>
						</ul>
					</div>
					<div class="description">
						<?$string = strip_tags($arItem['DETAIL_TEXT']);
						$string = substr($string, 0, 283);
						$string = rtrim($string, "!,.-");
						$string = substr($string, 0, strrpos($string, ' '));
						echo $string."… ";?>
						
					</div>

				</div>
				<div class="ures_right">
					<div class="pic">
						<img src="<?=SITE_TEMPLATE_PATH;?>/images/user_icon.png" alt="">
					</div>
					<div class="description">
						<div class="h3">Иван Анатольевич С.</div>
						Информация<br>о рейтинге и отзывах
					</div>
				</div>
			</div>
			<?if($arItem['PROPERTIES']['ADDRESS']['VALUE'][0]){?>
				<div class="flex flj">
					<div class="address">
						<br>
						<p><?=$arItem['PROPERTIES']['ADDRESS']['VALUE'][0];?></p>
						<div class="map_point"><?=count($arItem['PROPERTIES']['ADDRESS']['VALUE']);?></div>
					</div>
				</div>
			<?}?>
		</a>
	</div>
<?}?>
