<ul class="ibl user_menu">
	<li class="active"><a href="/lichnyy-kabinet/moi-porucheniya/">Активные поручения</a></li>
	<li class="separator"></li>
	<li><a href="/lichnyy-kabinet/moi-porucheniya/arkhiv/">Архив поручений</a></li>
</ul>

<?
CModule::IncludeModule('iblock');
$arSelect = Array("ID", "IBLOCK_ID", 'DETAIL_TEXT', "IBLOCK_SECTION_ID", "DATE_CREATE", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*", "TIMESTAMP_X");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array("IBLOCK_ID"=>2, "ID" => $_GET["ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_USER" => $arUser['ID']);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
if($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	$arFields['PROPERTIES'] = $ob->GetProperties();
	$arResult = $arFields;
	if($arResult['PROPERTIES']['COORD']['VALUE']){
	$arPoint["type"] = "FeatureCollection";
	$i = 1;
	foreach($arResult['PROPERTIES']['ADDRESS']['VALUE'] as $key => $arAddress){
		$arCoord = explode(' ', $arResult['PROPERTIES']['COORD']['VALUE'][$key]);
		$arPoint["features"][0] = array(
			"type" => "Feature",
			"geometry" => array(
				"type" => "Point",
				"coordinates" => array(
					0 => $arCoord[1],
					1 => $arCoord[0]
				)
			),
			"properties" => array(
				"balloonContent" => $arAddress,
				"hintContent" => $arAddress,
				"iconContent" => $i
			),
			"options"=> array(
				
				 "preset" => "twirl#blueStretchyIcon"
				
			)
			
		);
		$i++;
	}
	
	
	foreach($arResult['PROPERTIES']['FILE']['VALUE'] as $id){
		$file = CFile::GetFileArray($id);
		$arFileType = explode('/', $file['CONTENT_TYPE']);
		if($arFileType[0] == 'image'){
			$arResult['FOTO'][] = $file;
		} else {
			$arResult['FILES'][] = $file;
		}
		
	}

	$arResult['point'] = json_encode($arPoint);
}?>
	
	<div class="wide_col">
		<div class="user_item">
			<div class="row">
				<div class="h2"><?=$arResult['NAME'];?></div>
				<p><a href="/lichnyy-kabinet/moi-porucheniya/">Вернуться к моим поручениям</a></p>
				<div class="row">
					<div class="col cstate">
						<div class="state">Поручение №<?=$arResult['ID'];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="date">от <?=$arResult['DATE_CREATE'];?></span></div>
					</div>
					<div class="col cprice">
						<div class="price_big" style="top: 2px"><em>P<em>&mdash;</em></em> <?=number_format($arResult['PROPERTIES']['SUMMA']['VALUE'], 0, ' ', ' ');?> </div>
					</div>
					<div class="col ctime">
			
						<?$date1 = $arResult['PROPERTIES']['SROK']['VALUE'].' '.$arResult['PROPERTIES']['SROK_TIME']['VALUE'];
							
						$date1 = strtotime($date1);
						$date2 = time();
						$date3 = $date1 - $date2;
						$dni = $date3 / 86400;
						$sec = $date3 % 60;
						$chas = floor($date3 / 60);
						$min = $chas % 60;
						$chas = floor($chas / 60);
						$dni = round($dni);
						$chas = str_pad($chas, 2, '0', STR_PAD_LEFT);
						$min = str_pad($min, 2, '0', STR_PAD_LEFT);
						$sec = str_pad($sec, 2, '0', STR_PAD_LEFT);
						?>
						<?if($chas >= 72){?>
							<div class="time"><?echo $dni;?><?echo endingsForm($dni, 'день', 'дня', 'дней');?></div>
						<?} else {?>
							<div class="time">
								<div class="timer">
									<span class="hours"><?=$chas?></span> : <span class="minutes"><?=$min?></span> : <span class="sec"><?=$sec?></span>
								</div>
							</div>
							
						<?}?>
					</div>	
					<div class="col cs6">
						<?=$arResult['PROPERTIES']['STATUS']['VALUE'];?>
					</div>
					<div class="col cs7">
						<div class="submit_button">
							<?if($arResult['PROPERTIES']['ISPOLNITEL']['VALUE'] == false){?>
								<a style="text-transform: none; text-align: center;" href="#">Отмена поручения</a>
							<?}?>
						</div>
					</div>
				</div>
				<div class="row">
					<ul class="ibl mb_menu" style="display: block; float: left;">
						<?if($arResult['PROPERTIES']['AVTO']['VALUE']){?>
							<li class="tooltip" title="Нужен автомобиль"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon1.png" alt=""></li>
							<li class="separator"></li>
						<?}?>
						<?if($arResult['PROPERTIES']['HOT']['VALUE']){?>
							<li class="tooltip" title="Срочный заказ"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon2.png" alt=""></li>
							<li class="separator"></li>
						<?}?>
						<?if($arResult['PROPERTIES']['GRUZCHIKI']['VALUE']){?>
							<li class="tooltip" title="Нужны грузчики"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon7.png" alt=""></li>
							<li class="separator"></li>
						<?}?>
						<?if($arResult['PROPERTIES']['MEJGOROD']['VALUE']){?>
							<li class="tooltip" title="Междугородняя перевозка"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon5.png" alt=""></li>
							<li class="separator"></li>
						<?}?>
						<?if($arResult['PROPERTIES']['FOTO']['VALUE']){?>
							<li class="tooltip" title="Нужно фото"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon6.png" alt=""></li>
							<li class="separator"></li>
						<?}?>
						<?if($arResult['PROPERTIES']['KUPIT']['VALUE']){?>
							<li class="tooltip" title="Совершить покупку"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon4.png" alt=""></li>
							<li class="separator"></li>
						<?}?>
					</ul>
					<div class="show_detail">
						<a href="#" data-alt="Свернуть детальную информацию" data-text="Развернуть детальную информацию">Развернуть детальную информацию</a>
					</div>
				</div>
			</div>
		</div>
		<div class="detail_info">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-9">
					<?echo $arResult['DETAIL_TEXT'];?>
					<br><br>
					<?if($arResult['FILES']){?>
						<ul class="download_list">
							<?foreach($arResult['FILES'] as $arFile){?>
								<li><a href="<?=$arFile['SRC'];?>"><?=$arFile['ORIGINAL_NAME'];?></a></li>
							<?}?>
						</ul>
					<?}?>
					<?if($arResult['FOTO']){?>
						<div class="prod_preview">
							<div class="preview_carousel">
								<ul>
									<?foreach($arResult['FOTO'] as $arFile){?>
										<li>
											<?$imgB = CFile::ResizeImageGet($arFile['ID'], array("width"=>1200, "height"=>1000), BX_RESIZE_IMAGE_PROPORTIONAL, true, $arWaterMark);?>
											<?$imgS = CFile::ResizeImageGet($arFile['ID'], array("width"=>133, "height"=>133), BX_RESIZE_IMAGE_EXACT, true, $arWaterMark);?>
											<a href="<?=$imgB['src'];?>" class="openImg" rel="1">
												<em><img src="<?=$imgS['src'];?>" alt=""></em>
											</a>
										</li>
									<?}?>
								</ul>
							</div>

							<a href="#" class="jcarousel-control-prev"></a>
							<a href="#" class="jcarousel-control-next"></a>
						</div>
					<?}?>
					<?if($arResult['PROPERTIES']['SHIRINA']['VALUE'] || $arResult['PROPERTIES']['VYSOTA']['VALUE'] || $arResult['PROPERTIES']['DLINA']['VALUE']){?>
						<p><strong>Габариты груза:</strong>  
						<?if($arResult['PROPERTIES']['DLINA']['VALUE']){?>
							длина (см) - <?=$arResult['PROPERTIES']['DLINA']['VALUE'];?>
						<?}?>	
						<?if($arResult['PROPERTIES']['SHIRINA']['VALUE']){?>
							ширина (см) - <?=$arResult['PROPERTIES']['SHIRINA']['VALUE'];?>
						<?}?>	
						<?if($arResult['PROPERTIES']['VYSOTA']['VALUE']){?>
							высота (см) <?=$arResult['PROPERTIES']['VYSOTA']['VALUE'];?>
						<?}?>
						</p>
					<?}?>
					<?if($arResult['PROPERTIES']['VES']['VALUE']){?>
						<p><strong>Вес:</strong>  <?=$arResult['PROPERTIES']['VES']['VALUE'];?></p>
					<?}?>
					<?if($arResult['PROPERTIES']['ADDRESS']['VALUE']){?>
						<p style="line-height: 30px">
							<strong>Адреса доставки:</strong><br>
							<?foreach($arResult['PROPERTIES']['ADDRESS']['VALUE'] as $key => $address){?>
								<span style="color: #41a3cb"><?=$key+1;?>.</span> <?=$address;?><br>
							<?}?>
						</p>
					<?}?>
					<?if($arResult['point']){?>
						<script src="https://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU&mode=debug" type="text/javascript"></script>
						<script type="text/javascript">

							

							function init() {
								 myMap = new ymaps.Map("map", {
									center: [57.002620490491296, 40.94630303081358],
									zoom: 11,
									controls: ['typeSelector', 'zoomControl']
								});		
								
								var result = ymaps.geoQuery(<?=$arResult['point'];?>);
								myMap.geoObjects.add(result.search('geometry.type == "Point"').clusterize()); 

								//myMap.setBounds(myMap.geoObjects.getBounds());	

								myMap.setBounds(myMap.geoObjects.getBounds(), {checkZoomRange:true}).then(function(){ if(myMap.getZoom() > 17) myMap.setZoom(17);});

								
							}

							function getPlaceMark(addr = 0){
								var addr_full = '?';
								var i = 1;
								$('input[name=address]').each(function(){
									var val = $(this).val();
									if(val){
										addr_full =  addr_full + 'addr'+i+'='+val+'&';
										i = i + 1;
									}
								});
								myMap.geoObjects.removeAll();
								$.getJSON('/includes/get_json.php'+addr_full, function (json) {
									

								});
								

							}
						</script>
						<div class="product_map" id="map" style="width: 100%; height: 500px;"></div>
					<?}?>
				</div>
			</div>
		</div>
		<?
		$arSelect = Array("ID", "IBLOCK_ID", 'DETAIL_TEXT', "PREVIEW_TEXT", "IBLOCK_SECTION_ID", "DATE_CREATE", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*", "TIMESTAMP_X");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
		$arFilter = Array("IBLOCK_ID"=>5,  "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_PORUCHENIE" => $_GET['ID']);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();  
			$arFields['PROPERTIES'] = $ob->GetProperties();
			$arUserID[] = $arFields['PROPERTIES']['ISPOLNITEL']['VALUE'];
			$arResult['ZAYAVKI'][$arFields['PROPERTIES']['ISPOLNITEL']['VALUE']] = $arFields;?>
		
		<?}
		if($arUserID){
			$filter["ID"] = $arUserID;
			$filter["ID"] = implode('|', $filter["ID"]);
			$rsUser = CUser::GetList(($by="ID"), ($order="desc"), $filter,array("SELECT"=>array("UF_*"), 'FIELD' => array('NAME', 'ID')));
			while($arUser = $rsUser->Fetch()){
				if($arUser['PERSONAL_PHOTO']){
					$img = CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array("width"=>128, "height"=>128), BX_RESIZE_IMAGE_EXACT, true);
					$arUser['PHOTO'] = $img['src'];
				} else {
					$arUser['PHOTO'] = SITE_TEMPLATE_PATH.'/images/photo.jpg';
				}	
				$arResult['ZAYAVKI'][$arUser['ID']]['USER'] = $arUser;
			}	
			$temp = array();
			$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$USER->GetID()),array("SELECT"=>array("UF_*"), 'FIELD' => array('UF_WORK_WITH_USER')));
			$arUser = $rsUser->Fetch();
			$arWorkWithUser = $arUser['UF_WORK_WITH_USER'];
			$arWorkWithUser = explode('/',$arWorkWithUser);
			foreach($arWorkWithUser as $userID){
				if($userID)
					$temp[$userID] = $userID;
			}
			
			$arWorkWithUser = $temp;
		}
		?>  
		<div class="click_count">У Вас <?echo count($arResult['ZAYAVKI']).' '.endingsForm(count($arResult['ZAYAVKI']), 'отклик', 'отклика', 'откликов');?>:</div>
		<?foreach($arResult['ZAYAVKI'] as $arItem){?>
			<div class="user_item">
				<div class="row">
					<div class="pic text_left">
						<img src="<?=$arItem['USER']['PHOTO'];?>" alt="">
					</div>
					<div class="description">
						<div class="name_line"><span class="name"><?=$arItem['USER']['NAME'];?></span> &nbsp;&nbsp;&nbsp;&nbsp; 
							<div class="rating_container" style="display: inline-block; padding: 4px 0 0 10px; margin: 0;">
								<div class="rating readonly" data-rate-value="<?=$arItem['USER']['UF_RATING'];?>"></div>
								<div class="count"><?=$arItem['USER']['UF_RATING'];?></div>
							</div>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="ustate">
								<?if($arWorkWithUser[$arItem['USER']['ID']]){?>
									<img src="<?=SITE_TEMPLATE_PATH;?>/images/handup.png" alt="">&nbsp;
									<span>Вы уже работали вместе</span>
								<?}?>
							</span>
							&nbsp;&nbsp;&nbsp;
							<span class="ustate">
								<?if($arItem['USER']['UF_TRIED']){?>
									<img src="<?=SITE_TEMPLATE_PATH;?>/images/shield.png" alt="">&nbsp;
									<span>Проверенный исполнитель</span>
								<?}?>
							</span>
						</div>
						<p><?=$arItem['PREVIEW_TEXT'];?></p>
						<?if($arResult['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 2 || $arResult['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 3 || $arResult['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 4 || $arResult['PROPERTIES']['STATUS']['VALUE_XML_ID'] == false){?>
							<?if($arResult['PROPERTIES']['ISPOLNITEL']['VALUE'] == $arItem['USER']['ID']){?>
								Назначен исполнителем 
							<?} else {?>
								<div class="submit_button">
									<a style="display: inline-block; text-transform: none;" href="" onclick="select_ispolnitel(<?=$arItem['ID'];?>); return false;">Выбрать исполнителем </a>
								</div>
							<?}?>
						<?}?>
					</div>
				</div>
			</div>
		<?}?>
	</div>
<?}?>


