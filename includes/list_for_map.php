<?
$arSelect = Array("ID", "NAME", "PROPERTY_AVTO", "PROPERTY_MEJGOROD", "PROPERTY_GRUZCHIKI", "PROPERTY_HOT", "PROPERTY_ONE_ADDR", "PROPERTY_COORD", "PROPERTY_SUMMA");
$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", "!PROPERTY_COORD" => false);
if($_GET['kat']){
	$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", 'SECTION_ID' => $_GET['kat'], 'INCLUDE_SUBSECTIONS' => 'Y', "!PROPERTY_COORD" => false);
}
if($_GET['sub']){
	$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", 'SECTION_ID' => $_GET['sub'], 'INCLUDE_SUBSECTIONS' => 'Y', "!PROPERTY_COORD" => false);
}

if($_GET['custom']){
	$arFilter['>=PROPERTY_SUMMA'] = $_GET['custom'];
}
if($_GET['avto']){
	$arFilter['PROPERTY_AVTO_VALUE'] = "Да";
}
if($_GET['mejgorod']){
	$arFilter['PROPERTY_MEJGOROD_VALUE'] = "Да";
}
if($_GET['gruzchiki']){
	$arFilter['PROPERTY_GRUZCHIKI_VALUE'] = "Да";
}
if($_GET['hot']){
	$arFilter['PROPERTY_HOT_VALUE'] = "Да";
}
if($_GET['one_addr']){
	$arFilter['PROPERTY_ONE_ADDR_VALUE'] = "Да";
}

$arElements = array();
$arResultMap["type"] = "FeatureCollection";

$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	
	if($arElements[$arFields['ID']]) continue;
	
	$arCoord = explode(' ', $arFields['PROPERTY_COORD_VALUE']);
	$arResultMap["features"][] = array(
		"type" => "Feature",
		"geometry" => array(
			"type" => "Point",
			"coordinates" => array(
				0 => $arCoord[1],
				1 => $arCoord[0]
			)
		),
		"properties" => array(
			/*"balloonContent" => '
				<div class="map_win" data-href="#mbottom1" data-id="'.$arFields['ID'].'">
					<div class="description">'.$arFields['NAME'].'</div>
					<div class="row">
						<div class="price" style="top: 2px; margin-right: 20px; margin-bottom: 0; float:right"><em>P<em>&mdash;</em></em>'.number_format($arFields['PROPERTY_SUMMA_VALUE'], 0, ' ', ' ').' </div>
					</div>
					<div class="arrow"></div>
				</div>
			',
			"hintContent" => $arFields['NAME']*/
		),
		"options"=> array(
			
			 "ID"  => $arFields['ID'],
			/* "balloonLayout"  => "default#imageWithContent",*/
			"content" => '
				
					<div class="description">'.$arFields['NAME'].'</div>
					<div class="row">
						<div class="price" style="top: 2px; margin-right: 20px; margin-bottom: 0; float:right"><em>P<em>&mdash;</em></em>'.number_format($arFields['PROPERTY_SUMMA_VALUE'], 0, ' ', ' ').' </div>
					</div>
					<div class="arrow"></div>
				',
			 "iconLayout"  => "default#image",
			 "iconImageHref"  => "/local/templates/poruchenia/images/map_point2.png",
			 "iconImageSize"  => array(33, 44),
			 "iconImageOffset"  => array(-13, -44)

			
		)
		
	);
	$i++;
	$arElements[$arFields['ID']] = $arFields;
	
}
?> 

<?
$arResultMap = json_encode($arResultMap);
?>

<div class="map">
<script src="https://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU&mode=debug" type="text/javascript"></script>
	<script type="text/javascript">

		ymaps.ready(init);

		function init() {
			 myMap = new ymaps.Map("map", {
				center: [57.002620490491296, 40.94630303081358],
				zoom: 11,
				controls: ['typeSelector', 'zoomControl']
			});		
			
			var result = ymaps.geoQuery(<?=$arResultMap;?>);
			myMap.geoObjects.add(result.search('geometry.type == "Point"').clusterize()); 

			//myMap.setBounds(myMap.geoObjects.getBounds());	

			myMap.setBounds(myMap.geoObjects.getBounds(), {checkZoomRange:true}).then(function(){ if(myMap.getZoom() > 17) myMap.setZoom(17);});

			 myMap.geoObjects.events
				.add('mouseenter', function (e) {
					var content = e.get('target').options.get('content');
					if(content){
						$('.map_win').html(content);
						$('.map_win').show();
						$('.map').mousemove(function(e){
							// положение элемента
							var pos = $(this).offset();
							var elem_left = pos.left;
							var elem_top = pos.top;
							// положение курсора внутри элемента
							var Xinner = e.pageX - elem_left;
							var Yinner = e.pageY - elem_top;
							
							Yinner = Yinner - $('.map_win').height() - 40;
							Xinner = Xinner - ($('.map_win').width() / 2) - 17;	
							$('.map_win').css('top', Yinner);
							$('.map_win').css('left', Xinner);

						}); 
					}
				})
				.add('click', function (e) {
					var id = e.get('target').options.get('ID');
					if(id){
						$.post('/includes/map_element.php', {ID:id}, function(data){
							$('#res_element').html(data);
						});
					}
					//console.log(e.get('target').options.get('ID'));
				})
				.add('mouseleave', function (e) {
					$('.map_win').hide();
					$('.map_win').html('');
					$('.map').unbind('mousemove');
				});
		}

		function getPlaceMark(addr = 0){
			var addr_full = '?';
			var i = 1;
			$('input[name=address]').each(function(){
				var val = $(this).val();
				if(val){
					addr_full =  addr_full + 'addr'+i+'='+val+'&';
					i = i + 1;
				}
			});
			myMap.geoObjects.removeAll();
			$.getJSON('/includes/get_json.php'+addr_full, function (json) {
				

			});
			

		}
	</script>
	<div class="map" id="map" style="width: 100%; height: 500px;"></div>
	<div class="map_win" style="display:none;"></div>
<style>

.map_win {
    position: absolute;
    top: -86px;
    left: -101px;
}

.map_win .description {
    width: 167px;
}
</style>