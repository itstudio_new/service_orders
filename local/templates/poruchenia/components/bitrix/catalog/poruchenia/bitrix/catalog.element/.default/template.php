<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $USER;
if ($USER->IsAuthorized()){
	$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$USER->GetID()),array("SELECT"=>array("UF_*"), 'FIELD' => array('NAME', 'ID')));
	$arUser = $rsUser->Fetch();
}?>
<div class="middle_col">
	<div class="flex flj" style="margin-bottom: 30px; max-width: 586px">
		<div class="price_big" style="top: 2px"><em>P<em>&mdash;</em></em><?=number_format($arResult['PROPERTIES']['SUMMA']['VALUE'], 0, ' ', ' ');?> </div>
		<?$date1 = $arResult['PROPERTIES']['SROK']['VALUE'].' '.$arResult['PROPERTIES']['SROK_TIME']['VALUE'];
						
		$date1 = strtotime($date1);
		$date2 = time();
		$date3 = $date1 - $date2;
		$dni = $date3 / 86400;
		$sec = $date3 % 60;
		$chas = floor($date3 / 60);
		$min = $chas % 60;
		$chas = floor($chas / 60);
		$dni = round($dni);
		$chas = str_pad($chas, 2, '0', STR_PAD_LEFT);
		$min = str_pad($min, 2, '0', STR_PAD_LEFT);
		$sec = str_pad($sec, 2, '0', STR_PAD_LEFT);
		?>
		<?if($chas >= 72){?>
			<div class="time"><?echo $dni;?><?echo endingsForm($dni, 'день', 'дня', 'дней');?></div>
		<?} else {?>
			<div class="time">
				<?if($chas <= 3){?><strong><?}?>
				<?echo $chas . ":" . $min . ":" . $sec;?>
				<?if($chas <= 3){?></strong><?}?>
			</div>
		<?}?>
		<div class="product_status"><?=$arItem['PROPERTIES']['STATUS']['VALUE'];?></div>
	</div>
	<div class="product">
		<div class="flex flj" style="margin-bottom: 30px">
			<div class="mb10"> Поручение №<?=$arResult['ID'];?> </div>
			<div class="date mb10">от <?=$arResult['DATE_CREATE'];?></div>
			<ul class="ibl mb_menu">
				<?if($arResult['PROPERTIES']['AVTO']['VALUE']){?>
					<li class="tooltip" title="Нужен автомобиль"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon1.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
				<?if($arResult['PROPERTIES']['HOT']['VALUE']){?>
					<li class="tooltip" title="Срочный заказ"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon2.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
				<?if($arResult['PROPERTIES']['GRUZCHIKI']['VALUE']){?>
					<li class="tooltip" title="Нужны грузчики"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon7.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
				<?if($arResult['PROPERTIES']['MEJGOROD']['VALUE']){?>
					<li class="tooltip" title="Междугородняя перевозка"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon5.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
				<?if($arResult['PROPERTIES']['FOTO']['VALUE']){?>
					<li class="tooltip" title="Нужно фото"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon6.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
				<?if($arResult['PROPERTIES']['KUPIT']['VALUE']){?>
					<li class="tooltip" title="Совершить покупку"><img src="<?=SITE_TEMPLATE_PATH;?>/images/mtf_icon4.png" alt=""></li>
					<li class="separator"></li>
				<?}?>
				
			</ul>
		</div>
		<?echo $arResult['DETAIL_TEXT'];?><br><br>
		<?if($arResult['FILES']){?>
			<ul class="download_list">
				<?foreach($arResult['FILES'] as $arFile){?>
					<li><a href="<?=$arFile['SRC'];?>"><?=$arFile['ORIGINAL_NAME'];?></a></li>
				<?}?>
			</ul>
		<?}?>
		<?if($arResult['FOTO']){?>
			<div class="prod_preview">
				<div class="preview_carousel">
					<ul>
						<?foreach($arResult['FOTO'] as $arFile){?>
							<li>
								<?$imgB = CFile::ResizeImageGet($arFile['ID'], array("width"=>1200, "height"=>1000), BX_RESIZE_IMAGE_PROPORTIONAL, true, $arWaterMark);?>
								<?$imgS = CFile::ResizeImageGet($arFile['ID'], array("width"=>133, "height"=>133), BX_RESIZE_IMAGE_EXACT, true, $arWaterMark);?>
								<a href="<?=$imgB['src'];?>" class="openImg" rel="1">
									<em><img src="<?=$imgS['src'];?>" alt=""></em>
								</a>
							</li>
						<?}?>
					</ul>
				</div>

				<a href="#" class="jcarousel-control-prev"></a>
				<a href="#" class="jcarousel-control-next"></a>
			</div>
		<?}?>
		<?if($arResult['PROPERTIES']['SHIRINA']['VALUE'] || $arResult['PROPERTIES']['VYSOTA']['VALUE'] || $arResult['PROPERTIES']['DLINA']['VALUE']){?>
			<p><strong>Габариты груза:</strong>  
			<?if($arResult['PROPERTIES']['DLINA']['VALUE']){?>
				длина (см) - <?=$arResult['PROPERTIES']['DLINA']['VALUE'];?>
			<?}?>	
			<?if($arResult['PROPERTIES']['SHIRINA']['VALUE']){?>
				ширина (см) - <?=$arResult['PROPERTIES']['SHIRINA']['VALUE'];?>
			<?}?>	
			<?if($arResult['PROPERTIES']['VYSOTA']['VALUE']){?>
				высота (см) <?=$arResult['PROPERTIES']['VYSOTA']['VALUE'];?>
			<?}?>
			</p>
		<?}?>
		<?if($arResult['PROPERTIES']['VES']['VALUE']){?>
			<p><strong>Вес:</strong>  <?=$arResult['PROPERTIES']['VES']['VALUE'];?></p>
		<?}?>
		<?if($arResult['PROPERTIES']['ADDRESS']['VALUE']){?>
			<p style="line-height: 30px">
				<strong>Адреса доставки:</strong><br>
				<?foreach($arResult['PROPERTIES']['ADDRESS']['VALUE'] as $key => $address){?>
					<span style="color: #41a3cb"><?=$key+1;?>.</span> <?=$address;?><br>
				<?}?>
			</p>
		<?}?>
		<?if($arResult['point']){?>
			<script src="https://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU&mode=debug" type="text/javascript"></script>
			<script type="text/javascript">

				ymaps.ready(init);

				function init() {
					 myMap = new ymaps.Map("map", {
						center: [57.002620490491296, 40.94630303081358],
						zoom: 11,
						controls: ['typeSelector', 'zoomControl']
					});		
					
					var result = ymaps.geoQuery(<?=$arResult['point'];?>);
					myMap.geoObjects.add(result.search('geometry.type == "Point"').clusterize()); 

					//myMap.setBounds(myMap.geoObjects.getBounds());	

					myMap.setBounds(myMap.geoObjects.getBounds(), {checkZoomRange:true}).then(function(){ if(myMap.getZoom() > 17) myMap.setZoom(17);});

					
				}

				function getPlaceMark(addr = 0){
					var addr_full = '?';
					var i = 1;
					$('input[name=address]').each(function(){
						var val = $(this).val();
						if(val){
							addr_full =  addr_full + 'addr'+i+'='+val+'&';
							i = i + 1;
						}
					});
					myMap.geoObjects.removeAll();
					$.getJSON('/includes/get_json.php'+addr_full, function (json) {
						

					});
					

				}
				
				$(document).ready(function(){
					$('body').on('click', '.show_detail a', function(){
						alert(1);
					});
				});
			</script>
			<div class="product_map" id="map" style="width: 100%; height: 500px;"></div>
		<?}?>
		<div class="product_bottom">
			<div class="submit_button_alt">
				<?if($arUser['UF_TYPE'] == false){?>
					Для полного доступа к функционалу проекта вы должны Войти или зарегистрироваться
				<?} elseif($arUser['UF_TYPE'] == 1){?>
					<input type="button"  onclick="otclick(<?=$arResult['ID'];?>); return false;" value="Откликнуться на поручение">
				<?} else {?>
					<input type="button" value="Стать исполнителем">
				<?}?>
				
			</div>
		</div>
	</div>
	<?/*<pre><?print_r ($arResult);?></pre>*/?>
</div>
<div class="right_col">
	<div class="right_col_user">
		<div class="h4">Заказчик</div>
		<div class="pic">
			<img src="<?=$arResult['USER']['PHOTO'];?>" alt="">
		</div>
		<div class="description">
			<div class="h5"><?=$arResult['USER']['NAME'];?></div>
			<div class="text">
				Информация<br>
				о рейтинге и отзывах
			</div>
		</div>
	</div>
	<div class="banner_right">
		<img src="<?=SITE_TEMPLATE_PATH;?>/images/banner2.jpg" alt="">
		<img src="<?=SITE_TEMPLATE_PATH;?>/images/banner3.jpg" alt="">
		<img src="<?=SITE_TEMPLATE_PATH;?>/images/banner4.jpg" alt="">
	</div>
</div>

