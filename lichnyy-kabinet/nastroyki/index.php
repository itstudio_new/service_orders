<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Настройки");

global $USER;
if ($USER->IsAuthorized()){
	$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$USER->GetID()),array("SELECT"=>array("UF_*"), 'FIELD' => array('NAME', 'ID', 'PERSONAL_PHOTO', 'PERSONAL_NOTES')));
	$arUser = $rsUser->Fetch();
}
if($arUser['UF_TYPE'] == false) $arUser['UF_TYPE'] = 1;
//$arUser['UF_TYPE'] = 1 исполнитель
//$arUser['UF_TYPE'] = 2 заказчик
?>

<ul class="ibl user_menu">
	<li class="active"><a href="user_profile.html">Настройки профиля</a></li>
	<li class="separator"></li>
	<li><a href="user_mail.html">Настройки подписки</a></li>
</ul>
<div class="wide_col">
	<div class="row user_setting">
		<div class="pic">
			<div class="pic_cnt">
				<?$img = CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array("width"=>196, "height"=>271), BX_RESIZE_IMAGE_EXACT, true);?>
				<img src="<?=$img['src'];?>"  style="width:196px; height:271px;"alt="">
				<div class="custom_upload">
					<input class="text" type="text" value="">
					<input class="file" id="file" type="file" value="">
				</div>
			</div>
		</div>
		<div class="user_setting_cnt">
			<form id="user_form">
				<div class="row">
				<div class="button_right">
					<?if($arUser['UF_TYPE'] == 1){?>
						<div class="submit_button">
							<a href="#">Удалить профиль</a>
						</div>
					<?}?>
					<?if($arUser['UF_TYPE'] == 2){?>
						<div class="submit_button">
							<a href="#call_dialog_order_user">Стать исполнителем</a>
							<a href="#">Удалить профиль</a>
						</div>
					<?}?>
				</div>
				<div class="registration_form user_profile_form">
						<div class="description">
							<div class="forms">
							  <div class="label row">
								  <em>ФИО </em>
								  <div class="col-md-6 col-sm-6 col-xs-10">
									 <input class="textbox fi fi3" placeholder="ФИО" name="name" id="reg_name" type="text" value="<?=$arUser['NAME'];?>" >
									 <span id="err_name_text" style="text-align:center"></span>
								  </div>
								  <div></div>
							  </div>
							  <div class="label row"><em>E-mail </em>
								  <div class="col-md-6 col-sm-6 col-xs-10">
									<input class="textbox fi fi4" placeholder="E-mail"  id="reg_email" type="email" disabled value="<?=$arUser['EMAIL'];?>" >
									<span id="err_mail_text" style="text-align:center"></span>
								  </div>
								  <div></div>
							  </div>
							  <div class="label labelr row">
									<em>Телефон </em>
									
									<div class="col-md-6 col-sm-6 col-xs-10">
										<input check="<?=($arUser['UF_CHECK_PHONE'] ? 'Y' : 'N');?>" class="textbox fi fi6" type="tel" name="phone" id="reg_phone_made" value="<?=$arUser['PERSONAL_PHONE'];?>">
										<span id="err_phone_text" style="text-align:center"></span>
									</div>
									<?if($arUser['UF_CHECK_PHONE']){
										$check = $arUser['PERSONAL_PHONE'].'_check';
										$time = time()+60*60*24;
										setcookie($check, $check, $time, '/');
										$display_none = 'display:none;';
										$_SESSION['check_phone_made'] = $arUser['PERSONAL_PHONE'];
									}?>
									<div class="submit_button" style="float: left; padding-left: 10px; <?=$display_none;?>">
										<input  id="show_code_made" type="button" style="text-transform: none; padding-top: 12px; padding-bottom: 12px;" value="Запросить код">
									</div>
									<div class="code_field" style="padding-left: 10px">
										<input class="textbox" style="max-width: 100px; float: left" type="text" name="phone_check" id="reg_phone_check_made" value="">
										<div class="submit_button">
											<input type="button" style="text-transform: none; padding-top: 12px; padding-bottom: 12px;" id="check_code_made" value="ОК">
										</div>
									</div>

									<div id="phone_err_div"></div>

								</div>
							  <div class="label row reg_pass">
								  <div class="field">
									 <em>Пароль </em>
									<div>
										<input class="textbox fi fi7" type="password" id="reg_pass" name="password" value="" >
										
									</div>
								  </div>
								  <div class="empty_pic"></div>
								  <div class="field">
									 <em>Повторите Пароль </em>
									<div>
									  <input class="textbox fi fi7" name="check_pass" id="reg_pass_check" type="password" name="password_2" value="" >
									 </div>
								  </div>
								  <div></div>
								  <span id="err_pass_text" style="clear: both;"></span>
							  </div>
						</div>
						</div>
						<div class="clear"></div>
				</div>
				</div>
				<label>
					<textarea class="textbox textbox2" name="text" placeholder="Расскажите о себе"><?=$arUser['WORK_NOTES'];?></textarea>
				</label>
				<?if($arUser['UF_TYPE'] == 1){?>
					<p>Выберите категории, в которых Вы хотите получать поручения: </p>
					<div class="row">
						<div class="user_cat_list">
							<?
							CModule::IncludeModule('iblock');
							$arFilter = Array('IBLOCK_ID'=>2, 'GLOBAL_ACTIVE'=>'Y');
							$db_list = CIBlockSection::GetList(Array('sort'=>'asc'), $arFilter, false, array('ID', 'NAME', 'IBLOCK_SECTION_ID'));
							while($ar_result = $db_list->GetNext()){
								$temp[$ar_result['ID']] = $ar_result;
							}
							foreach($temp as $id => $arSection){
								if($arSection['IBLOCK_SECTION_ID'] == false){
									$arResult[$arSection['ID']] = $arSection;
								}
							}
							foreach($temp as $id => $arSection){
								if($arSection['IBLOCK_SECTION_ID']){
									$arResult[$arSection['IBLOCK_SECTION_ID']]['SUB'][] = $arSection;
								}
							}
							$arSectionUser = explode('/', $arUser['PERSONAL_NOTES']);

							?>
							<div class="user_cat_scroll">
								<ul>
									<?foreach($arResult as $arSection){?>
										<li>
											<a data-id="<?=$arSection['ID'];?>"> <?=$arSection['NAME'];?></a>
											<ul>
												<?foreach($arSection['SUB'] as $arSectionSub){?>
													<li><a href="#" data-id="<?=$arSectionSub['ID'];?>"> <?=$arSectionSub['NAME'];?></a></li>
													<?foreach($arSectionUser as $IDSectionUser){
														if($IDSectionUser == $arSectionSub['ID']){
															$sectHTML .= '<div class="item">'.$arSection['NAME'].' / '.$arSectionSub['NAME'].'<input type="hidden" data-id="'.$arSectionSub['ID'].'" name="section[]" value="'.$arSectionSub['ID'].'/'.$arSection['ID'].'"><a class="del" href="#"></a></div>';
														}
													}?>
												<?}?>
											</ul>
										</li>
									<?}?>
								</ul>
							</div>
						</div>
						<div class="user_cat_select">
							<?=$sectHTML;?>
						</div>
					</div>
				<?}?>
				<p class="rem" style="color: red;"></p>
				<div class="submit_button_alt" style="text-align: left;">
					<input style="text-align: center;"  type="submit" value="Сохранить">
				</div>
			</form>
		</div>
	</div>
</div>

<script>
$(function () {	
	//Телефон
	$("#reg_phone_made").focusout(function(){
		checkPhone(this);
		//var check = $(this).attr('check');
		$.post('/includes/ajax.php', {phone: $(this).val(), check_phone: 'y'}, function(data){
			//console.log(data);
			var check = parseInt(data);
			console.log(check);
			if(check == 1){
				$("#show_code_made").parent().hide();
				$('.code_field').hide();
				$('.submit_button input').hide();
				$('#reg_phone_made').attr('check', 'Y');
				//console.log(22);
			} else {
				$("#show_code_made").parent().show();
				$('.code_field').hide();
				$('.submit_button input').show();
				$('#reg_phone_made').attr('check', 'N');
				//console.log(33);
			}
		});
		$('.code_field .textbox').val('');
	});
	$("#reg_phone_check_made").focusout(function(){
		var name = $(this).val();
		if (name == false)
		{
			$(this).parent().addClass("error");
		}
		else
		{
			$(this).parent().removeClass("error");
		}
	});

	//Проверка телефона
	$("#show_code_made").click(function(){
		var regphone = /^[0-9\s\-\(\)\+]+$/;
		var phone = $("#reg_phone_made").val();
		if (phone != false && regphone.test(phone))
		{
			//отправка аякса
			var info = 'send_sms=y&phone='+phone;
			console.log(info);
			$.post('/includes/ajax.php', info, function(data){
				var check = parseInt(data);
				if(check == 1){
					$("#show_code_made").parent().hide();
					$('.code_field').show();
				}
				else{
					$("#err_phone_text").addClass("error");
					$("#show_code_made").parent().hide();
					$('.code_field').show();
					$("#err_phone_text").text("Подтверждение данного номера телефона было менее суток назад");
					return false;
				}	
				console.log(data);
			});
		}
		else
		{
			$(this).parent().prev().addClass("error");
			$("#phone_err_div").removeAttr("class").addClass("error_pic");
			$("#err_phone_text").addClass("error");
			$("#err_phone_text").text("Некорректный телефон");
			return false;
		}
	});

	$("#check_code_made").click(function(){
		var code = $("#reg_phone_check_made").val();
		var phone = $("#reg_phone_made").val();
		if (code != false)
		{
			//отправка аякса
			var info = 'check_phone=y&check_code='+code+'&phone='+phone;
			$.post('/includes/ajax.php', info, function(data){
				var check = parseInt(data);
				if(check == 1){
					//проверка прошла успешно
					
					$("#phone_err_div").removeAttr("class").addClass("success_pic");
					var buttonOKstyle = $("#check_code").attr('style');
					$("#err_phone_text").removeClass("error");
					$("#err_phone_text").text("");
					$("#check_code").attr('style', buttonOKstyle+"display:none");
					$('#reg_phone_made').attr('check', 'Y');
				}
				else{
					//ошибка проверки
					$("#phone_err_div").removeAttr("class").addClass("error_pic");
					$("#reg_phone_check").parent().addClass("error");
					$("#err_phone_text").addClass("error");
					$("#err_phone_text").text("Неверный проверочный код телефон");
				}	
			});
		}
		else
		{
			$("#reg_phone_check").parent().addClass("error");
			$("#phone_err_div").removeAttr("class").addClass("error_pic");
		}
	});
	
	document.getElementById('file').addEventListener('change', handleFileSelect, false);
});

function handleFileSelect(evt) {
	
    var file = evt.target.files; // FileList object
    var f = file[0];
    // Only process image files.
    if (!f.type.match('image.*')) {
        alert("Image only please....");
    }
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
        return function(e) {
            // Render thumbnail.;
			$('.pic_cnt img').attr('src', e.target.result);
        };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
}

</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>