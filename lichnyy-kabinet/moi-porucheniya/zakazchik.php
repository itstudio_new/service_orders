<ul class="ibl user_menu">
	<li class="active"><a href="/lichnyy-kabinet/moi-porucheniya/">Активные поручения</a></li>
	<li class="separator"></li>
	<li><a href="/lichnyy-kabinet/moi-porucheniya/arkhiv/">Архив поручений</a></li>
</ul>
<?
$date = strtotime('-3 days');
$date = date('Y-m-d H:i:s', $date);


CModule::IncludeModule('iblock');
$arSelect = Array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "DATE_CREATE", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*", "TIMESTAMP_X");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array(
	"IBLOCK_ID"=>2, 
	"ACTIVE_DATE"=>"Y", 
	"ACTIVE"=>"Y", 
	"PROPERTY_USER" => $arUser['ID'], 
	"!PROPERTY_STATUS" => 19,
	array(
        "LOGIC" => "OR",
        array("PROPERTY_DATE_END" => false),
        array(">=PROPERTY_DATE_END" => $date)
    )
);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	$arFields['PROPERTIES'] = $ob->GetProperties();
	$arResult[] = $arFields;
}


$rsSect = CIBlockSection::GetList(array(),array('IBLOCK_ID' => 2), false, array('ID', 'IBLOCK_SECTION_ID', 'NAME'));
while ($arSect = $rsSect->GetNext()){
	$arSections[$arSect['ID']] = $arSect;
}

?>
<div class="wide_col">
	<?foreach($arResult as $arItem){?>
		<div class="user_item <?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 5){?> active<?}?>">
			<div class="row">
				<div class="h2"><a href="?ID=<?=$arItem['ID'];?>"><?=$arItem['NAME'];?></a></div>
				<div class="row">
					<div class="col cstate">
						<div class="state">Поручение <?=$arItem['ID'];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="date">от <?=$arItem['DATE_CREATE'];?></span></div>
						<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 4){?>
							<a class="review_read" href="#call_user_review">Читать отзыв</a>
						<?}?>
					</div>
					<div class="col cprice">
						<div class="price_big" style="top: 2px"><em>P<em>&mdash;</em></em><?=number_format($arItem['PROPERTIES']['SUMMA']['VALUE'], 0, ' ', ' ');?> </div>
					</div>
					<div class="col ctime">
						
						<?
						$stop = false;
						$date1 = $arItem['PROPERTIES']['SROK']['VALUE'].' '.$arItem['PROPERTIES']['SROK_TIME']['VALUE'];
						if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 4 || $arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 5){
							$arDate = explode(':', $arItem['TIMESTAMP_X']);
							
							if($arItem['PROPERTIES']['DATE_END']['VALUE']){
								$date1 = $arItem['PROPERTIES']['DATE_END']['VALUE'];
								$stop = true;
							} else {
								$date1 = strtotime($arItem['TIMESTAMP_X']);
							}
							$arItem['TIMESTAMP_X'] = $arDate[0].':'.$arDate[1];
	
							$stop = true;
	
							$dateEnd = explode(':', $arItem['PROPERTIES']['DATE_END']['VALUE']);
							$dateEnd = $dateEnd[0].':'.$dateEnd[1];
							
						}	
						$date1 = strtotime($date1);
						$date2 = time();
						$date3 = $date1 - $date2;
						$dni = $date3 / 86400;
						$sec = $date3 % 60;
						$chas = floor($date3 / 60);
						$min = $chas % 60;
						$chas = floor($chas / 60);
						$dni = round($dni);
						$chas = str_pad($chas, 2, '0', STR_PAD_LEFT);
						$min = str_pad($min, 2, '0', STR_PAD_LEFT);
						$sec = str_pad($sec, 2, '0', STR_PAD_LEFT);
						
						$arDMY = explode('.', $arItem['PROPERTIES']['SROK']['VALUE']);
						$D = $arDMY[0];
						$M = $arDMY[1];
						$Y = $arDMY[2];
						?>
						<script>
							$(document).ready(function(){
								$('#time_'+<?=$arItem['ID'];?>).timer({ date: new Date(2017, <?=$M;?>, <?=$dni;?>, <?=$chas;?>, <?=$min;?>, <?=$sec;?>) });
							});	
						</script>
						<?if($chas >= 72){?>
							<div class="time"><?echo $dni;?><?echo endingsForm($dni, 'день', 'дня', 'дней');?></div>
						<?} else {?>
							<div class="time <?if($chas <= 3){?>active<?}?> <?=$dni;?>" >
								<div class="timer" <?if($stop == false){?>id="time_<?=$arItem['ID'];?>"<?}?>>
									<span class="days"><?=$dni;?></span> : <span class="hours"><?=$chas;?></span> : <span class="minutes"><?=$min;?></span> : <span class="seconds"><?=$sec;?></span>
								</div>
							</div>
						<?}?>
					</div>
					<div class="col cs2">
						<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 2) $arItem['PROPERTIES']['STATUS']['VALUE'] = 'Ждет подтверждения';?>
						<?=$arItem['PROPERTIES']['STATUS']['VALUE'];?>
						<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 5){?>
							<span style="color: #000; font-size: 14px;"><?=$arItem['TIMESTAMP_X'];?></span>
						<?}?>
						<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 4){?>
							<span class="d"><?=$dateEnd;?></span>
						<?}?>
					</div>
					<div class="col cs3">
						<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 2){?>
							<?$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$arItem['PROPERTIES']['ISPOLNITEL']['VALUE']),array('FIELD' => array('NAME', 'ID')));
							$arUserIspolnitel = $rsUser->Fetch();?>
							Выполняет
							<span class="name "><a class="tooltips" href="/ispolniteli/<?=$arUserIspolnitel['ID'];?>/"><?=$arUserIspolnitel['NAME'];?></a></span>
                        <?} elseif($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 1){?>
							<?$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$arItem['PROPERTIES']['ISPOLNITEL']['VALUE']),array('FIELD' => array('NAME', 'ID', 'PERSONAL_PHONE')));
							$arUserIspolnitel = $rsUser->Fetch();?>
							Выполняет
							<span class="name "><a class="tooltips" title="Контактный телефон: +<?=$arUserIspolnitel['PERSONAL_PHONE'];?>" href="/ispolniteli/<?=$arUserIspolnitel['ID'];?>/"><?=$arUserIspolnitel['NAME'];?></a></span>
                        <?} elseif($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 4 || $arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 5){?>
							<?$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$arItem['PROPERTIES']['ISPOLNITEL']['VALUE']),array('FIELD' => array('NAME', 'ID', 'PERSONAL_PHONE')));
							$arUserIspolnitel = $rsUser->Fetch();?>
							Выполнил
							<span class="name "><a class="tooltips" title="Контактный телефон: +<?=$arUserIspolnitel['PERSONAL_PHONE'];?>" href="/ispolniteli/<?=$arUserIspolnitel['ID'];?>/"><?=$arUserIspolnitel['NAME'];?></a></span>
                                                                        
						<?} elseif($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 3) {?>
							<?if($arItem['PROPERTIES']['COUNT']['VALUE']){
								echo $arItem['PROPERTIES']['COUNT']['VALUE'].' '.endingsForm($arItem['PROPERTIES']['COUNT']['VALUE'], 'Отклик', 'Отклика', 'Откликов');
							} else {?>
								Нет откликов
							<?}?>
						<?}?>
					</div>
					
					<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 5){?>
						<div class="col cs4">
							<a class="button" href="" onclick="accept(<?=$arItem['ID'];?>); return false;">Принять работу</a>
						</div>
					<?}?>
				</div>
				
			
				<?
				if($arSections[$arItem['IBLOCK_SECTION_ID']]){
					$copy_link = '/sozdat-poruchenie/';
					if($arSections[$arItem['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']){
						$copy_link .= '?kat='.$arSections[$arItem['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID'].'&sub='.$arItem['IBLOCK_SECTION_ID'];
					} else {
						$copy_link .= '?kat='.$arItem['IBLOCK_SECTION_ID'];
					}
					$copy_link .= '&id='.$arItem['ID'];
					$red_link = $copy_link.'&red=y';
					
				}
				?>
				<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 3 && $arItem['PROPERTIES']['COUNT']['VALUE'] == false){?>
					<div class="icons">
						<a class="copy" title="копировать" href="<?=$copy_link;?>"></a>
						<a class="edit" title="редактировать" href="<?=$red_link;?>"></a>
						<a class="del" title="удалить" href="" onclick="del_poruchenie(<?=$arItem['ID'];?>); return false;"></a>
					</div>
				<?}?>
				<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 3 && $arItem['PROPERTIES']['COUNT']['VALUE'] != false){?>
					<div class="icons">
						<a class="copy" title="копировать" href="<?=$copy_link;?>"></a>
						<a class="stop" title="отменить" href="" onclick="cancel_poruchenie(<?=$arItem['ID'];?>); return false;"></a>
					</div>
				<?}?>
				<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 2){?>
					<div class="icons">
						<a class="copy" title="копировать" href="<?=$copy_link;?>"></a>
						<a href="?ID=<?=$arItem['ID'];?>" title="сменить исполнителя"><img src="<?=SITE_TEMPLATE_PATH;?>/images/change.png" alt=""></a>
					</div>
				<?}?>
				<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 1){?>
					<div class="icons">
						<a class="copy" title="копировать" href="<?=$copy_link;?>"></a>
					</div>	
				<?}?>
				<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 4){?>
					<div class="icons">
						<a class="copy" title="копировать" href="<?=$copy_link;?>"></a>
					</div>	
				<?}?>
				                               
				
			</div>
		</div>
	<?}?>
</div>

