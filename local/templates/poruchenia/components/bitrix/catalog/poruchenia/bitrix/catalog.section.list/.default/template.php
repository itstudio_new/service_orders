<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult['SECTION']['ID'] == 0){?>
	<ul class="ibl cat_list">
		<?foreach($arResult['SECTIONS'] as $key => $arSection){?>
			<li class="item<?=$key;?>">
				<div class="h3"><a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a></div>
				<ul>
					<?foreach($arSection['SUB'] as $arSectionSub){?>
						<li><a href="<?=$arSectionSub['SECTION_PAGE_URL']?>"><?=$arSectionSub['NAME']?></a></li>
					<?}?>
				</ul>
			</li>
		<?}?>
	</ul>
<?} else {
	if($arResult['SECTION']['PATH'][1]['NAME']){
		$title = $arResult['SECTION']['PATH'][0]['NAME'].' - '.$arResult['SECTION']['PATH'][1]['NAME'];
		$APPLICATION->SetTitle($title);
	}
}?>

