<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Карта поручений");
?>

<div class="left_col">
	<div class="left_filter">
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu", 
			"map", 
			array(
				"ALLOW_MULTI_SELECT" => "N",
				"CHILD_MENU_TYPE" => "left",
				"DELAY" => "N",
				"MAX_LEVEL" => "2",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"ROOT_MENU_TYPE" => "catalog",
				"USE_EXT" => "Y",
				"COMPONENT_TEMPLATE" => "porucheniya"
			),
			false
		);?>
	</div>
	<div class="banner_left">
		<img src="<?=SITE_TEMPLATE_PATH?>/images/banner2.jpg" alt="">
		<img src="<?=SITE_TEMPLATE_PATH?>/images/banner3.jpg" alt="">
	</div>
</div>
<div class="middle_col">
	<?if($_GET['kat']){?>
		<?include($_SERVER['DOCUMENT_ROOT'].'/includes/filter.php');?>
	<?}?>
	<?include($_SERVER['DOCUMENT_ROOT'].'/includes/list_for_map.php');?>
	<div id="res_element"></div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>