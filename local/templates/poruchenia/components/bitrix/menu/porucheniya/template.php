<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<ul class="nav_left">
		<?foreach($arResult as $key=>$arItem):
			$current_level = $arItem['DEPTH_LEVEL'];
			$next_level = $arResult[$key+1]['DEPTH_LEVEL'];
		?>
			<?if($arItem["IS_PARENT"]):?>
				<li class="<?if($arItem['SELECTED'])echo'active';?>">
					<a href="<?=$arItem['LINK']?>"><?=$arItem["TEXT"]?> </a>
					<ul <?if($arItem['SELECTED'] == false){?>style="display:none;"<?}?>>
			<?else:?>
				<li class="<?if($arItem['SELECTED'])echo'active';?>">
					<a href="<?=$arItem['LINK']?>"><?=$arItem["TEXT"]?></a>
				</li>
			<?endif;?>
			<?if(!$next_level) echo str_repeat("</ul></li>", $current_level-1);
			elseif($current_level>$next_level) echo str_repeat("</ul></li>", $current_level-$next_level);?>
		<?endforeach?>
	</ul>
<?endif?>
