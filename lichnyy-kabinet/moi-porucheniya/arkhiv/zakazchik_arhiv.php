<ul class="ibl user_menu">
	<li><a href="/lichnyy-kabinet/moi-porucheniya/">Активные поручения</a></li>
	<li class="separator"></li>
	<li  class="active"><a href="/lichnyy-kabinet/moi-porucheniya/arkhiv/">Архив поручений</a></li>
</ul>
<?
$date = strtotime('-3 days');
$date = date('Y-m-d H:i:s', $date);

CModule::IncludeModule('iblock');

$arSelect = Array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "DATE_CREATE", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*", "TIMESTAMP_X");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array(
	"IBLOCK_ID"=>2, 
	"ACTIVE_DATE"=>"Y", 
	"ACTIVE"=>"Y", 
	"PROPERTY_USER" => $arUser['ID'], 
	array(
        "LOGIC" => "OR",
        array("PROPERTY_STATUS" => 19),
        array("PROPERTY_STATUS" => 18, "<=PROPERTY_DATE_END" => $date)
    )
);

$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	$arFields['PROPERTIES'] = $ob->GetProperties();
	$arResult[] = $arFields;
}


$rsSect = CIBlockSection::GetList(array(),array('IBLOCK_ID' => 2), false, array('ID', 'IBLOCK_SECTION_ID', 'NAME'));
while ($arSect = $rsSect->GetNext()){
	$arSections[$arSect['ID']] = $arSect;
}

?>
<div class="wide_col">
	<?foreach($arResult as $arItem){?>
		<div class="user_item">
			<div class="row">
				<div class="h2"><a href="?ID=<?=$arItem['ID'];?>"><?=$arItem['NAME'];?></a></div>
				<div class="row">
					<div class="col cstate">
						<div class="state">Поручение <?=$arItem['ID'];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="date">от <?=$arItem['DATE_CREATE'];?></span></div>
					</div>
					<div class="col cprice">
						<div class="price_big" style="top: 2px"><em>P<em>&mdash;</em></em><?=number_format($arItem['PROPERTIES']['SUMMA']['VALUE'], 0, ' ', ' ');?> </div>
					</div>
					<div class="col cs2" style="width: 18%;">
						<?if($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 4){ //завершено?>
							<?$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$arItem['PROPERTIES']['ISPOLNITEL']['VALUE']),array('FIELD' => array('NAME', 'ID', 'PERSONAL_PHONE')));
							$arUserIspolnitel = $rsUser->Fetch();?>
							Выполнил 
							<span style="color: #2b3849; font-size: 14px;"> 
								<?$arDate = explode(':', $arItem['PROPERTIES']['DATE_END']['VALUE']);
								$date_end = $arDate[0].':'.$arDate[1];?>
								<?=$date_end;?>
							</span>
							<span class="name"><a class="tooltips" title="Контактный телефон: +<?=$arUserIspolnitel['PERSONAL_PHONE'];?>" href="/ispolniteli/<?=$arUserIspolnitel['ID'];?>/"><?=$arUserIspolnitel['NAME'];?></a></span>
						<?} elseif($arItem['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 6){ //отменено?>
							Отменено
						<?}?>        
					</div>
					<div class="col cs3" style="width: 23%;">
						<?if($arItem['PROPERTIES']['REVIEW']['VALUE']){?>
							Вас оценили &nbsp;&nbsp;
							<div class="rating_container" style="display: inline-block; padding: 0 0 8px;">
								<div class="rating readonly" data-rate-value="3.5"></div>
								<div class="count">3,5</div>
							</div>
							<a class="call_review" href="#call_user_review">Читать отзыв</a>
						<?}else{?>
							исполнитель еще не оставил отзыв
						<?}?>
						
					</div>
				</div>
				<?
				if($arSections[$arItem['IBLOCK_SECTION_ID']]){
					$copy_link = '/sozdat-poruchenie/';
					if($arSections[$arItem['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']){
						$copy_link .= '?kat='.$arSections[$arItem['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID'].'&sub='.$arItem['IBLOCK_SECTION_ID'];
					} else {
						$copy_link .= '?kat='.$arItem['IBLOCK_SECTION_ID'];
					}
					$copy_link .= '&id='.$arItem['ID'];
					$red_link = $copy_link.'&red=y';
					
				}
				?>
				<div class="icons">
					<a class="copy" title="копировать" href="<?=$copy_link;?>"></a>
				</div>
			</div>
		</div>
					
	<?}?>
</div>

