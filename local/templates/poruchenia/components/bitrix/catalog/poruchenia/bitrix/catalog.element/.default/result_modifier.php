<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<?

foreach($arResult['PROPERTIES']['FILE']['VALUE'] as $id){
	$file = CFile::GetFileArray($id);
	$arFileType = explode('/', $file['CONTENT_TYPE']);
	if($arFileType[0] == 'image'){
		$arResult['FOTO'][] = $file;
	} else {
		$arResult['FILES'][] = $file;
	}
	
}

if($arResult['PROPERTIES']['COORD']['VALUE']){
	$arPoint["type"] = "FeatureCollection";
	$i = 1;
	foreach($arResult['PROPERTIES']['ADDRESS']['VALUE'] as $key => $arAddress){
		$arCoord = explode(' ', $arResult['PROPERTIES']['COORD']['VALUE'][$key]);
		$arPoint["features"][0] = array(
			"type" => "Feature",
			"geometry" => array(
				"type" => "Point",
				"coordinates" => array(
					0 => $arCoord[1],
					1 => $arCoord[0]
				)
			),
			"properties" => array(
				"balloonContent" => $arAddress,
				"hintContent" => $arAddress,
				"iconContent" => $i
			),
			"options"=> array(
				
				 "preset" => "twirl#blueStretchyIcon"
				
			)
			
		);
		$i++;
	}

	$arResult['point'] = json_encode($arPoint);
}

$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array('ID'=>$arResult['PROPERTIES']['USER']['VALUE']),array("SELECT"=>array("UF_*"), 'FIELD' => array('NAME', 'ID', 'PERSONAL_NOTES')));
if($arUser = $rsUser->Fetch()){
	if($arUser['PERSONAL_PHOTO']){
		$img = CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array("width"=>63, "height"=>63), BX_RESIZE_IMAGE_EXACT, true);
		$arUser['PHOTO'] = $img['src'];
	} else {
		$arUser['PHOTO'] = SITE_TEMPLATE_PATH.'/images/user_icon.png';
	}	
	
	$arResult['USER'] = $arUser;
}
?>
