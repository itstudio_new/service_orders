<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');
$arSelect = Array("ID", "IBLOCK_ID",  "DATE_CREATE", "NAME", "PROPERTY_ISPOLNITEL");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array("IBLOCK_ID"=>2, "ID" => $_POST["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
if($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields(); 
	$arFields['PROPERTIES'] = $ob->GetProperties();
	$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID" => $arFields['PROPERTIES']['ISPOLNITEL']['VALUE']),array("SELECT"=>array("UF_*"), 'FIELD' => array('NAME', 'ID')));
	if($arUser = $rsUser->Fetch()){?>
	
		<h2>Оцените работу заказчика</h2>
		<div class="name"><?=$arUser['NAME'];?></div>
		<div class="num">В задании №<?=$arFields['ID'];?></div>
		<form id="accept_poruchenie">

				<input type="hidden" name="ID" value="<?=$arFields['ID'];?>" />
				<input type="hidden" name="USER" value="<?=$arFields['PROPERTIES']['ISPOLNITEL']['VALUE'];?>" />
				<input type="hidden" name="rating" value="" />
				<div class="rating_container" style="text-align: center; margin: 0 auto; width: 120px; padding: 0 0 10px 0;">
					<div class="rating_accept"  data-rate-value="0"></div>
					<div class="count"></div>
				</div>
				<div class="inner">
					<textarea class="textbox" placeholder="Вы можете оставить отзыв о работе заказчика" name="text" style="height: 260px"></textarea>
				</div>

			<div class="submit_button" style="text-align: center; margin: 20px 0 0 0; ">
				<input type="submit" class="submit" value="Готово">
			</div>
		</form>
<?
	}
}
?>	