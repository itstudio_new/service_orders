<div class="fileupload_container">

	 <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
        <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">

                <button type="submit" class="btn btn-primary start" style="display:none;">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
		
		<div class="row fileupload_buttonbar">
			<div class="col-lg-3">

				<label class="fileinput_button">
					<span>Добавьте файл</span>
					<input type="file" name="files[]" multiple="">
				</label>
				
				
				<span class="fileupload-process"></span>
			</div>

			<div class="col-lg-6">
				Вы можете добавить файлы или фотографии, если это поможет исполнителям лучше понять ваше поручение
			</div>

		</div>
		<div class="row">
			<div class="drop_zone" id="#drop">
				<div class="inner">
					<div class="td">
					Перетащите файлы сюда
						</div>
				</div>
			</div>
		</div>
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
    </form>
</div>