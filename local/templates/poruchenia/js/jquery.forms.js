﻿/**
* ...
* @author ser
* @version 1.0
* @current project forms_elelemets
*/
(function ($) {
    $.fn.forms = function (settings) {
        settings = jQuery.extend({

    }, settings);

    var forms = this;

    //select
    forms.select_init = function (sel) {
        var s = sel.find('select');
        s.hide();
        var template;

		if (s.find('option[data-selected=1]').length > 0) {
			template = "<div>" + s.find(':selected').text() + "</div>";
		} else {
			template = "<div>" + s.attr('data-placeholder') + "</div>";
		}
 
		
		//template = "<div>" + s.find(':selected').text() + "</div>";
        template += "<ul>";
        for (var i = 0; i < s.find('option').length; i++) {
            template += "<li><a href=\"#\">" + s.find('option:eq(' + i + ')').text() + "</a></li>";
        }
        template += "</ul>";
        sel.append(template);
    }

    this.select = $('.custom_select');
    $('.custom_select').each(function (i) {
        forms.select_init($(this));
        $(this).attr('data-id', 'select' + (i+1));
    });

    $('body').on('mouseleave', '.custom_select', function () { $(this).find('ul').hide(); $(this).removeAttr('style'); });
    $('body').on('click touchstart', '.custom_select', function () { $(this).css('z-index', 100); $(this).find('ul').show() });

    $('body').on('click touchend', '.custom_select ul li a', function () {
        var _parent = $(this).parents('.custom_select');
        var sel = _parent.find('select');
        var input = _parent.find('div');
        var ul = _parent.find('ul');
        ul.find('li').removeClass('active');
        var li = $(this).parent('li').addClass('active');
        var value = $(this).text();
        input.text(value);
        sel.find('option:contains("' + value + '")').prop('selected', true);
        ul.hide();
        
        if (typeof forms.select_change === 'function') {
            forms.select_change(sel);
        }

		sel.change();
		
        return false;
    });


    //custom_select_radio
    $('.custom_select_radio').each(function(i){
        if ($(this).find('input').is(':checked')) {
            var input = $(this).find('div');
            var value = $(this).find('input:checked').parent('label').text();
            input.text(value);
            $(this).attr('data-id', 'select_radio' + (i + 1));
        }
    });
    $('body').on('mouseleave', '.custom_select_radio', function () { $(this).find('ul').hide() });
    $('body').on('click', '.custom_select_radio', function () { $(this).find('ul').show() });

    $('body').on('click', '.custom_select_radio ul li input', function () {
        var name = $(this).attr('name');
        var _parent = $(this).parents('.custom_select_radio');
        var input = _parent.find('div');
        var ul = _parent.find('ul');
        ul.find('li').removeClass('active');
        var li = $(this).parent('li').addClass('active');
        var value = $(this).parent('label').text();
        
        _parent.find('input').each(function(){
            if ($(this).attr('name') != name) {
                $(this).prop('checked', false)
            }
        })
        input.text(value);
        ul.hide();
        

        //return false;
    });

    //checkbox_select
    $('body').on('click', '.checkbox_select div', function () {
        $(this).parent('.checkbox_select').find('ul').show();
    });
    $('body').on('mouseleave', '.checkbox_select', function () { $(this).find('ul').hide() });

    $('.checkbox_select').each(function (i) {
        $(this).find('input').each(function () {
            if ($(this).is(':checked')) {
                $(this).parents('li').addClass('active');
                if ($('.checkbox_select').hasClass('main_select')) {
                    $(this).closest('.main_select').find('div').append('<p><a class="sdel" data-index="' + $(this).parents('li').index() + '" href="#">' + $(this).parents('li').find('label').text() + '</a></p>');
                } else {
                    $(this).closest('.checkbox_select').append('<p><a class="sdel" data-index="' + $(this).parents('li').index() + '" href="#">' + $(this).parents('li').find('label').text() + '</a></p>');
                }
            } else {
                $(this).parents('li').removeClass('active');
                if ($('.checkbox_select').hasClass('main_select')) {
                    $(this).closest('.main_select').find('div').find('[data-index="' + $(this).parents('li').index() + '"]').parent('p').remove();
                } else {
                    $(this).closest('.checkbox_select').find('[data-index="' + $(this).parents('li').index() + '"]').parent('p').remove();
                }
            }
        })
        $(this).attr('data-id', 'checkbox_select' + (i + 1));
    });

    
    function chkSelectFixed(o) {
        o.find('p').remove();
        o.each(function () {
            $(this).find('input').each(function () {
                if ($(this).is(':checked')) {
                    $(this).closest('.checkbox_select').append('<p><a class="sdel" data-index="' + $(this).parents('li').index() + '" href="#">' + $(this).parents('li').find('label').text() + '</a></p>');
                } else {
                    $(this).closest('.checkbox_select').find('[data-index="' + $(this).parents('li').index() + '"]').parent('p').remove();
                }
            })

        });
    }
    function chkMainFixed(o) {
        var _text = o.find('div');
        _text.html("");
        o.each(function () {
            $(this).find('input').each(function () {
                if ($(this).is(':checked')) {
                    _text.append('<p><a class="sdel" data-index="' + $(this).parents('li').index() + '" href="#">' + $(this).parents('li').find('label').text() + '</a></p>');
                }
            })

        });
    }

    $('body').on('click', '.checkbox_select p a.sdel', function () {
        var _item = $(this).closest('.checkbox_select').find('ul li:eq(' + $(this).attr('data-index') + ')');
        _item.removeClass('active');
        _item.find('input').prop('checked', false)
        $(this).parent('p').remove();
        return false;
    });

    $('.checkbox_select ul li').click(function () {
        if ($(this).find('input').is(':checked')) {
            $(this).removeClass('active');
            $(this).find('input').prop('checked', false);
            if ($(this).closest('.checkbox_select').hasClass('main_select')) {
                chkMainFixed($(this).closest('.main_select'));
            } else {
                chkSelectFixed($(this).closest('.checkbox_select'));
            }
        } else {
            $(this).addClass('active');
            $(this).find('input').prop('checked', true);
            if ($(this).closest('.checkbox_select').hasClass('main_select')) {
                chkMainFixed($(this).closest('.main_select'));
            } else {
                chkSelectFixed($(this).closest('.checkbox_select'));
            }

        }
        return false;
    });

    //checkbox
    $('.custom_checkbox').each(function (i) {
        if ($(this).find('input').is(':checked')) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
        $(this).attr('data-id', 'checkbox' + (i + 1));
    });
    $('body').on('click', '.custom_checkbox', function () {
        if ($(this).find('input').is(':checked')) {
            $(this).removeClass('active');
            $(this).find('input').prop('checked', false);

            if ($(this).hasClass('gabarit_check')) {
                $('#gabarit_block').hide();
            }
        } else {
            $(this).addClass('active');
            $(this).find('input').prop('checked', true);
            if ($(this).hasClass('gabarit_check')) {
                $('#gabarit_block').show();
            }
        }
		$(this).find('input').change();
		return false;
    });

    //radiobutton
    $('.custom_radio').each(function (i) {
        if ($(this).find('input').is(':checked')) {
            $(this).addClass('active');
            if ($(this).find('input').is("[data-id]")) {
                $($(this).find('input').attr("data-id")).show();
            }
        } else {
            $(this).removeClass('active');
        }
        $(this).attr('data-id', 'radio' + (i + 1));
    });
    $('body').on('click', '.custom_radio', function () {
        var radio = $(this).find('input');
        var group = $(this).find('input').attr('name');
        if ($(this).find('input').is(':checked')) {
            return false;
        } else {
            $('[name="' + group + '"]').each(function () { $(this).parent().removeClass('active'); });
            $('[name="' + group + '"]').removeAttr('checked');
            if (radio.is("[data-id]")) {
                $('.radio_description').hide();
                $(radio.attr("data-id")).show();
            }
            $(this).addClass('active');
            $(this).find('input').prop("checked", true);
            return false;
        }
    });

    //upload
    $('body').on('change', '.custom_upload .file', function () {
        $(this).parent().find('input.text').val($(this).val());
    })

    //plus-minus
    $('body').on('click', '.custom_count .next', function () {
        var inp = $(this).closest('.custom_count').find('input');
        inp.val(parseInt(inp.val()) + 1);
    });
    $('body').on('click', '.custom_count .prev', function () {
        var inp = $(this).closest('.custom_count').find('input');
        if (parseInt(inp.val()) > 0) inp.val(parseInt(inp.val()) - 1);
    })

    forms.getCurrent = function (el) {
        return $('[data-id="' + el + '"]');
    }

    return this;

};
})(jQuery);    