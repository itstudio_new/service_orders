<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
$curDir = $APPLICATION->GetCurDir(true);



$arSelect = Array("ID", "NAME", "PROPERTY_AVTO", "PROPERTY_MEJGOROD", "PROPERTY_GRUZCHIKI", "PROPERTY_HOT", "PROPERTY_ONE_ADDR");
$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", 'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'], 'INCLUDE_SUBSECTIONS' => 'Y');
//если на странице карта поручений
if($curDir == '/karta-porucheniy/'){
	if($_GET['kat']){
		$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", 'SECTION_ID' => $_GET['kat'], 'INCLUDE_SUBSECTIONS' => 'Y', "!PROPERTY_COORD" => false);
	}
	if($_GET['sub']){
		$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", 'SECTION_ID' => $_GET['sub'], 'INCLUDE_SUBSECTIONS' => 'Y', "!PROPERTY_COORD" => false);
	} 
}

$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	$arElements[] = $arFields;
}
$filter = array();
foreach($arElements as $arElement){
	if($arElement['PROPERTY_AVTO_VALUE']){
		$filter['AVTO'] = 1;
	}
	if($arElement['PROPERTY_MEJGOROD_VALUE']){
		$filter['MEJGOROD'] = 1;
	}
	if($arElement['PROPERTY_GRUZCHIKI_VALUE']){
		$filter['GRUZCHIKI'] = 1;
	}
	if($arElement['PROPERTY_HOT_VALUE']){
		$filter['HOT'] = 1;
	}
	if($arElement['PROPERTY_ONE_ADDR_VALUE']){
		$filter['ONE_ADDR'] = 1;
	}
}

if($_GET['custom']){
	$GLOBALS['arrFilter']['>=PROPERTY_SUMMA'] = $_GET['custom'];
}
if($_GET['avto']){
	$GLOBALS['arrFilter']['PROPERTY_AVTO_VALUE'] = "Да";
}
if($_GET['mejgorod']){
	$GLOBALS['arrFilter']['PROPERTY_MEJGOROD_VALUE'] = "Да";
}
if($_GET['gruzchiki']){
	$GLOBALS['arrFilter']['PROPERTY_GRUZCHIKI_VALUE'] = "Да";
}
if($_GET['hot']){
	$GLOBALS['arrFilter']['PROPERTY_HOT_VALUE'] = "Да";
}
if($_GET['one_addr']){
	$GLOBALS['arrFilter']['PROPERTY_ONE_ADDR_VALUE'] = "Да";
}
?>


<div class="map_top_filter">
	<form id="filter" method="get">
	
		<?//если на странице карта поручений
		if($curDir == '/karta-porucheniy/'){?>
			<?if($_GET['kat']){?>
				<input type="hidden" name='kat' value="<?=$_GET['kat']?>" />
			<?}?>
			<?if($_GET['sub']){?>
				<input type="hidden" name='sub' value="<?=$_GET['sub']?>" />
			<?}?>
		<?}?>
		
		<div class="row top_row">
			<div class="tl">Бюджет от: <input class="textbox" name="custom" style="width: 110px" type="text" value="<?=$_GET['custom'];?>"> руб</div>
			<div class="tr">
				<div class="review_row row">
					<div class="rating_container readonly">
						<div class="name">Рейтинг заказчика от: </div>
						<div class="rating" data-rate-value="3.5"></div>
						<div class="count"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<?if($filter['AVTO']){?>
				<label class="custom_checkbox" style="display: inline-block">
					<input type="checkbox" name="avto" <?if($_GET['avto']){?>checked="checked"<?}?>>
					<span class="mtop_icon icon1">Нужен автомобиль</span>
				</label>
			<?}?>
			<?if($filter['HOT']){?>
				<label class="custom_checkbox" style="display: inline-block">
					<input type="checkbox" name="hot" <?if($_GET['hot']){?>checked="checked"<?}?>>
					<span class="mtop_icon icon2">Срочный заказ</span>
				</label>
			<?}?>	
			<?if($filter['ONE_ADDR']){?>
				<label class="custom_checkbox" style="display: inline-block">
					<input type="checkbox" name="one_addr" <?if($_GET['one_addr']){?>checked="checked"<?}?>>
					<span class="mtop_icon icon3">Один адрес</span>
				</label>
			<?}?>	
			<?if($filter['GRUZCHIKI']){?>
				<label class="custom_checkbox" style="display: inline-block">
					<input type="checkbox" name="gruzchiki"  <?if($_GET['gruzchiki']){?>checked="checked"<?}?> />
					<span class="mtop_icon icon7">С грузчиками</span>
				</label>
			<?}?>	
			<?if($filter['MEJGOROD']){?>
				<label class="custom_checkbox" style="display: inline-block">
					<input type="checkbox" name="mejgorod"  <?if($_GET['mejgorod']){?>checked="checked"<?}?> />
					<span class="mtop_icon icon5">Междугородняя перевозка</span>
				</label>
			<?}?>	
		</div>
		
		
	</form>
</div>
<script type="text/javascript"> 
	$(document).ready(function(){
		$('form#filter input[type=checkbox]').change(function(){
			$('form#filter').submit();
		});
		$('form#filter input[name=custom]').focusout(function(){
			$('form#filter').submit();
		});
	});	
</script>
<pre><?//print_r ($arElements);?></pre>