<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?$APPLICATION->ShowTitle()?></title>
	<? 
	CJSCore::Init(array('ajax'));
	global $USER;
	$dir = $APPLICATION->GetCurDir();
		  function ShowCanonical(){ 
			 global $APPLICATION; 
				if ($APPLICATION->GetProperty("canonical")!="" && $APPLICATION->GetProperty("canonical")!=$APPLICATION->sDirPath){
					  return '<link rel="canonical" href="'.$APPLICATION->GetProperty("canonical").'" />'; 
				} 
				else {
					  return false;
				} 
		  } 
		  $APPLICATION->AddBufferContent('ShowCanonical');
			if ($_GET) {
			   $APPLICATION->SetPageProperty('canonical', $dir);
			}
    ?>   
	<?$APPLICATION->ShowHead()?>
	
	<?
		$APPLICATION->SetAdditionalCSS("https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i,900,900i&amp;subset=cyrillic");
		$APPLICATION->SetAdditionalCSS("https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,700,700i&amp;subset=cyrillic");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/styles/adaptive.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/styles/template_styles.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/styles/styles.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/styles/my_styles.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/styles/styles.additional.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/styles/scroll/jquery.mCustomScrollbar.css");
		
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-1.10.2.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.transit.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.maskedinput.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.touchSwipe.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/rater.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.mousewheel.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.customDialog.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.customSlider.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.forms.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.fancybox.js");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/styles/jquery.fancybox.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/styles/jquery.fancybox-buttons.css");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.fancybox-buttons.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.jcarousel.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.preview.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.submenu.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.timer.js");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/styles/jcarousel.responsive.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/styles/jquery-ui-1.11.4.custom/jquery-ui.css");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/styles/jquery-ui-1.11.4.custom/jquery-ui.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.mCustomScrollbar.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/scripts.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/scripts.mechanics.js");
		
	?>

	
	<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="styles/ie.css" /><![endif]-->
</head>
<body>
<? if ($USER->IsAdmin()) { ?>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<?}?>

<div class="all_page">
        <header>
            <div class="fixed">
                <div class="flex flj">
                    <a class="logo" <?if($GLOBALS["APPLICATION"]->GetCurPage() != '/'):?>href="/"<?endif;?> title="Сервис поручений"></a>
                    <div>
                        <?$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"top_menu",
							Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "left",
								"COMPONENT_TEMPLATE" => ".default",
								"DELAY" => "N",
								"MAX_LEVEL" => "1",
								"MENU_CACHE_GET_VARS" => "",
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"ROOT_MENU_TYPE" => "top",
								"USE_EXT" => "N"
							)
						);?>
                    </div>
                    <div>
                        <a class="add_mission" href="/sozdat-poruchenie/">Создать поручение</a>
						<?if ($USER->IsAuthorized()){?>
							<div class="user">
								<a  href="/lichnyy-kabinet/"><?echo ($USER -> GetFullName())?> </a>
								<ul class="user_submenu">
									<li><a href="/lichnyy-kabinet/moi-porucheniya/" title="Мои поручения">Мои поручения</a></li>
									<li><a href="/lichnyy-kabinet/" title="Моя страница">Моя страница</a></li>
									<li><a href="/lichnyy-kabinet/nastroyki/" title="Настройки">Настройки</a></li>
									<li><a href="<?echo $APPLICATION->GetCurPageParam("logout=yes", array("login","logout","register","forgot_password","change_password"));?>">Выход</a></li>
								</ul>
							</div>
						<?}else{?>
							<a href="#call_enter">Войти на сайт</a>
						<?}?>
                    </div>
                </div>
            </div>
        </header>
        <div class="search_line">
            <div class="fixed">
                <div class="city">
                    <em>Ваш город:</em>
                    <div class="dadata_address_city">
			            <input id="addresst" name="address" type="text" size="100">
			            <link href="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/css/suggestions.css" type="text/css" rel="stylesheet">
			            <!--[if lt IE 10]>
			            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
			            <![endif]-->
			            <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.5.3/js/jquery.suggestions.min.js"></script>
			            <script type="text/javascript">
			                $("#addresst").suggestions({
			                    serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
			                    token: "09d08b8bed5266658a946ab6eb5e7bd20791aa72",
			                    type: "ADDRESS",
			                    count: 5,
			                    /* Вызывается, когда пользователь выбирает одну из подсказок */
			                    onSelect: function (suggestion) {
			                        console.log(suggestion);
			                    }
			                });
			            </script>
		            </div>
                </div>
                <div class="header_search">
                    <div class="search">
                        <form action="#">
                            <input class="text" type="text" placeholder="Начните вводить ключевые слова" value="">
                            <input class="submit" type="submit" value="">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper">
		<!-- это закрывается в футере -->
			<div class="content">
				<div class="fc">
					<h1><?=$APPLICATION->ShowTitle(false)?></h1>