<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Создать поручение");

echo '
<div class="make_top">
	<div class="h2">Как это работает?</div>
	<div class="description">
		<div class="make_list flex flj">
			<div class="item item1">
				<div class="text">
					Вы создаете поручение, и оно попадает в ваш каталог и карту поручений
				</div>
				<div class="num">1</div>
			</div>
			<div class="arrow"></div>
			<div class="item item2">
				<div class="text">
					Вы получаете заявки от исполнителей и выбираете того, кто вам понравился
				</div>
				<div class="num">2</div>
			</div>
			<div class="arrow"></div>
			<div class="item item3">
				<div class="text">
					Ваше поручение выполнено, а вы можете оставить отзыв об исполнителе
				</div>
				<div class="num">3</div>
			</div>
		</div>
	</div>
</div>';
?>

<?
CModule::IncludeModule('iblock');
$rsSect = CIBlockSection::GetList(array('sort' => 'asc'), array('IBLOCK_ID' => 2), false, array('NAME', 'ID', 'IBLOCK_SECTION_ID', 'DEPTH_LEVEL'));
while ($arSect = $rsSect->GetNext()){
	$arTemp[] = $arSect;
}
foreach($arTemp as $arSect){
	if($arSect['DEPTH_LEVEL'] == 1) $arResult[$arSect['ID']] = $arSect;
}
foreach($arTemp as $arSect){
	if($arSect['DEPTH_LEVEL'] == 2) $arResult[$arSect['IBLOCK_SECTION_ID']]['SUBSECTION'][] = $arSect;
}

if($_GET['id']){
	$arSelect = Array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "PREVIEW_TEXT", "DETAIL_TEXT", "DATE_CREATE", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*", "TIMESTAMP_X");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
	$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID" => $_GET['id'], 'PROPERTY_USER' => $USER->GetID());
	if($_GET['red']){
		$arFilter['PROPERTY_COUNT'] = false;
	}

	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	if($ob = $res->GetNextElement()){ 
		$arFields = $ob->GetFields();  
		$arFields['PROPERTIES'] = $ob->GetProperties();
		$arElement = $arFields;
		$my_dir = time();
		$_SESSION['MY_DIR'] = $my_dir;
		mkdir($_SERVER['DOCUMENT_ROOT'].'/includes/php/'.$my_dir);
		
		foreach($arElement['PROPERTIES']['FILE']['VALUE'] as $fileID){
			$arFile = CFile::GetFileArray($fileID);
			$file_path = $_SERVER['DOCUMENT_ROOT'].$arFile['SRC'];
			$new_name = $_SERVER['DOCUMENT_ROOT'].'/includes/php/'.$my_dir.'/'.$arFile['FILE_NAME'];
			copy($file_path, $new_name);
		}
	} else {
		LocalRedirect('/sozdat-poruchenie/');
	}
	if($arElement['PROPERTIES']['COORD']['VALUE']){
		$arPoint["type"] = "FeatureCollection";
		$i = 1;
		foreach($arElement['PROPERTIES']['ADDRESS']['VALUE'] as $key => $arAddress){
			$arCoord = explode(' ', $arElement['PROPERTIES']['COORD']['VALUE'][$key]);
			$arPoint["features"][0] = array(
				"type" => "Feature",
				"geometry" => array(
					"type" => "Point",
					"coordinates" => array( 
						0 => $arCoord[1],
						1 => $arCoord[0]
					)
				),
				"properties" => array(
					"balloonContent" => $arAddress,
					"hintContent" => $arAddress,
					"iconContent" => $i
				),
				"options"=> array(
					
					 "preset" => "twirl#blueStretchyIcon"
					
				)
				
			);
			$i++;
		}

		$arElement['point'] = json_encode($arPoint);
	}
}

$arForm = array(
	1 => 'kurier_uslugu.php',
	2 => 'bytovye_uslugu.php',
	3 => 'muzh_na_chas.php',
	4 => 'osmotr_avto.php',
	5 => 'osmotr_pokupok.php',
	6 => 'perevozki.php',
	7 => 'uhod_za_zahoroneniem.php',
	8 => 'dop_uslugi.php'
);
?>
<?/*
<pre><?print_r ($arTemp);?></pre>
*/?>
<div class="middle_col">
	<div class="product">
		<div class="row">
			<div class="left_make_col" current-id="<?=$_GET['kat'];?>">
				<div class="custom_select">
					<select id="sel1" data-placeholder="Выберите категорию">
						<?foreach($arResult as $arSection){?>
							<option value="<?=$arSection['ID']?>" <?if($_GET['kat'] == $arSection['ID']){?>selected data-selected="1"<?}?>><?=$arSection['NAME']?></option>
						<?}?>
					</select>
				</div>
			</div>
			
			<?foreach($arResult as $arSection){?>
				<div class="right_make_col" data-id="<?=$arSection['ID'];?>" <?if($_GET['kat'] != $arSection['ID']){?>style="display:none;"<?}?>>
					<?if($arSection['SUBSECTION']){?>
						<div class="custom_select">
							<select class="sel2" data-placeholder="Выберите подкатегорию">
								<?foreach($arSection['SUBSECTION'] as $arSubSection){?>
									<option value="<?=$arSubSection['ID']?>" <?if($_GET['sub'] == $arSubSection['ID'] && isset($_GET['sub'])){?>selected data-selected="1"<?}?>><?=$arSubSection['NAME']?></option>
								<?}?>
							</select>
						</div>
					<?}?>
				</div>
			<?}?>
			<div style="clear:both;"></div>	
			<div id="form">
				<?if($_GET['kat'] && $arForm[$_GET['kat']]){
					include($_SERVER['DOCUMENT_ROOT'].'/includes/forms/'.$arForm[$_GET['kat']]);
				}?>
			</div>
		</div>
		<!-- адреса -->
	</div>
</div>
<script>
	$(document).ready(function(){
		
		arForm = [];
		arForm[1] = 'kurier_uslugu.php';
		arForm[2] = 'bytovye_uslugu.php';
		arForm[3] = 'muzh_na_chas.php';
		arForm[4] = 'osmotr_avto.php';
		arForm[5] = 'osmotr_pokupok.php';
		arForm[6] = 'perevozki.php';
		arForm[7] = 'uhod_za_zahoroneniem.php';
		arForm[8] = 'dop_uslugi.php';
		
		$('#sel1').change(function(){
			var  val = $(this).val();
			$('.right_make_col').hide();
			$('.right_make_col .custom_select div').text('Выберите подкатегорию');
			if(val != 8)
				$('.right_make_col[data-id='+val+']').show();
			$('#form').html('');
			$('.left_make_col').attr('current-id', 0);
			if(val == 8){
				var link = "?kat="+8;
				window.location.href = link;
			} 
		});
		
		$('.sel2').change(function(){
			var  id = $('#sel1').val();
			var  id_sub = $(this).val();
			var file = arForm[id];
			var current_id = $('.left_make_col').attr('current-id');
			if(current_id == 8){
				var link = "?kat=8&sub="+id_sub;
				window.location.href = link;
			}
			if(current_id == false){
				var link = "?kat="+id+"&sub="+id_sub;
				window.location.href = link;
			} else {
				var link = "?kat="+id+"&sub="+id_sub;
				history.pushState('', '', link);
			}
		});
	});	
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
