<?
// регистрируем последнюю активность пользователя, если модуль соцсетей не установлен 
if(!IsModuleInstalled('socialnetwork')) { 
	AddEventHandler('main', 'OnBeforeProlog', 'CustomSetLastActivityDate'); 
	function CustomSetLastActivityDate() { 
		if($GLOBALS['USER']->IsAuthorized()) { 
			CUser::SetLastActivityDate($GLOBALS['USER']->GetID()); 
		} 
	} 
}

function removeDirectory($dir) {
	if ($objs = glob($dir."/*")) {
		foreach($objs as $obj) {
			is_dir($obj) ? removeDirectory($obj) : unlink($obj);
		}
	}
	rmdir($dir);
}

function object_to_array($data){
	if (is_array($data) || is_object($data))
	{
		$result = array();
		foreach ($data as $key => $value)
		{
			$result[$key] = object_to_array($value);
		}
		return $result;
	}
	return $data;
}

function endingsForm($n, $form1, $form2, $form5) 
{
	$n = abs($n) % 100;
	$n1 = $n % 10;
	if ($n > 10 && $n < 20) return $form5;
	if ($n1 > 1 && $n1 < 5) return $form2;
	if ($n1 == 1) return $form1;
	return $form5;
}

function dateDifference($date_1,  $date_2, $differenceFormat = '%y %m %d %h %i' )
{
	$datetime1 = date_create($date_1);
	$datetime2 = date_create($date_2);
	
	$interval = date_diff($datetime1, $datetime2);
	
	return $interval->format($differenceFormat);
	
}
?>