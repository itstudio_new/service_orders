<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Исполнители");
if($_GET['ID']){
	CModule::IncludeModule('iblock');
	$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array('ID' => $_GET['ID'], 'ACTIVE' => 'Y', 'UF_TYPE' => 1),array("SELECT"=>array("UF_*"), 'FIELD' => array('NAME', 'ID')));
	if($arUser = $rsUser->Fetch()){
		if($arUser['PERSONAL_PHOTO']){
			$img = CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array("width"=>190, "height"=>251), BX_RESIZE_IMAGE_EXACT, true);
			$arUser['PHOTO'] = $img['src'];
		} else {
			$arUser['PHOTO'] = SITE_TEMPLATE_PATH.'/images/photo_med.jpg';
		}	
		
		$db = CUser::GetList($by, $order, array("LAST_ACTIVITY"=>120, 'ID' =>$_GET['ID'])); 
		if($dba = $db->Fetch()){ 
			$online = true;
		}
		
		if($online == false){
			$arDateReg = dateDifference($arUser['DATE_REGISTER'] ,date());
			$arDateReg = explode(' ', $arDateReg);
			$arDateRegFormat['y'] = $arDateReg[0];
			$arDateRegFormat['m'] = $arDateReg[1];
			$arDateRegFormat['d'] = $arDateReg[2];
		}
		$userCheck = true;
		
		$APPLICATION->SetTitle($arUser['NAME']);
		
		
		//***отзывы***//
		
		
		$res = CIBlockElement::GetList(Array('DATE_ACTIVE_FROM'=>'ASC'), array('IBLOCK_ID' => 4, 'PROPERTY_ISPONITEL' => $_GET['ID']), false, false, array('NAME', 'PREVIEW_PICTURE', 'PREVIEW_TEXT', 'DATE_ACTIVE_FROM', 'PROPERTY_NAME', 'PROPERTY_ISPOLNITEL', 'PROPERTY_MARK', 'PROPERTY_ZAKAZCHIK', 'PROPERTY_KATEGORIYA', 'PROPERTY_PORUCHENIE', 'PROPERTY_SUB_KATEGORIYA'));
		while($ob = $res->GetNextElement()){ 
			$arFields = $ob->GetFields();
			$kategory[] = $arFields['PROPERTY_KATEGORIYA_VALUE'];
			$kategory_sub[] = $arFields['PROPERTY_SUB_KATEGORIYA_VALUE'];
			if($arFields['PREVIEW_PICTURE']){
				$img = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array("width"=>128, "height"=>128), BX_RESIZE_IMAGE_EXACT, true);
				$arFields['PHOTO'] = $img['src'];
			} else {
				$arFields['PHOTO'] = SITE_TEMPLATE_PATH.'/images/photo.jpg.jpg';
			}
			$arReviews[] = $arFields;
		}
		
		$arSectID = array_merge($kategory, $kategory_sub);
		//разделы для отзывов
		$arFilter = array('IBLOCK_ID' => 2, 'ID' => $arSectID); 
		$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter, false, array('ID', 'IBLOCK_SECTION_ID', 'NAME'));
		while ($arSect = $rsSect->GetNext()){
			$arSection[] = $arSect;
		}
		
		$temp = array();
		
		foreach($arSection as $arSect){
			if($arSect['IBLOCK_SECTION_ID'] == false){
				$temp[$arSect['ID']] = $arSect;
			}
		}
		foreach($arSection as $arSect){
			if($arSect['IBLOCK_SECTION_ID']){
				$temp[$arSect['IBLOCK_SECTION_ID']]['SUB'] = $arSect;
				$arSelect[$arSect['ID']] = $temp[$arSect['IBLOCK_SECTION_ID']]['NAME'].'/ '.$arSect['NAME'];
			}
		}		
		
		
		
		//***отзывы***//
		
	} else {
		LocalRedirect('/404.php');
	}
}
if($userCheck){ //страница пользователя ?>

	 <div class="wide_col">
		<div class="row user_detail">
			<div class="pic">
				<div class="pic_cnt">
					<img  src="<?=$arUser['PHOTO'];?>" alt="">
				</div>
			</div>
			<div class="description">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<div class="name">
							<?=$arUser['NAME'];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span><?if($online){?>Сейчас на сайте<?}?></span>
						</div>
						<div class="duration">
							На сайте 
							<?if($arDateRegFormat['y']){
								echo $arDateRegFormat['y'].' '.endingsForm($arDateRegFormat['y'], 'год', 'года', 'лет'); 
							}?>
							<?if($arDateRegFormat['m']){
								echo $arDateRegFormat['m'].' '.endingsForm($arDateRegFormat['m'], 'месяц', 'месяца', 'месяцев');
							}?>
							<?if($arDateRegFormat['d']){
								echo $arDateRegFormat['d'].' '.endingsForm($arDateRegFormat['d'], 'день', 'дня', 'дней');
							}?>
						</div>
						<div class="text">Успешно выполнил 585 поручений в 5 категориях</div>
						<div class="rating_container" style="display: inline-block; padding: 0 0 10px 0;">
							<div class="rating readonly" data-rate-value="3.5"></div>
							<div class="count"><?=$arUser['UF_RATING'];?></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<div class="state">
							<?if($arUser['UF_TRIED']){?>
								<img  src="<?=SITE_TEMPLATE_PATH?>/images/handup.png" alt="">&nbsp;
								Проверенный исполнитель
							<?}?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-8">
						<p><?=$arUser['WORK_NOTES'];?></p>
					</div>
				</div>
			</div>
		</div>
		<h2 class="user_title">Отзывы</h2>
		<div class="row user_detail_filter">
			<div class="col-xs-12 col-sm-12 col-md-4">
				<div class="custom_select">
					<form id="type_review" method="get">
						<input type="hidden" name="ID" value="<?=$_GET['ID'];?>" />
						<select name="review" data-placeholder="Все отзывы">
							<option value="all">Все отзывы</option>
							<?foreach($arSelect as $id => $name){?>
								<option value="<?=$id;?>" <?if($_GET['review'] == $id){?>selected data-selected="1"<?}?>><?=$name;?></option>
							<?}?>
						</select>
					</form>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-1"></div>
			<div class="col-xs-12 col-sm-12 col-md-4">
				<?$count = 0;
				$mark = 0;
				foreach($arReviews as $arReviewItem){
					if($_GET['review'] != false && $_GET['review'] != 'all'){
						if($arReviewItem['PROPERTY_SUB_KATEGORIYA_VALUE'] != $_GET['review'])
							continue;
					}
					$mark = $mark + $arReviewItem['PROPERTY_MARK_VALUE'];
					$count++;
				}
				$middleMark = $mark / $count;
				$middleMark = intval($middleMark*10)/10;
				?>
				<div class="rating_big_container" style="display: inline-block; padding: 10px 0 10px 10px;">
					<div class="text">Оценка в этой категории</div>
					<div class="rating_big readonly" data-rate-value="<?=$middleMark;?>"></div>
					<div class="count"><?=$middleMark;?></div>
				</div>
			</div>
		</div>
		<?foreach($arReviews as $arReviewItem){?>
			<?if($_GET['review'] != false && $_GET['review'] != 'all'){
				if($arReviewItem['PROPERTY_SUB_KATEGORIYA_VALUE'] != $_GET['review']) continue;
			}?>
			<div class="user_item">
				<div class="row">
					<div class="pic text_left">
						<img  src="<?=$arFields['PHOTO'];?>" alt="">
					</div>
					<div class="description">
						<p>от заказчика &nbsp;&nbsp;&nbsp; <span class="name"><?=$arReviewItem['PROPERTY_NAME_VALUE'];?></span> &nbsp;&nbsp;&nbsp;&nbsp; <span class="date"><?=$arReviewItem['DATE_ACTIVE_FROM'];?></span></p>
						<div class="state">в Поручении №<?=$arReviewItem['PROPERTY_PORUCHENIE_VALUE'];?> &nbsp;&nbsp;&nbsp; <span class="it"><?=$arReviewItem['NAME'];?></span> &nbsp;&nbsp;&nbsp;&nbsp;
							<div class="rating_container" style="display: inline-block; padding: 0;">
								<div class="rating readonly" data-rate-value="<?=$arReviewItem['PROPERTY_MARK_VALUE'];?>"></div>
								<div class="count"><?=$arReviewItem['PROPERTY_MARK_VALUE'];?></div>
							</div>
						</div>
						<p><?=$arReviewItem['PREVIEW_TEXT'];?></p>
					</div>
				</div>
			</div>
		<?}?>

	</div>
	<pre><?print_r ($arReviews);?></pre>
	<script>
		$(document).ready(function(){
			$('#type_review').change(function(){
				$(this).submit();
			});
		});	
	</script>
	<pre><?print_r ($arUser);?></pre>
<?} else { //страница испонителей?>
	<div class="left_col">
		<div class="left_filter">
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu", 
				"map", 
				array(
					"ALLOW_MULTI_SELECT" => "N",
					"CHILD_MENU_TYPE" => "left",
					"DELAY" => "N",
					"MAX_LEVEL" => "2",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_TYPE" => "N",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"ROOT_MENU_TYPE" => "catalog",
					"USE_EXT" => "Y",
					"COMPONENT_TEMPLATE" => "porucheniya"
				),
				false
			);?>
		</div>
		<div class="banner_left">
			<img src="<?=SITE_TEMPLATE_PATH?>/images/banner2.jpg" alt="">
			<img src="<?=SITE_TEMPLATE_PATH?>/images/banner3.jpg" alt="">
		</div>
	</div>
	<div class="middle_col">
		<?include($_SERVER['DOCUMENT_ROOT'].'/includes/filter_ispolniteli.php');?>
		<?foreach($arUsers as $key => $arUser){?>
			<div class="artists_item">
				<div class="row">
					<div class="pic">
						<img src="<?=$arUser['PHOTO'];?>" alt="">
						<?if($arUser['UF_COUNT_REVIEW']){?>
							<a href="?ID=<?=$arUser['ID'];?>"><?=$arUser['UF_COUNT_REVIEW'].' '.endingsForm($arUser['UF_COUNT_REVIEW'], 'отзыв', 'отзыва', 'отзывов');?></a>
						<?}?>
					</div>
					<div class="description">
						<div class="h2">
							<a href="?ID=<?=$arUser['ID'];?>"><?=$arUser['NAME'];?></a>
								<div class="rating_container" style="display: inline-block; padding: 0 0 0 10px;">
									<div class="rating readonly" data-rate-value="<?=$arUser['UF_RATING'];?>"></div>
									<div class="count"></div>
								</div>
							<span class="ustate">
								<?if($arWorkWithUser[$arUser['ID']]){?>
									<img src="<?=SITE_TEMPLATE_PATH?>/images/handup.png" alt="">&nbsp;
									<span>Вы уже работали вместе</span>
								<?}?>
							</span>
							&nbsp;&nbsp;&nbsp;
							<span class="ustate">
								<?if($arUser['UF_TRIED']){?>
									<img src="<?=SITE_TEMPLATE_PATH?>/images/shield.png" alt="">&nbsp;
									<span>Проверенный исполнитель</span>
								<?}?>
							</span>
						</div>
						
						<div class="state">
							<?if($arActiveUser[$arUser['ID']]){?>
								сейчас на сайте
							<?} else {?>
								Был на сайте 
								<?if($arUser['LAST_Y']){
									echo $arUser['LAST_Y'].' '.endingsForm($arUser['LAST_Y'], 'год', 'года', 'лет'); 
								}?>
								<?if($arUser['LAST_M']){
									echo $arUser['LAST_M'].' '.endingsForm($arUser['LAST_M'], 'месяц', 'месяца', 'месяцев');
								}?>
								<?if($arUser['LAST_D']){
									echo $arUser['LAST_D'].' '.endingsForm($arUser['LAST_D'], 'день', 'дня', 'дней');
								}?>
								<?if($arUser['LAST_H']){
									echo $arUser['LAST_H'].' '.endingsForm($arUser['LAST_H'], 'час', 'часа', 'часов');
								}?>
								<?if($arUser['LAST_MIN']){
									echo $arUser['LAST_MIN'].' '.endingsForm($arUser['LAST_MIN'], 'минуту', 'минуты', 'минут');
								}?>
								назад
							<?}	?>
							</div>
						
						<p>Текст, который человек хочет о себе разместить.  Текст, который человек хочет о себе разместить. Текст, который человек хочет о себе разместить.  Текст, который человек хочет о себе разместить. Текст, который человек хочет о себе разместить.  Текст, который человек хочет о себе разместить. Текст, который человек хочет о себе разместить.</p>
						<a class="button" href="#">Предложить задание</a>
					</div>
				</div>
			</div>
		<?}?>
	</div>
<?}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>