<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?if (!empty($arResult)):?>
	<ul class="nav_left">
		<?foreach($arResult as $key=>$arItem):
			$current_level = $arItem['DEPTH_LEVEL'];
			$next_level = $arResult[$key+1]['DEPTH_LEVEL'];
		?>
			<?if($arItem["IS_PARENT"]):?>
				<li class="<?if($_GET['kat'] == $arItem['PARAMS']['ID'] && $_GET['kat'] != false)echo'active';?>">
					<a href="?kat=<?=$arItem['PARAMS']['ID']?>"><?=$arItem["TEXT"]?> </a>
					<ul <?if($_GET['kat'] != $arItem['PARAMS']['ID']){?>style="display:none;"<?}?>>
			<?else:?>
				<li class="<?if($_GET['sub'] == $arItem['PARAMS']['ID'] && $_GET['sub'] != false)echo'active';?>">
					<a href="?kat=<?=$arItem['PARAMS']['SECTION_ID']?>&sub=<?=$arItem['PARAMS']['ID']?>"><?=$arItem["TEXT"]?></a>
				</li>
			<?endif;?>
			<?if(!$next_level) echo str_repeat("</ul></li>", $current_level-1);
			elseif($current_level>$next_level) echo str_repeat("</ul></li>", $current_level-$next_level);?>
		<?endforeach?>
	</ul>
<?endif?>
